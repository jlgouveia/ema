<?php

class UserManager
{

	public static $language = "";

	public static function login($userVO)
	{

		if(isset($_COOKIE['languageID']))
		{
			$languageID = $_COOKIE['languageID'];
			$language = LanguageManager::getLanguage($languageID);
		}
		else
		{
			$language = LanguageManager::getDefaultLanguage();
			$languageID = $language->ID;
		}
		$userVO->email = stripslashes($userVO->email);
		$userVO->password = stripslashes($userVO->password);


		$sql = "SELECT *, concat(COALESCE(firstName, ''), ' ', COALESCE(lastName, '')) as fullName FROM usersWebsite WHERE lower(email) = '".strtolower($userVO->email)."' AND password = md5('".$userVO->password."') AND active='1' AND enabled = '1' AND approved != '2'";

		BdConn::query($sql);
        $resultset = BdConn::single();


        if (!$resultset) {
			$response = LabelsManager::getLabel("GENERICLOGINERROR", $languageID);
			$notes = $userVO->email;
			LogManager::logAction(0, '3', null, null, $notes);
		}
		else {
			$row = BdConn::next($resultset);
			$response = new UserVO($row);



			if($response->ID)
			{

				if(isset($_SESSION['userID']))
				{
					unset($_SESSION['userID']);
					session_destroy();
				}
				else
				{
					if( !isset($_SESSION) ) session_start();
					session_cache_expire(10);
					$_SESSION["userID"] = $response;

				}


				date_default_timezone_set("Europe/London");
				$lastLoginDate = date('y-m-d H:i:s');
				if(StoreManager::currentCart()) {
					StoreManager::updateUserID($response->ID);
				}
				$SET_LAST_LOGIN_SQL = "UPDATE usersWebsite SET lastLoginDate = '$lastLoginDate' where ID = $response->ID";

				BdConn::runSQL($SET_LAST_LOGIN_SQL);

				LogManager::logAction($response->ID, '1');

			}
		}

		return $response;
	}

	public static function logout()
	{
		if( !isset($_SESSION) ) session_start();

		if (UtilsManager::is_serialized($_SESSION["userID"])) {
		    $userVO = unserialize($_SESSION["userID"]);
		} else {
		    $userVO = UtilsManager::fixObject($_SESSION["userID"]);
		}

		LogManager::logAction($userVO->ID, '2');

		unset($_SESSION['userID']);
		session_destroy();
		if(StoreManager::currentCart())
		{
			return StoreManager::updateUserID(0);
		}
	}

	public static function registerUser($userVO, $lang = null, $duploOptin = true, $sendRegisterToAdmin = true, $parentID = 0, $allowDuplicateEmail = false, $returnLabelTypeSucess = "REGISTERSUCCESS", $registerConfirmationSubject = "REGISTERCONFIRMATIONSUBJECT", $registerConfirmationBody = "REGISTERCONFIRMATIONSBODY", $sendRegisterInfoToUser = false, $updateUser = false)
	{
		$userManagerInstance = new UserManager();


		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		self::$language = $lang;
		
		$settings = SettingsManager::getSettings();
		$registerConfirmationSubject = stripslashes($registerConfirmationSubject);
		$registerConfirmationBody = stripslashes($registerConfirmationBody);


		//$subscribe_date = $user_in['subscribe_date'];
		date_default_timezone_set("Europe/London");
		$subscribe_date = date('y-m-d H:i:s');

		$activation_code = md5(UtilsManager::generatePassword(10));

		$userVO->activationCode = $activation_code;
		$userVO->subscribeDate = $subscribe_date;
		$userVO->enabled = 1;
		// if(isset($userVO->birthday)) { $userVO->birthday = UtilsManager::verifyDateFormat($userVO->birthday, "d/m/Y", 'Y-m-d'); }
		if(isset($userVO->password)) {
			if(!UtilsManager::isValidMd5($userVO->password) && $userVO->password)
			{
				$userVO->password = md5($userVO->password);
			}
		}
		if(!isset($userVO->country)) {
			$userVO->country = IP2NationManager::getCountry(); }

		if(!isset($userVO->shippingContactFirstName)) {
			if(isset($userVO->firstName)) { $userVO->shippingContactFirstName = $userVO->firstName; } }

		if(!isset($userVO->shippingContactLastName)) {
			if(isset($userVO->lastName)) { $userVO->shippingContactLastName = $userVO->lastName; } }

		if(!isset($userVO->shippingContactMobile)) {
			if(isset($userVO->phone)) { $userVO->shippingContactMobile = $userVO->phone; } }

		if(!isset($userVO->shippingAddress)) {
			if(isset($userVO->address)) { $userVO->shippingAddress = $userVO->address; } }

		if(!isset($userVO->shippingZipcodeZone)) {
			if(isset($userVO->zipcodeZone)) { $userVO->shippingZipcodeZone = $userVO->zipcodeZone; } }

		if(!isset($userVO->shippingAlternativeZipcode)) {
			if(isset($userVO->alternativeZipcode)) { $userVO->shippingAlternativeZipcode = $userVO->alternativeZipcode; } }

		if(!isset($userVO->shippingCountry)) {
			if(isset($userVO->country)) { $userVO->shippingCountry = $userVO->country; } }

		if(!isset($userVO->invoiceFirstName)) {
			if(isset($userVO->firstName)) { $userVO->invoiceFirstName = $userVO->firstName; } }

		if(!isset($userVO->invoiceLastName)) {
			if(isset($userVO->lastName)) { $userVO->invoiceLastName = $userVO->lastName; } }

		if(!isset($userVO->invoiceAddress)) {
			if(isset($userVO->address)) { $userVO->invoiceAddress = $userVO->address; } }

		if(!isset($userVO->invoiceZipcodeZone)) {
			if(isset($userVO->zipcodeZone)) { $userVO->invoiceZipcodeZone = $userVO->zipcodeZone; } }

		if(!isset($userVO->invoiceAlternativeZipcode)) {
			if(isset($userVO->alternativeZipcode)) { $userVO->invoiceAlternativeZipcode = $userVO->alternativeZipcode; } }

		if(!isset($userVO->invoiceCountry)) {
			if(isset($userVO->country)) { $userVO->invoiceCountry = $userVO->country; } }

		if(!isset($userVO->invoiceNif)) {
			if(isset($userVO->nif)) { $userVO->invoiceNif = $userVO->nif; } }

		if(isset($userVO->files)) {
			if(!is_array($userVO->files)) { $userVO->files = array(); } }

		
		if(self::isEmailIn($userVO->email) && $parentID == "0" && !$allowDuplicateEmail && $userVO->email != "")
		{
			if(!$updateUser) {
				$label = LabelsManager::getLabel("REGISTERALREADYREGISTERED", $lang);
				return $label;
			}
			else {
				$sqlquery = "SELECT ID FROM usersWebsite WHERE email='".$userVO->email."' AND enabled = 1";

				$result = BdConn::runSQL($sqlquery);
				//self::getUser()


				$userVO->ID = $result[0]['ID'];
				if( !isset($_SESSION) ) session_start();
				$_SESSION["userID"] = serialize($userVO);

				//return $userVO;
				self::editUserBasicInfo($userVO);
			}
		}
		else
		{
			$sql = $userManagerInstance->buildUserQuery($userVO);
		
			$query = BdConn::runSQL($sql);
			$userVO->ID = BdConn::getLastInsertId();
		}
		
		if ($userVO->ID)
		{
			if($userVO->newsletter == 1 && $duploOptin) {
				LogManager::logAction($userVO->ID, '13', null, null, $userVO->notes);
				NewsletterManager::duploOptin($userVO->firstName, $userVO->email, $userVO->activationCode);
			}
			$logNotes = $userVO->email;
			if(isset($userVO->notes)) {
				$logNotes .= ", " .$userVO->notes;
			}
			LogManager::logAction($userVO->ID, '9', null, null, $logNotes);

			if(isset($userVO->files))
			{
				//return self::createUserFolder($res);
				if(count($userVO->files)) {
					$userFolderPath = self::createUserFolder($userVO->ID);
					if($userFolderPath)
					{
						$options = array('upload_dir'=>$userFolderPath, 'upload_url'=>$userFolderPath, 'param_name'=>'file', 'delete_type'=>'POST','image_versions' => array());
						$uploadHandler = new UploadHandler($options, false);
						$uploadHandler->post(false);
					}
				}
			}


			$body = OperationEmailManager::getOperationalEmail($settings->registerUserEmailTemplate, $userVO->ID, $lang);

			if($sendRegisterToAdmin)
			{

				SendEmailManager::sendEmail($settings->globalEmail,  LabelsManager::getLabel($returnLabelTypeSucess, $lang)->label, $body, "", "", false);

			}
			if($sendRegisterInfoToUser && $userVO->email != "")
			{
				SendEmailManager::sendEmail($userVO->email,  LabelsManager::getLabel($returnLabelTypeSucess, $lang)->label, $body, "", "", false);
			}


			// REGISTERSUCCESS
			if($duploOptin && $userVO->email != "")
			{

				if(self::duploOptin($userVO->firstName . $userVO->lastName, $userVO->email, $activation_code, $registerConfirmationSubject, $registerConfirmationBody))
				{
					$label = LabelsManager::getLabel($returnLabelTypeSucess,$lang);
					$label->data = $userVO->ID;
					return $label;
				}
				else
				{
					
					$label = LabelsManager::getLabel("REGISTERERROR", $lang);
					return $label;
				}
			}
			else
			{
					$label = LabelsManager::getLabel($returnLabelTypeSucess,$lang);
					$label->data = $userVO->ID;
					return $label;
			}
		}
		else
		{
			
			$label = LabelsManager::getLabel("REGISTERERROR", $lang);
			return $label;
		}

		$label = LabelsManager::getLabel($returnLabelTypeSucess,$lang);
		$label->data = $userVO->ID;
		return $label;
	}

	private function buildUserQuery($userIn) {
    	$vars = get_object_vars ( $userIn );
    	$query = "INSERT INTO usersWebsite ";
    	$queryFields = "(";
    	$queryValues = " values (";

		foreach($vars as $key=>$value) {
		  if($value && $key != 	"_explicitType" && $key != 	"files") {
			if(!is_array($value)) {
				$queryFields .= $key.',';
			
				$queryValues .= '"'.$value.'",';
			}
		  }

		}
		$queryFields = rtrim($queryFields,",");
		$queryValues = rtrim($queryValues,",");
		$queryFields .= ") ";
		$queryValues .= ") ";
		return $query.$queryFields.$queryValues;
    }

    private function buildEditUserQuery($userIn) {
    	$vars = get_object_vars ( $userIn );
    	$query = "UPDATE usersWebsite SET ";

			foreach($vars as $key=>$value) {
			  if($value && $key != 	"_explicitType" && $key != "ID" && $key != "files") {
			  	$query .= $key.' = "'.$value.'",';
			  }
			}

			$query = rtrim($query,",");
			$query .= " WHERE ID=$userIn->ID";

			return $query;
    }

	public static function updateUserOnOrder($userVO)
	{
		if(!isset($_SESSION) ) session_start();
		if(!isset($_SESSION['userID']))
		{
			return false;
		}
		$update_date = date('y-m-d H:i:s');

		$userVO = new UserVO($userVO);


		if(!$userVO->shippingContactFirstName) { $userVO->shippingContactFirstName = $userVO->firstName; }
		if(!$userVO->shippingContactLastName) { $userVO->shippingContactLastName = $userVO->lastName; }
		if(!$userVO->shippingContactMobile) { $userVO->shippingContactMobile = $userVO->phone; }
		if(!$userVO->shippingAddress) { $userVO->shippingAddress = $userVO->address; }
		if(!$userVO->shippingZipcodeZone) { $userVO->shippingZipcodeZone = $userVO->zipcodeZone; }
		if(!$userVO->shippingAlternativeZipcode) { $userVO->shippingAlternativeZipcode = $userVO->alternativeZipcode; }
		if(!$userVO->shippingCountry) { $userVO->shippingCountry = $userVO->country; }

		if(!$userVO->invoiceFirstName) { $userVO->invoiceFirstName = $userVO->firstName; }
		if(!$userVO->invoiceLastName) { $userVO->invoiceLastName = $userVO->lastName; }
		if(!$userVO->invoiceAddress) { $userVO->invoiceAddress = $userVO->address; }
		if(!$userVO->invoiceZipcodeZone) { $userVO->invoiceZipcodeZone = $userVO->zipcodeZone; }
		if(!$userVO->invoiceAlternativeZipcode) { $userVO->invoiceAlternativeZipcode = $userVO->alternativeZipcode; }
		if(!$userVO->invoiceCountry) { $userVO->invoiceCountry = $userVO->country; }
		if(!$userVO->invoiceNif) { $userVO->invoiceNif = $userVO->nif; }

		$userVO = (array) $userVO;
		extract($userVO);

		$updateUserQuery = ("UPDATE usersWebsite
		SET
		firstName = '$firstName',
		lastName = '$lastName',
		address = '$address',
		zipcode4 = '$zipcode4',
		zipcode3 = '$zipcode3',
		zipcodeZone = '$zipcodeZone',
		alternativeZipcode = '$alternativeZipcode',
		shippingContactFirstName = '$shippingContactFirstName',
		shippingContactLastName = '$shippingContactLastName',
		shippingContactMobile = '$shippingContactMobile',
		shippingAddress = '$shippingAddress',
		shippingZipcode4 = '$shippingZipcode4',
		shippingZipcode3 = '$shippingZipcode3',
		shippingZipcodeZone = '$shippingZipcodeZone',
		shippingAlternativeZipcode = '$shippingAlternativeZipcode',
		shippingCountry = '$shippingCountry',
		invoiceFirstName = '$invoiceFirstName',
		invoiceLastName = '$invoiceLastName',
		invoiceNif = '$invoiceNif',
		invoiceAddress = '$invoiceAddress',
		invoiceZipcode4 = '$invoiceZipcode4',
		invoiceZipcode3 = '$invoiceZipcode3',
		invoiceZipcodeZone = '$invoiceZipcodeZone',
		invoiceAlternativeZipcode = '$invoiceAlternativeZipcode',
		invoiceCountry = '$invoiceCountry',
		phone = '$phone',
		mobile = '$mobile',
		optional1 = '$optional1',
		optional2 = '$optional2',
		optional3 = '$optional3',
		optional4 = '$optional4',
		optional5 = '$optional5',
		optional6 = '$optional6',
		optional7 = '$optional7',
		optional8 = '$optional8',
		optional9 = '$optional9',
		optional10 = '$optional10',
		nif = '$nif',
		editDate = '$update_date'

		WHERE ID = ".$ID);

		return BdConn::runSQL($updateUserQuery);

	}
	/**
	* sendUserRegisterEmail
	*
	* @param number
	* @return boolean
	*/
	public static function sendUserRegisterEmail($userID,$returnLabelTypeSucess = "REGISTERSUCCESS")
	{

		if(isset($_COOKIE['languageID']))
		{
			$languageID = $_COOKIE['languageID'];
		}
		else
		{
			$languageID = LanguageManager::getDefaultLanguage()->ID;
		}
		$settings = SettingsManager::getSettings();

		$body = OperationEmailManager::getOperationalEmail($settings->registerUserEmailTemplate, $userID, $languageID);
		$emailToSend = $settings->globalEmail;
		return SendEmailManager::sendEmail($emailToSend,  LabelsManager::getLabel($returnLabelTypeSucess, $languageID)->label, $body, "", "", false);


	}



	public static function createUserFolder($ID)
	{
		if(!is_dir(USERCONTENTS.$ID.'/'))
		{
			if(mkdir(USERCONTENTS.$ID.'/'))
			{
				return USERCONTENTS.$ID.'/';
			}
			else
			{
				return false;
			}
		}
		else
		{
			return USERCONTENTS.$ID.'/';
		}
		return false;
	}

	public static function editUserBasicInfo($userVO, $lang = null, $parentID = 0)
	{

		$ema = EmaBrain::Instance();
		$currentUser = new UserVO();
		if( !isset($_SESSION) ) {
			session_start();
		}
		if(!$ema::$ALLOW_EDIT_USERS) {
			return false;
		}


		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}


		self::$language = $lang;
		date_default_timezone_set("Europe/London");
		$edit_date = date('y-m-d H:i:s');
		$currentUser = unserialize($_SESSION["userID"]);

		if($parentID != 0)
		{
			if(!self::isUserParent($currentUser->ID, $userVO->ID))
			{
				return false;
			}

		}
		$userVO->editDate = $edit_date;
		$userManagerInstance = new UserManager();

		$sql = $userManagerInstance->buildEditUserQuery($userVO);

		

		$res=BdConn::runSQL($sql);

		if ($res)
		{
			$logNotes = $parentID;
			if($parentID == 0)
			{

				$sql = "SELECT *, concat(COALESCE(firstName, ''), ' ', COALESCE(lastName, '')) as fullName FROM usersWebsite WHERE ID = '$userVO->ID'";

				$resultset=BdConn::runSQL($sql);

				$userVO = new UserVO($resultset[0]);

				$logNotes = $userVO->email .", " .$userVO->notes;


				if( !isset($_SESSION) ) session_start();

				session_cache_expire(10);
				$_SESSION["userID"] = serialize($userVO);

			}
			LogManager::logAction($userVO->ID, '12', null, null, $logNotes);
			return LabelsManager::getLabel("USEREDITSUCCESS",$lang);
		}

		else
		{
			return LabelsManager::getLabel("USEREDITERROR", $lang);
		}
	}

	public static function isUserParent($parentID, $userID)
	{
		$sql = "SELECT ID FROM usersWebsite WHERE parentID = '".$parentID."' AND ID ='".$userID."'";
		$sql_relation = "SELECT childID FROM relations WHERE parentID = $parentID AND parentTable = 'usersWebsite' AND childTable = 'usersWebsite' and childID = $userID";
		$resultset=BdConn::runSQL($sql);
		$resultset_relation=BdConn::runSQL($sql_relation);

		if (mysql_num_rows($resultset) != 1 && mysql_num_rows($resultset_relation) == 0)
		{
			return false;
		}
		return true;
	}

	public static function duploOptin($name_in,$email_in, $active_code_in, $registerConfirmationSubject = "REGISTERCONFIRMATIONSUBJECT", $registerConfirmationBody = "REGISTERCONFIRMATIONSBODY"){

		$to = $email_in;

		// Provides: <body text='black'>

		$mySubect = LabelsManager::getLabelValue($registerConfirmationSubject,self::$language);

		$subject = SettingsManager::getAdminName()." / ".$mySubect;


		$link = SettingsManager::getSiteURL() ."/ema/frontend/inc/registerActivation.php?activation_code=". $active_code_in   . "&amp;language=".self::$language."&email=" . $email_in;

		$myContent = LabelsManager::getLabelValue($registerConfirmationBody,self::$language);

		$link_replace = array("%LINK%");

		$content = str_replace($link_replace,$link, $myContent);

		if(SendEmailManager::sendEmail($to, $subject, $content))
		{
			return true;
		}

		else
		{
			return false;
		}
	}

	public static function activateSubscription($email_in, $active_code_in, $lang, $sendPassword = true, $isNewsletter = false)
	{


	//	$email_in = $user_in['email'];
	//	$active_code_in = $user_in['activation_code'];
		//return NewsletterManager::isCodeValid($email_in, $active_code_in);
		//return self::isEmailActive($email_in);
		if  (!self::isEmailIn($email_in))
		{

			//LabelsManager::getLabelValue("NEWSLETTEREMAILNOTFOUND", $lang);
			return LabelsManager::getLabelValue("NEWSLETTEREMAILNOTFOUND", $lang);


		} else {

			if  (!NewsletterManager::isCodeValid($email_in, $active_code_in))
			{

					//LabelsManager::getLabelValue("NEWSLETTERINVALIDCODE", $lang);
					return LabelsManager::getLabelValue("NEWSLETTERINVALIDCODE", $lang);
			}
			else {


				if  (!self::isEmailActive($email_in))
				{

					if(self::getUserByActivationCode($active_code_in, $email_in)->password == "")
					{

						$pwd = UtilsManager::generatePassword(6);
						$pwd_enc = md5($pwd);

						$newsletterQuery = "";

						if($isNewsletter) {
							$newsletterQuery = ", newsletter = 1 ";
						}
						$res=BdConn::runSQL("UPDATE usersWebsite SET active='1', password = '$pwd_enc' $newsletterQuery WHERE email='$email_in' AND activationCode='$active_code_in'");

						if ($res)
						{
							$sender = SettingsManager::getAdminName()."<".SettingsManager::getGlobalEmail().">";
							$to = $email_in;

							$mySubect = LabelsManager::getLabelValue("REGISTERACTIVATIONSUBJECTSUCCESS", $lang);
							$subject = SettingsManager::getAdminName()." / ".$mySubect;



							$content = LabelsManager::getLabelValue("REGISTERACTIVATIONCONTENTSUCCESS", $lang);
							if($sendPassword)
							{
								$emailBody = $content."<br/><br/><b>Login:</b> <a style='color: {{linkColor}};' href='mailto:".$email_in."'>".$email_in."</a><br/><b>Password:</b> ".$pwd;

								if(SendEmailManager::sendEmail($to, $subject, $emailBody))
								{
									return $content;
								}
								else
								{
									return LabelsManager::getLabelValue("REGISTERERROR", $lang);
								}
							}
							else {
								return $content;
							}
						}
					}
					else
					{
						if($isNewsletter) {
							$newsletterQuery = ", newsletter = 1 ";
						}
						$res=BdConn::runSQL("UPDATE usersWebsite SET active='1' $newsletterQuery WHERE email='$email_in' AND activationCode='$active_code_in'");
						$sender = SettingsManager::getAdminName()."<".SettingsManager::getGlobalEmail().">";
						$to = $email_in;

						$mySubect = LabelsManager::getLabelValue("REGISTERACTIVATIONSUBJECTSUCCESS", $lang);
						$subject = SettingsManager::getAdminName()." / ".$mySubect;



						$content = LabelsManager::getLabelValue("REGISTERACTIVATIONCONTENTSUCCESS", $lang);

						$emailBody = $content."<br/><br/><b>Login:</b> <a style='color: {{linkColor}};' href='mailto:".$email_in."'>".$email_in."</a><br/>";

						if(SendEmailManager::sendEmail($to, $subject, $emailBody))
						{
							return $content;
						}
					}

				}
				else
				{
					return LabelsManager::getLabelValue("NEWSLETTERALREADYACTIVATED", $lang);
				}
			}
		}
	}

	public static function recoverPassword($user, $lang)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		self::$language = $lang;
		if (!self::isEmailIn($user->email))
		{
			return LabelsManager::getLabel("USERRECOVERPASSWORDERROR", $lang);
		}
		else
		{
			return self::sendNewPasswordRequest($user, $lang);
		}
	}

	private function sendNewPasswordRequest($user, $lang)
	{


		$sql = "SELECT * FROM usersWebsite WHERE email = '$user->email'";

		$resultset=BdConn::runSQL($sql);

		$row = BdConn::next($resultset);
		$userVO = new UserVO($row);

		$to = $user->email;

		$mySubect = LabelsManager::getLabelValue("USERRECOVERPASSWORDSUBJECT", $lang);
		$subject = SettingsManager::getAdminName()." / ".$mySubect;

		$content = LabelsManager::getLabelValue("USERRECOVERPASSWORDCONTENT", $lang);

		$link = SettingsManager::getSiteURL() ."frontend/inc/recoverPassword.php?activation_code=". $userVO->activationCode   . "&amp;language=".self::$language."&email=" . $user->email;

		$link_replace = array("%LINK%");

		$content = str_replace($link_replace,$link, $content);

		if(SendEmailManager::sendEmail($to, $subject, $content))
		{
			return LabelsManager::getLabel("USERRECOVERPASSWORDSTANDBY", $lang);
		}
		else
		{
			return LabelsManager::getLabel("USERRECOVERPASSWORDERROR", $lang);
		}

	}

	public static function sendNewPassword($user, $lang)
	{

		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		self::$language = $lang;

		$pwd = UtilsManager::generatePassword(6);
		$pwd_enc = md5($pwd);

		$res=BdCoon::runSQL("UPDATE usersWebsite SET active='1', password = '$pwd_enc' WHERE email='$user->email' AND activationCode='$user->activationCode'");

		if ($res)
		{
			$to = $user->email;

			$mySubect = LabelsManager::getLabelValue("USERRECOVERPASSWORDSUBJECTSUCCESS", $lang);
			$subject = SettingsManager::getAdminName()." / ".$mySubect;


			$content = LabelsManager::getLabelValue("USERRECOVERPASSWORDCONTENTSUCCESS", $lang);
			$contentBody = LabelsManager::getLabelValue("USERRECOVERPASSWORDBODYSUCCESS", $lang);

			$emailBody = $contentBody."<br/><br/><b>Password:</b> ".$pwd;


			if(SendEmailManager::sendEmail($to, $subject, $emailBody))
			{
				return $content;
			}
			else
			{
				return LabelsManager::getLabelValue("USERRECOVERPASSWORDERROR", $lang);
			}
		}
	}

	public static function isEmailIn($email_in){
		$sqlquery = "SELECT email FROM usersWebsite WHERE email='$email_in' AND enabled = 1";

		$result = BdConn::query($sqlquery);
		$number = BdConn::single();

		if ($number) {
			return true;
		}
		else {
			return false;
		}

	}
	/**
	*
 	* Author: Jose Luis Gouveia
	* This method verifies if the email is already activated
	*
	*/
	public static function isEmailActive($email_in)
	{
		$sqlquery = "SELECT active FROM usersWebsite WHERE email='$email_in' AND active = '1'";
		$result = BdConn::query($sqlquery);
		$number = BdConn::single();

		if ($number) {
			return true;
		}
		else {
			return false;
		}
	}

	public static function getUserByActivationCode($activationCode, $emailIn)
	{
		$sql = "SELECT *, concat(COALESCE(firstName, ''), ' ', COALESCE(lastName, '')) as fullName FROM usersWebsite WHERE activationCode='".$activationCode."' AND email = '".$emailIn."'";

		$resultset=BdConn::runSQL($sql);

		$row = BdConn::next($resultset);
		$response = new UserVO($row);
		return $response;
	}

	public static function currentUser($activationCode = null)
	{
		$response = "";
		if(isset($_SESSION['userID']))
		{
			if(!AuthorizationManager::hasAuthorization())
			{
				return;
			}

			if (UtilsManager::is_serialized($_SESSION["userID"])) {
			    $userVO = unserialize($_SESSION["userID"]);
			} else {
			    $userVO = UtilsManager::fixObject($_SESSION["userID"]);
			}

			$sql = "SELECT *, concat(COALESCE(firstName, ''), ' ', COALESCE(lastName, '')) as fullName FROM usersWebsite WHERE ID=".$userVO->ID;

			$query = BdConn::query($sql);
			$row = BdConn::single();
	        $response = new UserVO($row);

		}
		return $response;

	}

	public static function getAllContactsFromUser()
	{
		if( !isset($_SESSION) ) session_start();
		if(!isset($_SESSION['userID']))
		{
			return false;
		}

		$userVO = unserialize($_SESSION["userID"]);

		$sql = "SELECT firstName, lastName, ID, job, concat(COALESCE(firstName, ''), ' ', COALESCE(lastName, '')) as fullName FROM usersWebsite WHERE parentID='".$userVO->ID."' AND active = 1 AND approved = 1 AND enabled = 1";
		$resultset=BdConn::runSQL($sql);

		$response;
		$response->userList = array();
		$users = array();

		while ($row=mysql_fetch_array($resultset))
		{
			$users[] = $row;
		}

		$alpha = range('a', 'z');

		for($i =0; $i < count($alpha); $i++)
		{
			$letter = new stdClass();
			$usersWithLetter = array();

			for($u = 0; $u < count($users); $u++)
			{
				if($users[$u]['firstName'])
				{
					$key = strtolower($users[$u]['firstName'][0]);
				}
				else
				{
					$key = strtolower($users[$u]['lastName'][0]);
				}
				if($key == $alpha[$i])
				{
					$usersWithLetter[] = $users[$u];
				}
			}

			$letter->letter = $alpha[$i];
			$letter->users = $usersWithLetter;
			if(count($letter->users))
			{
				$response->userList[] = $letter;
			}

		}

		//TODO: Zé Luis checa isto depois sff
		$response->schoolsList = UserManager::getListFromField('job');


		return $response;

	}

	public static function getUser($userID)
	{
		$ema = EmaBrain::Instance();
		$currentUser = new UserVO();
		
		if( !isset($_SESSION) ) {
			session_start();
		}
		if(!$ema::$ALLOW_GET_USERS) {
			return false;
		}
		$userID = intval($userID);
		BdConn::query("SELECT *, concat(COALESCE(firstName, ''), ' ', COALESCE(lastName, '')) as fullName FROM usersWebsite WHERE ID = '$userID'");
        $query = BdConn::single();
		$userVO = new UserVO($query);
		return $userVO;
	}


	public static function getListFromField($field, $group = true)
	{
		//TODO: Zé Luis checa isto depois sff
		if( !isset($_SESSION) ) session_start();
		if(!isset($_SESSION['userID']))
		{
			return false;
		}

		$userVO = unserialize($_SESSION["userID"]);
		$groupSql = "";
		if($group)
		{
			$groupSql = " GROUP BY ".$field;
		}
		$field = trim($field);

		$sql = "SELECT ".$field." FROM usersWebsite WHERE parentID = '".$userVO->ID."'".$groupSql;

		$resultset=BdConn::runSQL($sql);

		$response = array();

		while ($row=mysql_fetch_array($resultset))
		{
			$response[] = $row;
		}

		return $response;

	}

	public static function getAllUsers()
	{
		$ema = EmaBrain::Instance();
		if($ema::$ALLOW_GET_USERS) {
			$sql = "SELECT *, concat(COALESCE(firstName, ''), ' ', COALESCE(lastName, '')) as fullName FROM usersWebsite WHERE enabled = 1";

			$resultset=BdConn::runSQL($sql);

			$response = array();

			while ($row=mysql_fetch_array($resultset))
			{
				$userVO = new UserVO($row);
				$response[] = $userVO;
			}

			return $response;
		}
		return false;
	}

	public static function searchUser($search, $limit = 5, $searchField = null)
	{
		$search = strtolower($search);

		$searchTerms = explode(" ", $search);


		$addFieldQuery = " lower(firstName) like '%$search%' collate utf8_general_ci OR lower(lastName) like '%$search%' collate utf8_general_ci OR concat_ws(' ',lower(firstName), lower(lastName)) like '%$search%' collate utf8_general_ci ";

		switch (count($searchTerms)) {
			case 2:

			$addFieldQuery .= " OR lower(firstName) like '%".$searchTerms[0]."%' collate utf8_general_ci AND lower(lastName) like '%".$searchTerms[1]."%' collate utf8_general_ci ";

			break;

		}
		if(mb_strlen($search, "utf-8") == 1) {
			$addFieldQuery = " lower(firstName) like '$search%' collate utf8_general_ci OR lower(lastName) like '$search%' collate utf8_general_ci ";
		}

		if($searchField) {
			//$searchField = mysql_real_escape_string($searchField);
			$addFieldQuery = " lower($searchField) like '%$search%' collate utf8_general_ci ";
		}

		$ema = EmaBrain::Instance();
		//$limit = mysql_escape_string($limit);


		if($ema::$ALLOW_GET_USERS) {
			$sql = "SELECT *, concat(COALESCE(firstName, ''), ' ', COALESCE(lastName, '')) as fullName FROM usersWebsite WHERE enabled = 1 AND ($addFieldQuery) order by firstName, lastName, email, job asc limit $limit";

			//return $sql;

			$resultset=BdConn::runSQL($sql);

			$response = array();

			$response = array();
			foreach ($resultset as $row)
			{
				$userVO = new UserVO($row);
				$response[] = $userVO;
			}

			return $response;
		}
		return false;
	}

	// YET TO DO
	public static function hasUserChanged($userVO, $update = false) {


		// check changes for userVO
		//self::editUserBasicInfo($userVO, $lang = null, $parentID = 0);


	}



}
