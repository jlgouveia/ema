<?php

class InvoicexpressManager
{


	public static function createInvoice($order, $client)
     {
            // transform data:
            include SPATH_API . '/invoicexpress/apiXML.php';
            include SPATH_API . '/invoicexpress/conf.php';

            $invoiceState = $order['invoiceState'];

            if (!$invoiceState || $invoiceState == "")
                $invoiceState = 0;

            if ($invoiceState == 0)
            {
                $invoiceXML = $this->generateNewInvoiceXML($order['orderID'], $order['orderTotal'], $client['invoiceName'], $client['clientID'], $client['email'], $client['nif'], $client['country'], $newInvoiceXML);

                if (!$invoiceXML)
                    return false;

                Logger::slog('INVOICE XML: ' . print_r($invoiceXML, true));

                $url = "https://".INVOICE_API_SCREEN_NAME.".".INVOICE_API_HOST."/simplified_invoices.xml?api_key=".INVOICE_API_KEY;
                $result = $this->invoiceExpressCall(1, $url, $invoiceXML);

                // TODO: VALIDATE RESPONSES! Log error
                if (!$result)
                {
                    Logger::slog('ERROR creating invoice: ' . print_r($result, true));
                    return false;
                }

                $invoice = new SimpleXMLElement($result);

                $invoiceState = 1;

                if (!$this->updateOrderInvoice($order['orderID'], $invoice->id, $invoiceState))
                    return false;

                $order['invoiceID'] = $invoice->id;
            }


            if ($invoiceState == 1)
            {

                $url = "https://".INVOICE_API_SCREEN_NAME.".".INVOICE_API_HOST."/simplified_invoices/". $order['invoiceID'] ."/change-state.xml?api_key=".INVOICE_API_KEY;
                $result = $this->invoiceExpressCall(2, $url, $changeInvoiceStateXML);

                // TODO: VALIDATE RESPONSES! Log error
                if (!$result)
                {
                    Logger::slog('ERROR finalizing invoice: ' . print_r($result, true));
                    return false;
                }

                $invoiceState = 2;

                if (!$this->updateOrderInvoice($order['orderID'], $order['invoiceID'], $invoiceState))
                    return false;

            }


            if ($invoiceState == 2)
            {
                $emailXML = $this->generateEmailInvoiceXML($client['email'], $emailInvoiceXML);

                $url = "https://".INVOICE_API_SCREEN_NAME.".".INVOICE_API_HOST."/simplified_invoices/". $order['invoiceID'] ."/email-document.xml?api_key=".INVOICE_API_KEY;
                $result = $this->invoiceExpressCall(2, $url, $emailXML);
                // TODO: VALIDATE RESPONSES! Log error

                $invoiceState = 3;
                if (!$this->updateOrderInvoice($order['orderID'], $order['invoiceID'], $invoiceState))
                    return false;
                // actualizar state na DB
            }

            return true;
        }


		protected function generateNewInvoiceXML($orderID, $orderTotal, $clientName, $clientID, $clientEmail, $clientNif, $country, $newInvoiceXML)
        {
            //$licensePriceBeforeTaxes = round(OrdersManager::$LICENSE_PRICE / 1.23, 2);
            $licensePriceBeforeTaxes = OrdersManager::$LICENSE_PRICE / 1.23;
            $discountWithTaxes = OrdersManager::$LICENSE_PRICE - $orderTotal;
            $discountValue = NULL;
            if ($discountWithTaxes > 0)
            {
                $discountValue = $discountWithTaxes / 1.23;
                $discountValue = ($discountValue * 100) / $licensePriceBeforeTaxes;
                //$discountValue = round($discountValue, 2);
            }

            $date = date('d/m/Y');

            $simplefiedInvoice = new SimpleXMLElement($newInvoiceXML);
            $simplefiedInvoice->reference = $orderID;
            $simplefiedInvoice->date = $date;
            if (!is_null($clientName))
                $simplefiedInvoice->client->name = $clientName;
            $simplefiedInvoice->client->email = $clientEmail;

            $simplefiedInvoice->client->country = $country;

            if (!is_null($clientNif))
            {
                    $simplefiedInvoice->client->addChild('fiscal_id', $clientNif);
                $simplefiedInvoice->client->code = $clientID;
            }
            else
            {
                if (!is_null($clientName))
                    $simplefiedInvoice->client->code = $clientID;
            }
            $simplefiedInvoice->items->item[0]->unit_price = $licensePriceBeforeTaxes;
            if (!is_null($discountValue))
                $simplefiedInvoice->items->item[0]->addChild('discount', $discountValue);

            return $simplefiedInvoice->asXML();
        }

}