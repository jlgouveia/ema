<?php

	class BingManager
	{

		function __construct() {
			$this->bing = new Bing();

			 //Client ID of the application.
		    $this->clientID     = "63b3c09f-daca-43a8-88b7-97d352fec086";
		    //Client Secret key of the application.
		    $this->clientSecret = "eQqQ7oN4RqVJf5RiTX9Yq1Yh/MZislt46TXXp4YsAe4";
		    //OAuth Url.
		    $this->authUrl      = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13/";
		    //Application Scope Url
		    $this->scopeUrl     = "http://api.microsofttranslator.com";
		    //Application grant type
		    $this->grantType    = "client_credentials";

		    //Get the Access token.
		    $this->accessToken  = $this->bing->getTokens($this->grantType, $this->scopeUrl, $this->clientID, $this->clientSecret, $this->authUrl);
		    //Create the authorization Header string.
		    $this->authHeader = "Authorization: Bearer ". $this->accessToken;
		}


		/*
		*	Author: Jose Luis Gouveia
		*
		*	translate:
		*	@string to be translated
		*	@string languageCodes code to be translated in
		*/
		public static function translate($inputStr, $langShort)
		{
			//Create the Translator Object.

			//Input String.
			//$inputStr = 'traduz isto para françês';
			//HTTP Detect Method URL.
			$detectMethodUrl = "http://api.microsofttranslator.com/V2/Http.svc/Detect?text=".urlencode($inputStr);
			//Call the curlRequest.
			$from = $this->bing->curlRequest($detectMethodUrl, $this->authHeader);

			//Interprets a string of XML into an object.
			$xmlObj = simplexml_load_string($from);
			foreach((array)$xmlObj[0] as $val){
				$languageCode = $val;
			}

			/*
			* Get the language Names from languageCodes.
			*/
			//$langShort = 'en';
			$getLanguageNamesurl = "http://api.microsofttranslator.com/V2/Http.svc/GetLanguageNames?locale=$langShort";
			//Create the Request XML format.
			$requestXml = $this->bing->createReqXML($languageCode);
			//Call the curlRequest.
			$strResponse = $this->bing->curlRequest($getLanguageNamesurl, $this->authHeader, $requestXml);

			$getTranslation = "http://api.microsofttranslator.com/V2/Http.svc/Translate?text=".urlencode($inputStr)."&to=".$langShort;

			//Create the Request XML format.

			//Call the curlRequest.
			$curlResponse = $this->bing->curlRequest($getTranslation, $this->authHeader);
			$xml = new SimpleXMLElement($curlResponse);

			return $xml[0];

		}

    }

?>