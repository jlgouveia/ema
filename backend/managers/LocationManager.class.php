<?php

class LocationManager {
	public static function getGeoCounty($geoAddress)
	{
	    $url = 'http://maps.google.com/maps/api/geocode/json?address=' . $geoAddress .'&sensor=false';
	    $get     = file_get_contents($url);
	    $geoData = json_decode($get);
	    if (json_last_error() !== JSON_ERROR_NONE) {
	        throw new \InvalidArgumentException('Invalid geocoding results');
	    }
		$geoDataObj = new stdClass();

	    if(isset($geoData->results[0]))
	    {
	        foreach($geoData->results[0]->address_components as $addressComponent) {
	            if(in_array('administrative_area_level_2', $addressComponent->types)) {
	                $geoDataObj->administrative_area_level_2 =  $addressComponent->long_name;
	            }
	            if(in_array('administrative_area_level_1', $addressComponent->types)) {
	                $geoDataObj->administrative_area_level_1 =  $addressComponent->long_name;
	            }
	            if(in_array('country', $addressComponent->types)) {
	                $geoDataObj->country =  $addressComponent->long_name;
	            }
	            if(in_array('country', $addressComponent->types)) {
	                $geoDataObj->short_country =  $addressComponent->short_name;
	            }
	             if(in_array('administrative_area_level_3', $addressComponent->types)) {
	                $geoDataObj->administrative_area_level_3 =  $addressComponent->short_name;
	            }


	         }

			$geoDataObj->address =  $geoData->results[0]->formatted_address;
	        return $geoDataObj;
	    }
	}
/*
	object(stdClass)#11 (2) {
  ["results"]=>
  array(6) {
    [0]=>
    object(stdClass)#31 (5) {
      ["address_components"]=>
      array(8) {
        [0]=>
        object(stdClass)#32 (3) {
          ["long_name"]=>
          string(1) "9"
          ["short_name"]=>
          string(1) "9"
          ["types"]=>
          array(1) {
            [0]=>
            string(13) "street_number"
          }
        }
        [1]=>
        object(stdClass)#33 (3) {
          ["long_name"]=>
          string(5) "Rua C"
          ["short_name"]=>
          string(4) "R. C"
          ["types"]=>
          array(1) {
            [0]=>
            string(5) "route"
          }
        }
        [2]=>
        object(stdClass)#34 (3) {
          ["long_name"]=>
          string(17) "Pinhal do Cabedal"
          ["short_name"]=>
          string(17) "Pinhal do Cabedal"
          ["types"]=>
          array(2) {
            [0]=>
            string(8) "locality"
            [1]=>
            string(9) "political"
          }
        }
        [3]=>
        object(stdClass)#35 (3) {
          ["long_name"]=>
          string(7) "Castelo"
          ["short_name"]=>
          string(7) "Castelo"
          ["types"]=>
          array(2) {
            [0]=>
            string(27) "administrative_area_level_3"
            [1]=>
            string(9) "political"
          }
        }
        [4]=>
        object(stdClass)#36 (3) {
          ["long_name"]=>
          string(8) "Sesimbra"
          ["short_name"]=>
          string(8) "Sesimbra"
          ["types"]=>
          array(2) {
            [0]=>
            string(27) "administrative_area_level_2"
            [1]=>
            string(9) "political"
          }
        }
        [5]=>
        object(stdClass)#37 (3) {
          ["long_name"]=>
          string(8) "Setúbal"
          ["short_name"]=>
          string(8) "Setúbal"
          ["types"]=>
          array(2) {
            [0]=>
            string(27) "administrative_area_level_1"
            [1]=>
            string(9) "political"
          }
        }
        [6]=>
        object(stdClass)#38 (3) {
          ["long_name"]=>
          string(8) "Portugal"
          ["short_name"]=>
          string(2) "PT"
          ["types"]=>
          array(2) {
            [0]=>
            string(7) "country"
            [1]=>
            string(9) "political"
          }
        }
        [7]=>
        object(stdClass)#39 (3) {
          ["long_name"]=>
          string(4) "2970"
          ["short_name"]=>
          string(4) "2970"
          ["types"]=>
          array(2) {
            [0]=>
            string(18) "postal_code_prefix"
            [1]=>
            string(11) "postal_code"
          }
        }
      }
      ["formatted_address"]=>
      string(40) "R. C 9, 2970 Pinhal do Cabedal, Portugal"
      ["geometry"]=>
      object(stdClass)#40 (3) {
        ["location"]=>
        object(stdClass)#41 (2) {
          ["lat"]=>
          float(38.4869175)
          ["lng"]=>
          float(-9.1188354)
        }
        ["location_type"]=>
        string(7) "ROOFTOP"
        ["viewport"]=>
        object(stdClass)#42 (2) {
          ["northeast"]=>
          object(stdClass)#43 (2) {
            ["lat"]=>
            float(38.4882664803)
            ["lng"]=>
            float(-9.11748641971)
          }
          ["southwest"]=>
          object(stdClass)#44 (2) {
            ["lat"]=>
            float(38.4855685197)
            ["lng"]=>
            float(-9.12018438029)
          }
        }
      }
      ["place_id"]=>
      string(27) "ChIJ0c-F7sFNGQ0RU3BXPJp8Nw4"
      ["types"]=>
      array(1) {
        [0]=>
        string(14) "street_address"
      }
    }
    [1]=>
    object(stdClass)#45 (5) {
      ["address_components"]=>
      array(2) {
        [0]=>
        object(stdClass)#46 (3) {
          ["long_name"]=>
          string(4) "2970"
          ["short_name"]=>
          string(4) "2970"
          ["types"]=>
          array(2) {
            [0]=>
            string(18) "postal_code_prefix"
            [1]=>
            string(11) "postal_code"
          }
        }
        [1]=>
        object(stdClass)#47 (3) {
          ["long_name"]=>
          string(8) "Portugal"
          ["short_name"]=>
          string(2) "PT"
          ["types"]=>
          array(2) {
            [0]=>
            string(7) "country"
            [1]=>
            string(9) "political"
          }
        }
      }
      ["formatted_address"]=>
      string(14) "2970, Portugal"
      ["geometry"]=>
      object(stdClass)#48 (4) {
        ["bounds"]=>
        object(stdClass)#49 (2) {
          ["northeast"]=>
          object(stdClass)#50 (2) {
            ["lat"]=>
            float(38.575749)
            ["lng"]=>
            float(-9.0176531)
          }
          ["southwest"]=>
          object(stdClass)#51 (2) {
            ["lat"]=>
            float(38.4090711)
            ["lng"]=>
            float(-9.2229224)
          }
        }
        ["location"]=>
        object(stdClass)#52 (2) {
          ["lat"]=>
          float(38.4856094)
          ["lng"]=>
          float(-9.1234795)
        }
        ["location_type"]=>
        string(11) "APPROXIMATE"
        ["viewport"]=>
        object(stdClass)#53 (2) {
          ["northeast"]=>
          object(stdClass)#54 (2) {
            ["lat"]=>
            float(38.575749)
            ["lng"]=>
            float(-9.0176531)
          }
          ["southwest"]=>
          object(stdClass)#55 (2) {
            ["lat"]=>
            float(38.4090711)
            ["lng"]=>
            float(-9.2229224)
          }
        }
      }
      ["place_id"]=>
      string(27) "ChIJ8_hP6sZNGQ0R4LE7DsHrABw"
      ["types"]=>
      array(2) {
        [0]=>
        string(18) "postal_code_prefix"
        [1]=>
        string(11) "postal_code"
      }
    }
    [2]=>
    object(stdClass)#56 (5) {
      ["address_components"]=>
      array(4) {
        [0]=>
        object(stdClass)#57 (3) {
          ["long_name"]=>
          string(7) "Castelo"
          ["short_name"]=>
          string(7) "Castelo"
          ["types"]=>
          array(2) {
            [0]=>
            string(27) "administrative_area_level_3"
            [1]=>
            string(9) "political"
          }
        }
        [1]=>
        object(stdClass)#58 (3) {
          ["long_name"]=>
          string(8) "Sesimbra"
          ["short_name"]=>
          string(8) "Sesimbra"
          ["types"]=>
          array(2) {
            [0]=>
            string(27) "administrative_area_level_2"
            [1]=>
            string(9) "political"
          }
        }
        [2]=>
        object(stdClass)#59 (3) {
          ["long_name"]=>
          string(8) "Setúbal"
          ["short_name"]=>
          string(8) "Setúbal"
          ["types"]=>
          array(2) {
            [0]=>
            string(27) "administrative_area_level_1"
            [1]=>
            string(9) "political"
          }
        }
        [3]=>
        object(stdClass)#60 (3) {
          ["long_name"]=>
          string(8) "Portugal"
          ["short_name"]=>
          string(2) "PT"
          ["types"]=>
          array(2) {
            [0]=>
            string(7) "country"
            [1]=>
            string(9) "political"
          }
        }
      }
      ["formatted_address"]=>
      string(17) "Castelo, Portugal"
      ["geometry"]=>
      object(stdClass)#61 (4) {
        ["bounds"]=>
        object(stdClass)#62 (2) {
          ["northeast"]=>
          object(stdClass)#63 (2) {
            ["lat"]=>
            float(38.5757522)
            ["lng"]=>
            float(-9.0176742)
          }
          ["southwest"]=>
          object(stdClass)#64 (2) {
            ["lat"]=>
            float(38.409078)
            ["lng"]=>
            float(-9.2229219)
          }
        }
        ["location"]=>
        object(stdClass)#65 (2) {
          ["lat"]=>
          float(38.4856094)
          ["lng"]=>
          float(-9.1234795)
        }
        ["location_type"]=>
        string(11) "APPROXIMATE"
        ["viewport"]=>
        object(stdClass)#66 (2) {
          ["northeast"]=>
          object(stdClass)#67 (2) {
            ["lat"]=>
            float(38.5757522)
            ["lng"]=>
            float(-9.0176742)
          }
          ["southwest"]=>
          object(stdClass)#68 (2) {
            ["lat"]=>
            float(38.409078)
            ["lng"]=>
            float(-9.2229219)
          }
        }
      }
      ["place_id"]=>
      string(27) "ChIJ8_hP6sZNGQ0RYAqR5L3rAAU"
      ["types"]=>
      array(2) {
        [0]=>
        string(27) "administrative_area_level_3"
        [1]=>
        string(9) "political"
      }
    }
    [3]=>
    object(stdClass)#69 (5) {
      ["address_components"]=>
      array(3) {
        [0]=>
        object(stdClass)#70 (3) {
          ["long_name"]=>
          string(8) "Sesimbra"
          ["short_name"]=>
          string(8) "Sesimbra"
          ["types"]=>
          array(2) {
            [0]=>
            string(27) "administrative_area_level_2"
            [1]=>
            string(9) "political"
          }
        }
        [1]=>
        object(stdClass)#71 (3) {
          ["long_name"]=>
          string(7) "Setubal"
          ["short_name"]=>
          string(7) "Setubal"
          ["types"]=>
          array(2) {
            [0]=>
            string(27) "administrative_area_level_1"
            [1]=>
            string(9) "political"
          }
        }
        [2]=>
        object(stdClass)#72 (3) {
          ["long_name"]=>
          string(8) "Portugal"
          ["short_name"]=>
          string(2) "PT"
          ["types"]=>
          array(2) {
            [0]=>
            string(7) "country"
            [1]=>
            string(9) "political"
          }
        }
      }
      ["formatted_address"]=>
      string(18) "Sesimbra, Portugal"
      ["geometry"]=>
      object(stdClass)#73 (4) {
        ["bounds"]=>
        object(stdClass)#74 (2) {
          ["northeast"]=>
          object(stdClass)#75 (2) {
            ["lat"]=>
            float(38.5833189)
            ["lng"]=>
            float(-9.0176742)
          }
          ["southwest"]=>
          object(stdClass)#76 (2) {
            ["lat"]=>
            float(38.409078)
            ["lng"]=>
            float(-9.2229219)
          }
        }
        ["location"]=>
        object(stdClass)#77 (2) {
          ["lat"]=>
          float(38.4444674)
          ["lng"]=>
          float(-9.1020079)
        }
        ["location_type"]=>
        string(11) "APPROXIMATE"
        ["viewport"]=>
        object(stdClass)#78 (2) {
          ["northeast"]=>
          object(stdClass)#79 (2) {
            ["lat"]=>
            float(38.5833189)
            ["lng"]=>
            float(-9.0176742)
          }
          ["southwest"]=>
          object(stdClass)#80 (2) {
            ["lat"]=>
            float(38.409078)
            ["lng"]=>
            float(-9.2229219)
          }
        }
      }
      ["place_id"]=>
      string(27) "ChIJAy8mXMZNGQ0RYDuQ5L3rAAQ"
      ["types"]=>
      array(2) {
        [0]=>
        string(27) "administrative_area_level_2"
        [1]=>
        string(9) "political"
      }
    }
    [4]=>
    object(stdClass)#81 (5) {
      ["address_components"]=>
      array(2) {
        [0]=>
        object(stdClass)#82 (3) {
          ["long_name"]=>
          string(7) "Setubal"
          ["short_name"]=>
          string(7) "Setubal"
          ["types"]=>
          array(2) {
            [0]=>
            string(27) "administrative_area_level_1"
            [1]=>
            string(9) "political"
          }
        }
        [1]=>
        object(stdClass)#83 (3) {
          ["long_name"]=>
          string(8) "Portugal"
          ["short_name"]=>
          string(2) "PT"
          ["types"]=>
          array(2) {
            [0]=>
            string(7) "country"
            [1]=>
            string(9) "political"
          }
        }
      }
      ["formatted_address"]=>
      string(17) "Setubal, Portugal"
      ["geometry"]=>
      object(stdClass)#84 (4) {
        ["bounds"]=>
        object(stdClass)#85 (2) {
          ["northeast"]=>
          object(stdClass)#86 (2) {
            ["lat"]=>
            float(38.845423)
            ["lng"]=>
            float(-8.1296963)
          }
          ["southwest"]=>
          object(stdClass)#87 (2) {
            ["lat"]=>
            float(37.7493345)
            ["lng"]=>
            float(-9.2629752)
          }
        }
        ["location"]=>
        object(stdClass)#88 (2) {
          ["lat"]=>
          float(38.5240933)
          ["lng"]=>
          float(-8.8925876)
        }
        ["location_type"]=>
        string(11) "APPROXIMATE"
        ["viewport"]=>
        object(stdClass)#89 (2) {
          ["northeast"]=>
          object(stdClass)#90 (2) {
            ["lat"]=>
            float(38.845423)
            ["lng"]=>
            float(-8.1296963)
          }
          ["southwest"]=>
          object(stdClass)#91 (2) {
            ["lat"]=>
            float(37.7493345)
            ["lng"]=>
            float(-9.2629752)
          }
        }
      }
      ["place_id"]=>
      string(27) "ChIJaXWtREE2GQ0RDfjDYFSWnKU"
      ["types"]=>
      array(2) {
        [0]=>
        string(27) "administrative_area_level_1"
        [1]=>
        string(9) "political"
      }
    }
    [5]=>
    object(stdClass)#92 (5) {
      ["address_components"]=>
      array(1) {
        [0]=>
        object(stdClass)#93 (3) {
          ["long_name"]=>
          string(8) "Portugal"
          ["short_name"]=>
          string(2) "PT"
          ["types"]=>
          array(2) {
            [0]=>
            string(7) "country"
            [1]=>
            string(9) "political"
          }
        }
      }
      ["formatted_address"]=>
      string(8) "Portugal"
      ["geometry"]=>
      object(stdClass)#94 (4) {
        ["bounds"]=>
        object(stdClass)#95 (2) {
          ["northeast"]=>
          object(stdClass)#96 (2) {
            ["lat"]=>
            float(42.1542048)
            ["lng"]=>
            float(-6.1902091)
          }
          ["southwest"]=>
          object(stdClass)#97 (2) {
            ["lat"]=>
            float(32.40374)
            ["lng"]=>
            float(-31.2753886)
          }
        }
        ["location"]=>
        object(stdClass)#98 (2) {
          ["lat"]=>
          float(39.399872)
          ["lng"]=>
          float(-8.224454)
        }
        ["location_type"]=>
        string(11) "APPROXIMATE"
        ["viewport"]=>
        object(stdClass)#99 (2) {
          ["northeast"]=>
          object(stdClass)#100 (2) {
            ["lat"]=>
            float(42.1542048)
            ["lng"]=>
            float(-6.1902091)
          }
          ["southwest"]=>
          object(stdClass)#101 (2) {
            ["lat"]=>
            float(36.9601695)
            ["lng"]=>
            float(-9.5171107)
          }
        }
      }
      ["place_id"]=>
      string(27) "ChIJ1SZCvy0kMgsRQfBOHAlLuCo"
      ["types"]=>
      array(2) {
        [0]=>
        string(7) "country"
        [1]=>
        string(9) "political"
      }
    }
  }
  ["status"]=>
  string(2) "OK"
}
*/
}