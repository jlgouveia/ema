<?php

//require_once("../cfg/config.inc.php");

class LabelsManager
{

	public static function getLabelValue($type, $lang, $toUpper = '0')
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$response = array();
		$res=BdConn::query("SELECT * FROM labels WHERE type='".strtoupper($type)."' AND langID='".$lang."'");
		$row=BdConn::single();
		
		if($toUpper == "1")
		{
			$obj = mb_convert_case($row['label'],MB_CASE_UPPER, "UTF-8");
		}
		else
		{
			$obj = $row['label'];
		}

		return $obj;
	}


	/*
	*	Author: Jose Luis Gouveia
	*
	*	getLabelsXML: returns a XML format of all enabled Languages
	*
	*/
	public static function getLabelsXML($lang)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}
		$res = mysql_query("SELECT * from labels WHERE langID = '$lang' ORDER by type asc");

		$response = '<?xml version="1.0" encoding="UTF-8" ?><labels>';

		while ($row = mysql_fetch_array($res))
		{

			$response .= '<label id="'.$row['type'].'">'.$row['label'].'</label>';

		}
		$response .= "</labels>";
		return $response;
	}
	/*
	*	Author: Jose Luis Gouveia
	*
	*	getLabels: Returns an Array of LabelVO with the enabled languages
	*
	*/
	public static function getLabels($lang)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}
		$res = BdConn::runSQL("SELECT * from labels WHERE langID = '$lang' ORDER by type asc");

		$response = array();

		foreach ($res as $row)
		{
			$obj = new LabelVO($row);
			$response[] = $obj;
		}
		return $response;
	}

	/*
	*	Author: Jose Luis Gouveia
	*
	*	getLabel: Returns an LabelVO by a given lang and type
	*
	*/
	public static function getLabel($type, $lang = null)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}
		//return "SELECT * from labels WHERE langID = '$lang' AND type='$type'";
		BdConn::query("SELECT * from labels WHERE langID = '$lang' AND type='$type'");
        $row = BdConn::single();

        $obj = new LabelVO($row);
        // $obj->data = self::getLabelList($obj->ID);

        return $obj;
	}
	/*
	*	Author: Jose Luis Gouveia
	*
	*	getLabelFromDictionary: Returns an LabelVO by a given stack and type
	*
	*/
	public static function getLabelFromDictionary($stack, $type)
	{
		$labelVO = new LabelVO();
		for($i = 0; $i < count($stack); ++$i)
		{
			if($type == $stack[$i]->type)
			{
				$labelVO = $stack[$i];
			}
		}
		return $labelVO;

	}

	/*
	*	Author: Jose Luis Gouveia
	*
	*	getAllLabels: Returns an Array of LabelVO
	*
	*/
	public static function getAllLabels()
	{

		$res = mysql_query("SELECT * from labels ORDER by  LangID ASC");

		$response = array();

		while ($row = mysql_fetch_array($res))
		{
			$obj = new LabelVO($row);
			$response[] = $obj;
		}
		return $response;
	}


	/*
	*	Author: Jose Luis Gouveia
	*
	*	mustacheConvertLabels:
	*	@array objects LabelVO $labels
	*/
	public static function mustacheConvertLabels($labels)
	{
		$mustacheLabels = array();

		for($i = 0; $i < count($labels); ++$i)
		{

			$mustacheLabels[$labels[$i]->type] = $labels[$i]->label;

		}
		return $mustacheLabels;
	}
	/*
	*	Author: Jose Luis Gouveia
	*
	*	mustacheConvertObjects:
	*	@array objects VO $objects
	*/
	public static function mustacheConvertObjects($objects, $keyToIndex = null)
	{
		$output = array();
		foreach ($objects as $key => $value)
		{

			$output[($keyToIndex) ? $value->$keyToIndex : $key] = $value;
		}
		return $output;
	}

	/*
	*	Author: Jose Luis Gouveia
	*
	*	getLabelList:
	*	@array
	*/
	public static function getLabelList($ID)
	{
		global $mysqli;

		$res = BdConn::runSQL("SELECT value from labelsLists WHERE labelID = $ID");

        $list = array();
        if (isset($res)) {
            foreach ($res as $row) {
                $list[] = $row['value'];
            }
        }

		return $list;
	}
}

?>