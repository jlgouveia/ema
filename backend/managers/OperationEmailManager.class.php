<?php


	class OperationEmailManager
	{

		public static function getOperationalEmail($message, $userID = null, $lang = null)
		{

				if(!$lang)
				{
					$lang = LanguageManager::getDefaultLanguage()->ID;
				}
				else
				{
					if(!is_numeric($lang))
					{
						$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
					}
				}
/*
				require $_SERVER['DOCUMENT_ROOT'].'/ema/backend/plugins/Mustache/Autoloader.php';
				Mustache_Autoloader::register();

*/



				$labels = LabelsManager::getLabels($lang);
				$settings = SettingsManager::getSettings();
				$cart = StoreManager::getFullCart();
				$cartLabelsMustache = "";
				//return var_dump($cart);
				if($cart->ID)
				{
					$cartLabelsMustache['productsList'] = array();

					for($i = 0; $i < count($cart->products); ++$i)
					{
						$cartLabelsMustache['productsList'][$i]['menuLabel'] = $cart->products[$i]->menuLabel;
						$cartLabelsMustache['productsList'][$i]['title'] = $cart->products[$i]->title;
						$cartLabelsMustache['productsList'][$i]['image'] = $cart->products[$i]->files->imageFiles[0]->file;
						$cartLabelsMustache['productsList'][$i]['price'] = $cart->currencySymbol.$cart->products[$i]->vatPrice;
						$cartLabelsMustache['productsList'][$i]['quantity'] = $cart->products[$i]->quantity;
						$cartLabelsMustache['productsList'][$i]['productNotes'] = $cart->products[$i]->notes;
						$cartLabelsMustache['productsList'][$i]['subtotal'] = $cart->currencySymbol.($cart->products[$i]->quantity * $cart->products[$i]->vatPrice);

					}
					$cartLabelsMustache['totalPrice'] = $cart->currencySymbol.$cart->totalPrice;
					$cartLabelsMustache['totalWeight'] = $cart->totalWeight/1000;
					$cartLabelsMustache['shippingFees'] = $cart->currencySymbol.$cart->shippingFees;
					$cartLabelsMustache['country'] = $cart->country;
					foreach ($cart as $key => $value)
					{
						$cartLabelsMustache[$key] = $value;
					}
				}

				$mustacheConvertLabels = LabelsManager::mustacheConvertLabels($labels);
				$mustacheConvertSettings = LabelsManager::mustacheConvertObjects($settings);
				
				if(!is_array($cartLabelsMustache) ) { $cartLabelsMustache = array(); }
				$labelsMustache = array_merge($mustacheConvertLabels, $mustacheConvertSettings, $cartLabelsMustache);
				
				if($userID)
				{
					$sql = "SELECT * FROM usersWebsite WHERE ID=".$userID;

					$resultset=BdConn::runSQL($sql);
					$user = BdConn::next($resultset);
					if(isset($user['birthday'])) { $birthdayConvert = strtotime($user['birthday']);
						$newBirthday = date('d-m-Y',$birthdayConvert);
						$user['birthday'] = $newBirthday;
					}
					
					$labelsMustache = array_merge($labelsMustache, $user[0]);
				}
				
				$m = new Mustache_Engine();

				$message = $m->render($message, $labelsMustache);
				$labelsMustache['message'] = $message;
				$body = $m->render($settings->operationalEmail, $labelsMustache);

				return $body;
			}


	}

?>