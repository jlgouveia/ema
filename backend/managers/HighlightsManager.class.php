<?php


class HighlightsManager
{



	public static function getHighlights($lang, $limit=NULL, $orderBy = NULL)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$limitQuery = "";
		if($limit)
		{
			$limitQuery = " LIMIT ".$limit;
		}
		$orderQuery = "";
		if($orderBy)
		{
			$orderQuery = " ORDER BY ".$orderBy;
		}
		$sql = "SELECT ".ContentsManager::$contentSelectFields."

		WHERE contentsInfo.langID = '$lang' AND contents.enabled = '1' AND contentsInfo.enabled = '1' AND highlightType!='' 
		GROUP by contentsInfo.contentsUID
		".$orderQuery.$limitQuery;
		
		$res = BdConn::runSql($sql);

		$response = array();
		foreach ($res as $row)
		{
			$contentVO = new ContentVO($row);
			$contentVO->vatPrice = StoreManager::calculateVatRate($contentVO->price, $contentVO->storeVatRateID);
			if(!isset($contentVO->vatPrice))
			{
				$contentVO->vatPrice = $contentVO->price;
			}
			$contentVO->files = FilesManager::getContentFiles($row['UID'], $lang);
			$response[] = $contentVO;

		}

		return $response;
	}




}

?>