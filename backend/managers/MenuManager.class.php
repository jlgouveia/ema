<?php

class MenuManager
{
	public static function getMenu($lang)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}
		$res = BdConn::runSQL("SELECT
		contents.template,
		contents.siteMapLabel,
		contentsInfo.deeplink,
		contentsInfo.ID,
		contentsInfo.langID,
		contentsInfo.menuLabel,
		contentsInfo.title,
		contentsInfo.link,
		contentsInfo.linkTarget,
		contentsInfo.automaticSelectChild,
		contentsInfo.optional1,
		contentsInfo.optional2,
		contentsInfo.optional3,
		contentsInfo.optional4,
		contentsInfo.optional5,
		contentsInfo.optional6,
		contentsInfo.optional7,
		contentsInfo.optional8,
		contentsInfo.optional9,
		contentsInfo.optional10,
		contents.UID
		FROM contents INNER JOIN contentsInfo ON contents.UID = contentsInfo.contentsUID
		INNER JOIN contentsToContents ON contents.UID = contentsToContents.childUID
		WHERE contentsToContents.parentUID = '0' AND contentsInfo.langID = '$lang' AND contents.enabled = '1' AND contentsInfo.enabled = '1' AND contentsToContents.showInMenu = '1'
		ORDER BY contentsToContents.sortOrder ASC


		");

		$response = array();
		foreach ($res as $row)
		{
			$contentVO = new ContentVO($row);

			$contentVO->children = MenuManager::getMenuChildren($row['UID'], $lang);
			if($contentVO->automaticSelectChild > 0)
			{
				$contentVO->deeplink = self::getRootDeeplink($contentVO->UID, $lang);
				//return $contentVO->deeplink;
			}
			$response[] = $contentVO;
		}

		return $response;
	}

	public static function getMenuChildren($UID, $lang = null)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$res= BdConn::runSQL("SELECT
		contents.template,
		contents.locked,
		contentsInfo.deeplink,
		contentsInfo.ID,
		contentsInfo.langID,
		contentsInfo.menuLabel,
		contentsInfo.link,
		contentsInfo.linkTarget,
		contentsInfo.automaticSelectChild,
		contentsInfo.optional1,
		contentsInfo.optional2,
		contentsInfo.optional3,
		contentsInfo.optional4,
		contentsInfo.optional5,
		contentsInfo.optional6,
		contentsInfo.optional7,
		contentsInfo.optional8,
		contentsInfo.optional9,
		contentsInfo.optional10,
		contents.UID
		FROM contents INNER JOIN contentsInfo ON contents.UID = contentsInfo.contentsUID
		INNER JOIN contentsToContents ON contents.UID = contentsToContents.childUID
		WHERE contentsToContents.parentUID = '$UID' AND contentsInfo.langID = '$lang' AND contents.enabled = '1' AND contentsInfo.enabled = '1' AND contentsToContents.showInMenu = '1'
		ORDER BY contentsToContents.sortOrder ASC ");

		$response = array();
		foreach ($res as $row)
		{
			$contentVO = new ContentVO($row);
			if($row['automaticSelectChild'])
			{

				$contentVO->deeplink = self::getRootDeeplink($row['UID'], $lang);
			}

			$contentVO->children = self::getMenuChildren($row['UID'], $lang);
			$response[] = $contentVO;
		}

		return $response;
	}

	public static function getRootDeeplink($UID, $lang)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$res=BdConn::query("SELECT contentsInfo.contentsUID, contentsInfo.automaticSelectChild

		FROM contents INNER JOIN contentsInfo ON contents.UID = contentsInfo.contentsUID
		INNER JOIN contentsToContents ON contents.UID = contentsToContents.childUID
		WHERE contentsInfo.contentsUID = '$UID' AND contentsInfo.langID = '$lang' AND contents.enabled = '1' AND contentsInfo.enabled = '1' AND contentsInfo.automaticSelectChild != '0'");




		$row = BdConn::single();
		
		$children = ContentsManager::getChildren($row['contentsUID'], $lang);



		$output ="";

		if($row['automaticSelectChild'])
		{
			for($i = 0; $i < count($children); ++$i)
			{
				if($i == $row['automaticSelectChild']-1)
				{
					$output = $children[$i]->deeplink;

					if($children[$i]->automaticSelectChild)
					{
						$output = MenuManager::getRootDeeplink($children[$i]->UID, $lang);
					}
				}

			}
		}
		return $output;
	}

	public static function getDeeplinkMenu($lang)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}
		$res=mysql_query("SELECT
		contentsInfo.contentsUID as UID,
		contents.template,
		contents.locked,
		contentsInfo.deeplink,
		contentsInfo.ID,
		contentsInfo.langID,
		contentsInfo.menuLabel,
		contentsInfo.link,
		contentsInfo.linkTarget,
		contentsInfo.automaticSelectChild,
		contentsInfo.optional1,
		contentsInfo.optional2,
		contentsInfo.optional3,
		contentsInfo.optional4,
		contentsInfo.optional5,
		contentsInfo.optional6,
		contentsInfo.optional7,
		contentsInfo.optional8,
		contentsInfo.optional9,
		contentsInfo.optional10,
		contents.UID
		FROM contents INNER JOIN contentsInfo ON contents.UID = contentsInfo.contentsUID
		INNER JOIN contentsToContents ON contents.UID = contentsToContents.parentUID
		WHERE contentsToContents.childUID = '0' AND contentsInfo.langID = '$lang' AND contents.enabled = '1' AND contentsInfo.enabled = '1' AND contentsToContents.showInMenu = '1'
		ORDER BY contentsToContents.sortOrder ASC");

		$ouput = "<ul>";


		while ($row=mysql_fetch_array($res))
		{
			$contentVO = new ContentVO($row);

			$li .= '<li><a href="#/'.$contentVO->deeplink.'/">'.$contentVO->menuLabel.'</a></li>';
			$deeplink = "#/" . $contentVO->deeplink . "/";
			$li .= MenuManager::getDeeplinkMenuChildren($contentVO->UID, $deeplink, $lang);


		}

		$ouput .= $li."</ul>";
		return $ouput;
	}

	private function getDeeplinkMenuChildren($UID, $parentDeeplink, $lang)
	{

		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}		$res = mysql_query("SELECT
		contents.template,
		contents.locked,
		contentsInfo.deeplink,
		contentsInfo.ID,
		contentsInfo.langID,
		contentsInfo.menuLabel,
		contentsInfo.link,
		contentsInfo.linkTarget,
		contentsInfo.automaticSelectChild,
		contentsInfo.optional1,
		contentsInfo.optional2,
		contentsInfo.optional3,
		contentsInfo.optional4,
		contentsInfo.optional5,
		contentsInfo.optional6,
		contentsInfo.optional7,
		contentsInfo.optional8,
		contentsInfo.optional9,
		contentsInfo.optional10,
		contents.UID
		FROM contents INNER JOIN contentsInfo ON contents.UID = contentsInfo.contentsUID
		INNER JOIN contentsToContents ON contents.UID = contentsToContents.childUID
		WHERE contentsToContents.parentUID = '$UID' AND contentsInfo.langID = '$lang' AND contents.enabled = '1' AND contentsInfo.enabled = '1' AND contentsToContents.showInMenu = '1'
		ORDER BY contentsToContents.sortOrder ASC");

		$ouput = "<ul>";


		while ($row=mysql_fetch_array($res))
		{
			$contentVO = new ContentVO($row);

			$deeplink = $parentDeeplink . $contentVO->deeplink . "/";
			$li .= '<li><a href="'. $deeplink .'">'. $contentVO->menuLabel.'</a></li>';
			$li .= MenuManager::getDeeplinkMenuChildren($contentVO->UID,$deeplink, $lang);

		}

		$ouput .= $li."</ul>";
		return $ouput;
	}


	public static function getParent($UID, $isMainParent = false)
	{

		if(!$UID)
		{
			$UID = 0;
		}

		$sql = "SELECT contentsToContents.parentUID as UID
		FROM contentsToContents INNER JOIN contentsInfo ON contentsToContents.childUID = contentsInfo.contentsUID
		WHERE contentsInfo.contentsUID = $UID";
		if($isMainParent) {
			$sql .= " AND contentsToContents.mainParent = 1";
		}
		$resultset=BdConn::runSQL($sql);
		
		return $resultset;
	}

	public static function getSecundaryParents($UID)
	{
		$response = array();
		if(!$UID)
		{
			$UID = 0;
		}

		$sql = "SELECT contentsToContents.parentUID as UID
		FROM contentsToContents INNER JOIN contentsInfo ON contentsToContents.childUID = contentsInfo.contentsUID
		WHERE contentsInfo.contentsUID = $UID AND mainParent = 0";
		$resultset=BdConn::runSQL($sql);
		while ($row=mysql_fetch_array($resultset))
		{
			$response[] = $row['UID'];
		}

		return $response;
	}

	public static function getSiteMap($UID, $lang)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$res=BdConn::runSQL("SELECT
		contents.template,
		contents.locked,
		contentsInfo.deeplink,
		contentsInfo.ID,
		contentsInfo.langID,
		contentsInfo.menuLabel,
		contentsInfo.link,
		contentsInfo.linkTarget,
		contentsInfo.automaticSelectChild,
		contentsInfo.optional1,
		contentsInfo.optional2,
		contentsInfo.optional3,
		contentsInfo.optional4,
		contentsInfo.optional5,
		contentsInfo.optional6,
		contentsInfo.optional7,
		contentsInfo.optional8,
		contentsInfo.optional9,
		contentsInfo.optional10,
		contents.UID
		FROM contents INNER JOIN contentsInfo ON contents.UID = contentsInfo.contentsUID
		INNER JOIN contentsToContents ON contents.UID = contentsToContents.childUID
		WHERE contentsToContents.parentUID = '$UID' AND contentsInfo.langID = '$lang' AND contents.enabled = '1' AND contentsInfo.enabled = '1'
		ORDER BY contentsToContents.sortOrder ASC");

		$response = array();
		foreach ($res as $row)
		{
			$contentVO = new ContentVO($row);
			if($row['automaticSelectChild'])
			{
				$contentVO->deeplink = MenuManager::getRootDeeplink($row['UID'], $lang);
			}
			$contentVO->children = MenuManager::getMenuChildren($row['UID'], $lang);
			$response[] = $contentVO;
		}

		return $response;
	}
	public static function getGoogleSitemap($lang=NULL)
	{
		$siteURL = SettingsManager::getSiteURL();
		$langWhere = "";
		if(isset($lang))
		{
			$langWhere = " AND contentsInfo.langID = $lang ";
		}
		$res=BdConn::runSQL("SELECT

		contentsInfo.langID,
		contentsInfo.deeplink,
		contentsInfo.link,
		contentsInfo.linkTarget,
		contentsInfo.automaticSelectChild,
		contents.UID
		FROM contents INNER JOIN contentsInfo ON contents.UID = contentsInfo.contentsUID
		INNER JOIN contentsToContents ON contents.UID = contentsToContents.childUID
		WHERE  contents.enabled = '1' AND contentsInfo.enabled = '1' AND contents.searchable = '1'".$langWhere.
		"ORDER BY contentsInfo.langID ASC
		");

		$response = array();
		foreach ($res as $row)
		{
			$contentVO = new ContentVO($row);
			$contentVO->deeplink = $siteURL.$contentVO->deeplink;
			if($contentVO->automaticSelectChild)
			{
				$contentVO->deeplink = $siteURL.MenuManager::getRootDeeplink($row['UID'], $contentVO->langID);
			}
			if($contentVO->link)
			{
				//$contentVO->deeplink = $contentVO->link;
			}
			$response[] = $contentVO;
		}

		return $response;
	}

	public static function getBreadcrumbs($UID, $lang)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}
		$parents =  array();
		$parentsList = self::getParent($UID);
		// return $parentsList[0]['UID'];
		while(count($parentsList) > 0) {

			if($parentsList[0]['UID'] != 0) {
				$parents[] = $parentsList[0]['UID'];
			}
			$parentsList = self::getParent($parentsList[0]['UID']);
		}

		for($i = 0; $i < count($parents); $i++) {
			$content = new ContentVO();
			$content->UID = $parents[$i];
			$content->deeplink = ContentsManager::getDeeplink($parents[$i], $lang);
			$content->langID = $lang;
			$content->menuLabel = ContentsManager::getMenuLabel($parents[$i], $lang);
			$content->title = ContentsManager::getTitle($parents[$i], $lang);
			
			$parents[$i] = $content;
		}
		return $parents;
		
	}
	public static function buildHTMLMenu($menu, $ulClass = "", $liClass = "", $aClass = "")
	{
		if(count($menu))
		{
			$class = "";
			if($ulClass)
			{
				$class = ' class="'.$ulClass.'"';
			}

			$html = "<ul$class>";
			for($i = 0; $i < count($menu); $i++)
			{

				$html .= "<li class='$liClass'><a class='$aClass' href='/".$menu[$i]->deeplink."'>".$menu[$i]->menuLabel."</a>";
				if(isset($menu[$i]->children))
				{
					$html .= self::buildHTMLMenu($menu[$i]->children);
				}
				$html .= '</li>';
			}
			$html .= "</ul>";
			return $html;
		}
	}

}

?>