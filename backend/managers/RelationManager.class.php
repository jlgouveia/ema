<?php


class RelationManager
{
	
	/**
	*
	* 
	* @param int (ID)
	* @return Array
	*/
	public static function getFilesFromUser($userID)
	{
		$checkUser = UserManager::getUser($userID);
		if(isset($checkUser->ID))
		{
			$res= BdConn::runSQL("SELECT * FROM relations WHERE parentID = $userID AND parentTable = 'usersWebsite' AND childTable = 'files'");
			
			$response = array();
			foreach ($res as $row) {
				$vo = FilesManager::getFile($row['childID']);
				$response[] = $vo;
			}
	
			return $response;
		}
		else {
			return false;
		}
	}
	
	/**
	*
	* 
	* @param int (ID)
	* @return Array
	*/
	public static function getUsersFromUser($userID)
	{
		$checkUser = UserManager::getUser($userID);

		if(isset($checkUser->ID))
		{
			$res= BdConn::runSQL("SELECT childID FROM relations WHERE parentID = $userID AND parentTable = 'usersWebsite' AND childTable = 'usersWebsite'");
			
			$response = array();
			foreach ($res as $row) {	
				$vo = UserManager::getUser($row['childID']);
				$response[] = $vo;
			}
	
			return $response;
		}
		else {
			return false;
		}
	}

	/**
	*
	* 
	* @param int (UID)
	* @return Array
	*/
	public static function getUsersFromContent($UID, $asIdsArray = false)
    {
    	
    	$response = array();
    	if($UID) {
	        $sql = "SELECT * FROM relations WHERE relations.parentID = $UID AND relations.parentTable = 'contents' AND relations.childTable = 'usersWebsite'";
	        BdConn::query($sql);
	        $res = BdConn::resultset();

	       
	        if ($res) {
	            foreach ($res as $row) {

	                $vo = UserManager::getUser($row['childID']);
	                if ($asIdsArray) {
	                    $response[] = $vo->ID;
	                } else {
	                    $response[] = $vo;
	                }
	            }
	        }
        }
        return $response;
    }
	
	
	/**
	*
	* 
	* @param int (UID)
	* @return Array
	*/
	public static function setUserToContent($UID, $userID, $sortOrder = 0)
	{
		// VERIFICAR SE JÁ EXISTE A RELAÇÃO COM OS MESMOS DADOS
		$sql = "SELECT ID FROM relations WHERE parentTable = 'contents' AND parentID = $UID AND childTable = 'usersWebsite' AND childID = $userID ";
		
		$resultset=BdConn::query($sql);
		$resultset = BdConn::single();
		
		if (!$resultset)
		{
			return BdConn::runSQL("INSERT INTO relations (parentTable, parentID, childTable, childID, sortOrder) VALUES ('contents', $UID, 'usersWebsite', $userID, $sortOrder)");
		}
		else {
			return true;
		}
		return false;
	}
	
	/**
	*
	* 
	* @param int (UID, userID)
	* @return boolean
	*/
	public static function deleteUsersFromContent($UID, $userID)
	{	
		$ema = EmaBrain::Instance();
		
		if($ema::$ALLOW_ADD_CONTENT_NO_SESSION)
		{
			if(is_array($userID)) {
				for($i = 0; $i < count($userID); $i++) {
					$id = $userID[$i];
					$res= BdConn::runSQL("DELETE FROM relations WHERE parentID = $UID AND parentTable = 'contents' AND childTable = 'usersWebsite' AND childID=$id");
					if(!$res) {
						return false;
					}
				}
			}
			else {
				$res= mysql_query("DELETE FROM relations WHERE parentID = $UID AND parentTable = 'contents' AND childTable = 'usersWebsite' AND childID=$userID");
				if(!$res) {
					return false;
				}
			}
		
			return true;
		}
		return false;
	}
	
	/**
	*
	* 
	* @param int (UID)
	* @return boolean
	*/
	public static function deleteAllUsersFromContent($UID)
	{	
		$ema = EmaBrain::Instance();
		
		if($ema::$ALLOW_ADD_CONTENT_NO_SESSION)
		{
			
			$res= BdConn::runSQL("DELETE FROM relations WHERE parentID = $UID AND parentTable = 'contents' AND childTable = 'usersWebsite'");
			if(!$res) {
				return false;
			}
		}
		return true;
	}

}