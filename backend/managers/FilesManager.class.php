<?php


class FilesManager
{

	public static $imageExtensions = array(".jpg", ".png", ".gif", ".svg");
	public static $videoExtensions = array(".mts", ".avi", ".mpeg", ".mp4", ".wmv",".mov", ".flv", ".swf", ".m4v", ".ogv", ".webm");
	public static $audioExtensions = array(".aac", ".aif", ".iff", ".m3u", ".mid", ".mp3", ".mpa", ".ra", ".wav", ".wma");
	public static $otherExtensions = array(".doc", ".docx", ".log", ".msg", ".pages", ".rtf", ".txt", ".wpd", ".wps", ".csv", ".xml", ".pps", ".ppt", ".pptx", ".zip", ".pdf");
	public static $responsiveSizes = array("small", "medium", "large");

	public static function getContentFiles($UID, $lang)
	{



		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		if(!$UID)
		{
			$UID = "1";
		}

		$res = BdConn::runSQL("SELECT
		files.ID,
		files.file,
		files.thumb,
		files.videoType,
		files.videoID,
		filesToContents.title,
		filesToContents.subtitle,
		filesToContents.body,
		filesToContents.optional2,
		filesToContents.optional1,
		filesToContents.optional3,
		filesToContents.optional4,
		filesToContents.default,
		filesToContents.sortOrder
		FROM filesToContents INNER JOIN files ON files.ID = filesToContents.fileID
		WHERE filesToContents.contentsUID = '$UID' AND filesToContents.langID = '$lang' AND filesToContents.enabled = '1'
		ORDER BY filesToContents.sortOrder ASC");

		$imageFiles= array();
		$videoFiles= array();
		$audioFiles= array();
		$otherFiles= array();
		$allFiles= array();
		$currentFilesExtensions = array();
		$default = null;



		foreach ($res as $row)
		{
			$fileVO = new FileVO($row);
			$fileVO->extension = UtilsManager::getFileExtension($fileVO->file);

			if($row['default'])
			{
				//$default = $fileVO;
			}
			$fileExtension = UtilsManager::getFileExtension(strtolower($fileVO->file));
			if(in_array($fileExtension, self::$imageExtensions))
			{
				$imageFiles[] = $fileVO;
				if(!in_array($fileExtension, $currentFilesExtensions)){
					$currentFilesExtensions[] = $fileExtension;
				}

			}

			if(in_array($fileExtension, self::$videoExtensions))
			{
				$videoFiles[] = $fileVO;
				if(!in_array($fileExtension, $currentFilesExtensions)){
					$currentFilesExtensions[] = $fileExtension;
				}
			}

			if(in_array($fileExtension, self::$audioExtensions))
			{
				$audioFiles[] = $fileVO;
				if(!in_array($fileExtension, $currentFilesExtensions)){
					$currentFilesExtensions[] = $fileExtension;
				}
			}

			if(in_array($fileExtension, self::$otherExtensions))
			{
				$otherFiles[] = $fileVO;
				if(!in_array($fileExtension, $currentFilesExtensions)){
					$currentFilesExtensions[] = $fileExtension;
				}
			}



			$allFiles[] = $fileVO;
		}

		if(!isset($default))
		{
			if(count($otherFiles))
			{
				$default = $otherFiles[0];
			}
			if(count($audioFiles))
			{
				$default = $audioFiles[0];
			}
			if(count($videoFiles))
			{
				$default = $videoFiles[0];
			}
			if(count($imageFiles))
			{
				$default = $imageFiles[0];
			}
		}
		
		$response = new FilesVO();

		

		
		$response->imageFiles = $imageFiles;
		$response->videoFiles = $videoFiles;
		$response->audioFiles = $audioFiles;
		$response->otherFiles = $otherFiles;
		$response->allFiles = $allFiles;


		// ADD OPTIONALS ALBUNS START
		$optionalsTotal = 10;
		$optionalFiles = array();
		$files = $response->allFiles;

		for($i = 0; $i < $optionalsTotal; $i++) {
			
			
			
					
					$optionalFiles = array();
					for($j = 0; $j < count($files); $j++)
					{
						$fileVO = $files[$j];
						if(isset($fileVO->{'optional'.($i+1)})) {
				
							if($fileVO->{'optional'.($i+1)} != "") {
								
								$optional = $fileVO->{'optional'.($i+1)};
								if($files[$j]->optional1 == $optional || $files[$j]->optional2 == $optional || $files[$j]->optional3 == $optional || $files[$j]->optional4 == $optional)
								{
									$optionalFiles[] = $files[$j];
								}
							}
							$response->createProperty($fileVO->{'optional'.($i+1)}, $optionalFiles);
						}
				
			} 
		}	

		// ADD OPTIONALS ALBUNS END
		sort($currentFilesExtensions);
		$response->currentFilesExtensions = $currentFilesExtensions;

		$response->default = $default;


		return $response;


	}

	public static function getFileFromOptional($contentVO, $optional)
	{
		$fileVO = $contentVO->files->default;
		$files = $contentVO->files->allFiles;
		$response = array();
		for($i = 0; $i < count($files); $i++)
		{
			if($files[$i]->optional1 == $optional || $files[$i]->optional2 == $optional || $files[$i]->optional3 == $optional || $files[$i]->optional4 == $optional)
			{
				$response[] = $files[$i];
			}
		}

		if(count($response) > 0)
		{
			return $response;
		}
		else
		{
			return false;
		}
		$fileVO->extension = UtilsManager::getFileExtension($fileVO->file);
		return $fileVO;
	}
	public static function getFile($ID)
	{
		$sql = ("SELECT
		*
		FROM files
		WHERE files.ID = $ID");

		
		$resultset=BdConn::runSQL($sql);
		$file = BdConn::next($resultset);
		$fileVO = new FileVO($file);
		$fileVO->extension = UtilsManager::getFileExtension($fileVO->file);
		
		return $fileVO;
	}

	public static function getFileByDeeplink($deeplink)
	{
		$sql = ("SELECT
		*
		FROM files
		WHERE files.deeplink = '$deeplink'");

		
		$resultset=BdConn::runSQL($sql);
		$file = BdConn::next($resultset);
		$fileVO = new FileVO($file);
		$fileVO->extension = UtilsManager::getFileExtension($fileVO->file);
		
		return $fileVO;
	}

	public function getAllFiles($limit = null) {
		$limitQuery = "";
		if($limit)
		{
			$limitQuery = " LIMIT ".$limit;
		}
		$res=BdConn::runSQL("SELECT
		*,
		label as title
		FROM files ORDER by date_added DESC $limitQuery");
		$imageFiles= array();

		foreach ($res as $row)
		{
			$fileVO = new FileVO($row);
			$fileVO->extension = UtilsManager::getFileExtension($fileVO->file);
			$imageFiles[] = $fileVO;
		}
		return $imageFiles;
	}

	public static function getResponsiveImageFile($fileName)
	{
		$returnedSizes = [];

		for($i = 0; $i < count(self::$responsiveSizes); $i++) {
			$fileNameSized = preg_replace('/(\.gif|\.jpg|\.png)/', '-'.self::$responsiveSizes[$i].'$1', $fileName);
			// return $fileNameSized;
			if(file_exists ($_SERVER["DOCUMENT_ROOT"].$fileNameSized)) {
				$size = new stdClass();
				$size->size = self::$responsiveSizes[$i];
				$size->file = $fileNameSized;
				$returnedSizes[] = $size;
			} else {
				$size = new stdClass();
				$size->size = self::$responsiveSizes[$i];
				$size->file = $fileName;
				$returnedSizes[] = $size;
			}
		}
		// Touch file if exists, make default
		return $returnedSizes;
		// touch reponsive variants, make array

		// create tag

	}

	public static function getFoundationInterchangeImageTag($html) {
		$dom = new DOMDocument();
		// CLEAN WARNINGS !?!?
		libxml_use_internal_errors(true);
		$dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
		// CLEAN WARNINGS !?!?
		libxml_clear_errors();
		foreach ($dom->getElementsByTagName('img') as $image) {
			$siteUrl = SettingsManager::getSiteURL();
			$src = str_replace($siteUrl, "", $image->getAttribute('src'));
			$image->setAttribute('src', $src);
			
			$old_src = $image->getAttribute('src');
			// $image->setAttribute('src', $new_src);
			$image->setAttribute('data-src', $old_src);
			$responsive = FilesManager::getResponsiveImageFile($old_src);
			if(count($responsive)) {
				$interchange = "";
				for($i = 0; $i < count($responsive); $i++) {
					$interchange .= "[".$responsive[$i]->file.", ".$responsive[$i]->size."]";
					if($i < count($responsive)-1) {
						
						$interchange .= ",";
					}
				}
				$image->setAttribute('data-interchange', $interchange);
				
			}
			
		}

		return $dom->saveHTML();
	}
}