<?php

class AuthorizationManager
{
	public static function authorize($class, $function, $argsArray = null)
	{
		if (!AuthorizationManager::hasAuthorization($class, $function, $argsArray))
		{
			throw new Exception("User has no authorization to access $class.$function");
		}
		return true;
	}

	public static function hasAuthorization()
	{

		if(!isset($_SESSION["userID"]))
		{
			return false;
		}

//		$user = UserManager::getUser($_SESSION['userID']);
//
//		if ($user->isAdmin)
//		{
//			return true;
//		}

		/*if ($class == 'TaskService')
		{
			switch ($function)
			{
				case 'add':
					$task = $argsArray;
					return UserManager::isUserConnectedToProject($user->userID, $task->projectID);
				case 'addComment':
					$comment = $argsArray;
					return UserManager::isUserConnectedToProject($user->userID, TaskManager::getProjectIDByTaskID($comment->taskID));
				case 'getComments':
					$taskID = $argsArray;
					return UserManager::isUserConnectedToProject($user->userID, TaskManager::getProjectIDByTaskID($taskID));
				case 'set': // Only admins can change tasks
				case 'deleteComment':
				case 'delete':
					return false;
			}
		}
		*/

		return true;

	}
	public static function hasUserAccessToContent($userID, $contentUID)
	{
		$sql = "SELECT contentsRolesPermissions.contentsUID
		FROM usersRoles INNER JOIN rolesToUsers ON usersRoles.ID = rolesToUsers.usersRolesID
		INNER JOIN contentsRolesPermissions ON contentsRolesPermissions.usersRolesID = usersRoles.ID
		INNER JOIN usersWebsite ON usersWebsite.ID = rolesToUsers.websiteUsersID
		WHERE contentsRolesPermissions.contentsUID = '".$contentUID."' AND usersWebsite.ID = '".$userID."' group by contentsUID";



		BdConn::query($sql);
        $resultset = BdConn::single();

        if (!$resultset) {
            return false;
        }
		return true;

	}
}

?>