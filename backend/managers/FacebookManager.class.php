<?php


class FacebookManager
{

	/*
	*
	*
	*	FACEBOOK SETTINGS
	*
	*
	*/

	public static function getAppID()
	{
		$res = mysql_query("SELECT
		facebookSettings.appID

		FROM facebookSettings");
		$row = mysql_fetch_array($res);


		$value = $row['appID'];

		return $value;
	}

	public static function getAppSecret()
	{
		$res = mysql_query("SELECT
		facebookSettings.appSecret

		FROM facebookSettings");
		$row = mysql_fetch_array($res);


		$value = $row['appSecret'];

		return $value;
	}

	public static function getAppURL()
	{
		$res = mysql_query("SELECT
		facebookSettings.appURL

		FROM facebookSettings");
		$row = mysql_fetch_array($res);


		$value = $row['appURL'];

		return $value;
	}

	public static function getPageID()
	{
		$res = mysql_query("SELECT
		facebookSettings.pageID

		FROM facebookSettings");
		$row = mysql_fetch_array($res);


		$value = $row['pageID'];

		return $value;
	}

	public static function getShareMessage()
	{
		$res = mysql_query("SELECT
		facebookSettings.shareMessage

		FROM facebookSettings");
		$row = mysql_fetch_array($res);


		$value = $row['shareMessage'];

		return $value;
	}

	public static function getRegulation()
	{
		$res = mysql_query("SELECT
		facebookSettings.regulation

		FROM facebookSettings");
		$row = mysql_fetch_array($res);


		$value = $row['regulation'];

		return $value;
	}

	/*
	*
	*
	*	FACEBOOK SETTINGS
	*
	*
	*/
	public static function isParticipationValid($facebookID)
	{
		$sql = "SELECT * FROM usersWebsite WHERE facebookID = '$facebookID'";
		$obj;
		$resultset=BdConn::runSQL($sql);
		if (mysql_num_rows($resultset) != 1)
		{
			$obj->error = 0;
			$onj->message = LabelsManager::getLabelValue("FACEBOOKPARTICIPATIONSUCCESS",1);
			return $obj;
		}
		$obj->error = 1;
		$obj->message = LabelsManager::getLabelValue("FACEBOOKALREADYPARTICIPATE",1);
		return $obj;
	}

	public static function setImageParticipation($userVO)
	{

		$userVO = new UserVO($userVO);
		$birthday = $userVO->birthday;
		$birthday = explode("/", $birthday);
		$birthday = $birthday[2]."-".$birthday[0]."-".$birthday[1];
		$userVO->birthday = $birthday;



		if($userVO->facebookID)
		{
			UserManager::registerUser($userVO, 1, false);
			$obj->error = 0;
			$obj->message = LabelsManager::getLabelValue("FACEBOOKPARTICIPATIONSUCCESS",1);
			return $obj;
		}
		else
		{
			$obj->error = 1;
			$obj->message = LabelsManager::getLabelValue("FACEBOOKNOVALIDID",1);
			return $obj;
		}
	}
	public static function getTopTen($facebookID)
	{

		$query = mysql_query("SELECT

		facebookVotes.participantFacebookUserID,
		count(facebookVotes.participantFacebookUserID) AS totalPoints,
		usersWebsite.optional1 AS image,
		usersWebsite.firstName,
		usersWebsite.optional5 as description,
		usersWebsite.lastName,
		CONCAt(usersWebsite.firstName,' ',usersWebsite.lastName) as fullName

		FROM facebookVotes INNER JOIN usersWebsite ON facebookVotes.participantFacebookUserID = usersWebsite.facebookID
		GROUP BY participantFacebookUserID
		ORDER BY totalPoints DESC LIMIT 10");

		$response = array();
		$rank = 1;
		while ($row=mysql_fetch_array($query))
		{
			$facebookScoreVO = new FacebookScoreVO($row);
			$facebookScoreVO->rank = $rank;
			$facebookScoreVO->voted = self::gotMyVote($row['participantFacebookUserID'],$facebookID);
			$response[] = $facebookScoreVO;
			$rank++;
		}
		return $response;
	}
	public static function getTopThree($facebookID)
	{

		$query = mysql_query("SELECT

		facebookVotes.participantFacebookUserID,
		count(facebookVotes.participantFacebookUserID) AS totalPoints,
		usersWebsite.optional1 AS image,
		usersWebsite.firstName,
		usersWebsite.optional5 as description,
		usersWebsite.lastName,
		CONCAt(usersWebsite.firstName,' ',usersWebsite.lastName) as fullName

		FROM facebookVotes INNER JOIN usersWebsite ON facebookVotes.participantFacebookUserID = usersWebsite.facebookID
		GROUP BY participantFacebookUserID
		ORDER BY totalPoints DESC LIMIT 3");

		$response = array();
		$rank = 1;
		while ($row=mysql_fetch_array($query))
		{
			$facebookScoreVO = new FacebookScoreVO($row);
			$facebookScoreVO->rank = $rank;
			$facebookScoreVO->voted = self::gotMyVote($row['participantFacebookUserID'],$facebookID);
			$response[] = $facebookScoreVO;
			$rank++;
		}
		return $response;
	}
	public static function getAllParticipations($facebookID)
	{

		$query = mysql_query("
		SELECT

		usersWebsite.facebookID as participantFacebookUserID,
		count(facebookVotes.participantFacebookUserID) AS totalPoints,
		usersWebsite.optional1 AS image,
		usersWebsite.optional5 as description,
		usersWebsite.firstName,
		usersWebsite.lastName,
		CONCAt(usersWebsite.firstName,' ',usersWebsite.lastName) as fullName

		FROM usersWebsite left JOIN  facebookVotes ON usersWebsite.facebookID = facebookVotes.participantFacebookUserID
		GROUP BY usersWebsite.facebookID
		ORDER BY usersWebsite.subscribeDate DESC");

		$response = array();
		while ($row=mysql_fetch_array($query))
		{
			$facebookScoreVO = new FacebookScoreVO($row);
			$facebookScoreVO->voted = self::gotMyVote($row['participantFacebookUserID'],$facebookID);
			$response[] = $facebookScoreVO;
		}
		return $response;
	}
	public static function getAllParticipationsUnlimited($facebookID)
	{

		$query = mysql_query("
		SELECT

		usersWebsite.facebookID as participantFacebookUserID,
		count(facebookVotes.participantFacebookUserID) AS totalPoints,
		usersWebsite.optional1 AS image,
		usersWebsite.firstName,
		usersWebsite.optional5 as description,
		usersWebsite.lastName,
		CONCAt(usersWebsite.firstName,' ',usersWebsite.lastName) as fullName

		FROM usersWebsite left JOIN  facebookVotes ON usersWebsite.facebookID = facebookVotes.participantFacebookUserID
		GROUP BY usersWebsite.facebookID
		ORDER BY usersWebsite.subscribeDate DESC");

		$response = array();
		while ($row=mysql_fetch_array($query))
		{
			$facebookScoreVO = new FacebookScoreVO($row);
			$facebookScoreVO->voted = self::gotMyVote($row['participantFacebookUserID'],$facebookID);
			$response[] = $facebookScoreVO;
		}
		return $response;
	}

	public static function getParticipation($participantFacebookUserID, $facebookID = NULL)
	{

		$query = mysql_query("
		SELECT

		usersWebsite.facebookID as participantFacebookUserID,
		count(facebookVotes.participantFacebookUserID) AS totalPoints,
		usersWebsite.optional1 AS image,
		usersWebsite.firstName,
		usersWebsite.lastName,
		usersWebsite.optional5 as description,
		usersWebsite.email,
		CONCAt(usersWebsite.firstName,' ',usersWebsite.lastName) as fullName

		FROM usersWebsite left JOIN  facebookVotes ON usersWebsite.facebookID = facebookVotes.participantFacebookUserID
		WHERE usersWebsite.facebookID ='$participantFacebookUserID'
		GROUP BY usersWebsite.facebookID
		ORDER BY usersWebsite.subscribeDate DESC");


		$row=mysql_fetch_array($query);

		$facebookScoreVO = new FacebookScoreVO($row);
		$facebookScoreVO->voted = self::gotMyVote($row['participantFacebookUserID'],$facebookID);


		return $facebookScoreVO;
	}

	public static function vote($participantFacebookUserID, $voterFacebookUserID)
	{
		date_default_timezone_set("Europe/London");
		$date = date('y-m-d H:i:s');

		if($voterFacebookUserID != $participantFacebookUserID)
		{

			if($voterFacebookUserID != 0 && $participantFacebookUserID != 0)
			{


				$sql = "SELECT * FROM facebookVotes WHERE participantFacebookUserID = '$participantFacebookUserID' AND voterFacebookUserID = '$voterFacebookUserID'";
				$obj;
				$resultset=BdConn::runSQL($sql);

				$participation = new FacebookScoreVO();
				$participation = self::getParticipation($participantFacebookUserID);

				if (mysql_num_rows($resultset) != 1)
				{

					$query = mysql_query("INSERT INTO facebookVotes (participantFacebookUserID, voterFacebookUserID, date) VALUES ('$participantFacebookUserID', '$voterFacebookUserID', '$date')");
					if($query)
					{
						$participation = new FacebookScoreVO();
						$participation = self::getParticipation($participantFacebookUserID);
						$facebookVotingSettings = new FacebookVotingSettingsVO();
						$facebookVotingSettings = self::getFacebookVotingSettings();
						if($participation->totalPoints == $facebookVotingSettings->firstVotesIndicator)
						{
							if(self::getWinnersCount() < $facebookVotingSettings->participantsThatWin)
							{
								self::setWinner($participantFacebookUserID);
							}
						}
						$obj->error = 0;
						$obj->message = LabelsManager::getLabelValue("FACEBOOKTHANKYOU",1);

						$obj->currentVotes = $participation->totalPoints;
						return $obj;

					}
					else
					{
						$obj->error = 1;
						$obj->message = LabelsManager::getLabelValue("FACEBOOKERRORVOTING",1);
						return $obj;
					}
				}
				else
				{
					$sql = "DELETE FROM facebookVotes WHERE participantFacebookUserID = '$participantFacebookUserID' AND voterFacebookUserID = '$voterFacebookUserID'";
					$obj;
					$resultset=BdConn::runSQL($sql);
					$participation = new FacebookScoreVO();
					$participation = self::getParticipation($participantFacebookUserID);
					$obj->error = 2;
					$obj->message = "Voto retirado";
					$obj->currentVotes = $participation->totalPoints;
					return $obj;
				}
			}
			else
			{
				$participation = new FacebookScoreVO();
				$participation = self::getParticipation($participantFacebookUserID);
				$obj->error = 3;
				$obj->message = LabelsManager::getLabelValue("FACEBOOKNULLVOTEAUTH",1);
				$obj->currentVotes = $participation->totalPoints;
				return $obj;
			}

		}
		else
		{
			$participation = new FacebookScoreVO();
			$participation = self::getParticipation($participantFacebookUserID);
			$obj->error = 1;
			$obj->message = LabelsManager::getLabelValue("FACEBOOKNULLVOTESELF",1);
			$obj->currentVotes = $participation->totalPoints;
			return $obj;
		}
	}

	private function setWinner($participantFacebookID)
	{
		date_default_timezone_set("Europe/London");

		$date = date('y-m-d H:i:s');
		$query = mysql_query("INSERT INTO facebookVotingWinners (facebookID, date) VALUES ('$participantFacebookID','$date')");
		$userParticipation = self::getParticipation($participantFacebookID);

		if($query)
		{
			SendEmailManager::sendEmail($userParticipation->email, SettingsManager::getTitle(), "", SettingsManager::getAdminName(), SettingsManager::getGlobalEmail());

			SendEmailManager::sendEmail("me@joseluisgouveia.com", SettingsManager::getTitle(), $userParticipation->email, SettingsManager::getAdminName(), SettingsManager::getGlobalEmail());
			SendEmailManager::sendEmail("csaraiva@thegency.pt", SettingsManager::getTitle(), $userParticipation->email, SettingsManager::getAdminName(), SettingsManager::getGlobalEmail());
			SendEmailManager::sendEmail("anoronha@thegency.pt", SettingsManager::getTitle(), $userParticipation->email, SettingsManager::getAdminName(), SettingsManager::getGlobalEmail());
		}
	}

	private function getWinnersCount()
	{


		$query = mysql_query("SELECT count(facebookID) as totalWinners FROM facebookVotingWinners");
		$row=mysql_fetch_array($query);
		$totalWinners = $row['totalWinners'];
		return $totalWinners;

	}
	public static function getFacebookVotingSettings()
	{
		$query = mysql_query("SELECT * FROM facebookVotingSettings");
		$row=mysql_fetch_array($query);
		$facebookVotingSettings = new FacebookVotingSettingsVO($row);
		return $facebookVotingSettings;
	}
	private function gotMyVote($participantFacebookUserID, $voterFacebookUserID)
	{
		$sql = "SELECT * FROM facebookVotes WHERE participantFacebookUserID = '$participantFacebookUserID' AND voterFacebookUserID = '$voterFacebookUserID'";
		$obj;
		$resultset=BdConn::runSQL($sql);
		if (mysql_num_rows($resultset) != 1)
		{
			return false;
		}
		return true;
	}
	public static function search($search, $facebookID)
	{

		$query = mysql_query("
		SELECT usersWebsite.facebookID AS participantFacebookUserID,

		usersWebsite.optional1 AS image,
		usersWebsite.firstName,
		usersWebsite.lastName,
		CONCAt(usersWebsite.firstName,' ',usersWebsite.lastName) AS fullName
		FROM usersWebsite
		WHERE LOWER(usersWebsite.firstName ) like LOWER('%$search%') OR LOWER(usersWebsite.lastName ) like LOWER('%$search%')
		ORDER BY usersWebsite.subscribeDate DESC");

		$response = array();
		while ($row=mysql_fetch_array($query))
		{
			$facebookScoreVO = new FacebookScoreVO($row);
			$facebookScoreVO->voted = self::gotMyVote($row['participantFacebookUserID'],$facebookID);
			$facebookScoreVO->totalPoints = self::getVotes($row['participantFacebookUserID']);
			$response[] = $facebookScoreVO;
		}
		return $response;
	}
	private function getVotes($participantFacebookUserID)
	{
		$query = mysql_query("
		SELECT count(facebookVotes.participantFacebookUserID) as totalPoints FROM facebookVotes WHERE facebookVotes.participantFacebookUserID = '$participantFacebookUserID'");
		$row=mysql_fetch_array($query);
		return $row['totalPoints'];
	}

}

?>