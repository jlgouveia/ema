<?php


class ContentsManager
{

	public static $contentSelectFields = " contents.template,
		contentsTemplates.label as templateLabel,
		contents.isHome,
		contents.locked,
		contentsInfo.deeplink,
		contentsInfo.ID,
		contentsInfo.langID,
		contentsInfo.menuLabel,
		contentsInfo.title,
		contentsInfo.subtitle,
		contentsInfo.body,
		contentsInfo.date,
		contentsInfo.lead,
		contentsInfo.author,
		contentsInfo.location,
		contentsInfo.startDate,
		contentsInfo.endDate,
		contentsInfo.price,
		contentsInfo.quantity,
		contentsInfo.weight,
		contentsInfo.storeVatRateID,
		contentsInfo.link,
		contentsInfo.linkTarget,
		contentsInfo.optional1,
		contentsInfo.optional2,
		contentsInfo.optional3,
		contentsInfo.optional4,
		contentsInfo.optional5,
		contentsInfo.optional6,
		contentsInfo.optional7,
		contentsInfo.optional8,
		contentsInfo.optional9,
		contentsInfo.optional10,
		contentsInfo.highlightType,
		contentsInfo.automaticSelectChild,
		contentsInfo.urlTitle,
		contentsInfo.metaDescription,
		contentsInfo.metaKeywords,
		contents.UID,
		contents.userParentID,
		contents.enabled

		FROM contents INNER JOIN contentsInfo ON contents.UID = contentsInfo.contentsUID
		INNER JOIN contentsTemplates ON contents.template = contentsTemplates.template INNER JOIN contentsToContents ON contents.UID = contentsToContents.childUID ";
	
		public static $contentSelectFieldsAlias = " c.template,
		ct.label as templateLabel,
		c.isHome,
		c.locked,
		ci.deeplink,
		ci.ID,
		ci.langID,
		ci.menuLabel,
		ci.title,
		ci.subtitle,
		ci.body,
		ci.date,
		ci.lead,
		ci.author,
		ci.location,
		ci.startDate,
		ci.endDate,
		ci.price,
		ci.quantity,
		ci.weight,
		ci.storeVatRateID,
		ci.link,
		ci.linkTarget,
		ci.optional1,
		ci.optional2,
		ci.optional3,
		ci.optional4,
		ci.optional5,
		ci.optional6,
		ci.optional7,
		ci.optional8,
		ci.optional9,
		ci.optional10,
		ci.highlightType,
		ci.automaticSelectChild,
		ci.urlTitle,
		ci.metaDescription,
		ci.metaKeywords,
		c.UID,
		c.userParentID,
		c.enabled

		FROM contents c 
        INNER JOIN contentsInfo ci 
        ON c.UID = ci.contentsUID
		INNER JOIN contentsTemplates ct
        ON c.template = ct.template 
        INNER JOIN contentsToContents cc 
        ON c.UID = cc.childUID ";

	public static function getContent($uid, $lang = NULL, $children = false, $childrenUsers = true, $getUserParentObject = true)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$filesManager = new FilesManager;

		$res = BdConn::query("SELECT".self::$contentSelectFields." WHERE contents.UID = '$uid' AND contentsInfo.langID = '$lang'");
		$row = BdConn::single();

		$contentVO = new ContentVO($row);
		$contentVO->metaDescription = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaDescription));
		$contentVO->metaKeywords = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaKeywords));
		$contentVO->vatPrice = (float) StoreManager::calculateVatRate((float) $contentVO->price, (float) $contentVO->storeVatRateID);
		$contentVO->price = (float) $contentVO->price;

		$contentVO->files = FilesManager::getContentFiles($uid, $lang);
		if($children) {
			$contentVO->children = self::getChildren($uid, $lang);
		}

		if($getUserParentObject && $contentVO->userParentID) {
			$contentVO->userParent = UserManager::getUser($contentVO->userParentID);
		}

		if($childrenUsers) {
			$contentVO->childrenUsers = RelationManager::getUsersFromContent($contentVO->UID);
		}

		if($contentVO->automaticSelectChild > 0)
		{
			$row=mysql_fetch_array(BdConn::runSQL("SELECT childUID FROM contentsToContents WHERE parentUID = ".$contentVO->UID." ORDER by sortOrder ASC limit 1"));
			$childUID = $row['childUID'];
			$row2=mysql_fetch_array(BdConn::runSQL("SELECT deeplink FROM contentsInfo WHERE contentsUID = ".$childUID ." and langID = ".$lang));
			$contentVO->deeplink = $row2['deeplink'];
		}
		$contentVO->defaultLangID = LanguageManager::getDefaultLanguage()->ID;
		$userID = 0;
		if (isset($_SESSION["userID"])) {
	        $userVO = new UserVO((array) unserialize($_SESSION["userID"]));
	        $userID = $userVO->ID;
	    }
        LogManager::logAction($userID, 11, 0, $contentVO->UID, $contentVO->deeplink);
		return $contentVO;
	}
	public static function getUpcomingEvent($lang = NULL, $limit = NULL, $children = false, $childrenUsers = true, $getUserParentObject = true)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$filesManager = new FilesManager;
		$limitQuery = "";
		if($limit)
		{
			$limitQuery = " LIMIT ".$limit;
		}
		$res = BdConn::query("SELECT".self::$contentSelectFields." WHERE `startDate` >= CURDATE() ORDER BY `startDate` AND contentsInfo.langID = '$lang' $limitQuery");
		$row = BdConn::single();

		$contentVO = new ContentVO($row);
		$contentVO->metaDescription = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaDescription));
		$contentVO->metaKeywords = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaKeywords));
		$contentVO->vatPrice = (float) StoreManager::calculateVatRate($contentVO->price, $contentVO->storeVatRateID);
		if(!isset($contentVO->vatPrice))
		{
			$contentVO->vatPrice = (float) $contentVO->price;
		}
		$contentVO->files = FilesManager::getContentFiles($contentVO->UID, $lang);
		if($children) {
			$contentVO->children = self::getChildren($contentVO->UID, $lang);
		}

		if($getUserParentObject && $contentVO->userParentID) {
			$contentVO->userParent = UserManager::getUser($contentVO->userParentID);
		}

		if($childrenUsers) {
			$contentVO->childrenUsers = RelationManager::getUsersFromContent($contentVO->UID);
		}

		if($contentVO->automaticSelectChild > 0)
		{
			$row=mysql_fetch_array(BdConn::runSQL("SELECT childUID FROM contentsToContents WHERE parentUID = ".$contentVO->UID." ORDER by sortOrder ASC limit 1"));
			$childUID = $row['childUID'];
			$row2=mysql_fetch_array(BdConn::runSQL("SELECT deeplink FROM contentsInfo WHERE contentsUID = ".$childUID ." and langID = ".$lang));
			$contentVO->deeplink = $row2['deeplink'];
		}
		$contentVO->defaultLangID = LanguageManager::getDefaultLanguage()->ID;

		return $contentVO;
	}

	public static function getParentContent($UID, $lang, $isMainParent = false)
	{

		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$parentUID = MenuManager::getParent($UID, $isMainParent);

		return self::getContent($parentUID[0]['UID'], $lang);
	}

	public static function getParentUID($UID, $isMainParent = true)
	{
		if(!$UID)
		{
			$UID = 0;
		}

		$mainParentQuery = "";
		if($isMainParent)
		{
			$mainParentQuery = " AND contentsToContents.mainParent = 1 ";
		}

		$sql = "SELECT contentsToContents.parentUID as UID
   		 FROM contentsToContents INNER JOIN contentsInfo ON contentsToContents.childUID = contentsInfo.contentsUID
    	WHERE contentsInfo.contentsUID = $UID $mainParentQuery GROUP by contentsToContents.parentUID";

        $response = array();
        $resultset = BdConn::runSQL($sql);

       	foreach ($resultset as $row) {
       		//var_dump($row);
            $response[] = $row['UID'];
        }
        if(!count($response)) {
        	return false;
        }
        return $response[0];
	}
	public static function getAllParentsUID($UID, $includeMainParent = true)
	{
		if(!$UID)
		{
			$UID = 0;
		}

		$mainParentQuery = "";
		if(!$includeMainParent)
		{
			$mainParentQuery = " AND contentsToContents.mainParent = 0 ";
		}


		$sql = "SELECT contentsToContents.parentUID as UID FROM contentsToContents
		WHERE
		contentsToContents.childUID = $UID $mainParentQuery
		";

		//return $sql;
		$response = array();
        $resultset = BdConn::runSQL($sql);
       foreach ($resultset as $row) {
            $response[] = (int) $row['UID'];
        }

        return $response;
	}
	public static function getAllChildrenUID($UID)
	{
		if(!$UID)
		{
			$UID = 0;
		}

		$sql = "SELECT contentsToContents.childUID as UID FROM contentsToContents
		WHERE
		contentsToContents.parentUID = $UID
		";

		//return $sql;
		$response = array();
        $resultset = BdConn::runSQL($sql);
       	foreach ($resultset as $row) {
            $response[] = (int) $row['UID'];
        }

        return $response;
	}

	public static function getContentByDeeplink($deeplink)
    {
        $deeplink = strtolower($deeplink);
        // $filesManager = new FilesManager;
        BdConn::query("SELECT contentsUID, langID from contentsInfo WHERE LOWER(contentsInfo.deeplink) = '$deeplink'");
        $row = BdConn::single();
				$contentVO = new ContentVO($row);
				$contentVO = self::getContent($row['contentsUID'], $row['langID']);
				$contentVO->cols = 2;
				return $contentVO;
        // $contentVO = new ContentVO($row);
				//
        // if($row){
				//
        //     $uid = $row['UID'];
        //     $lang = $row['langID'];
				//
        //     $contentVO->metaDescription = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaDescription));
        //     $contentVO->metaKeywords = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaKeywords));
        //     $contentVO->vatPrice = (float)StoreManager::calculateVatRate($contentVO->price, $contentVO->storeVatRateID);
        //     if (!isset($contentVO->vatPrice)) {
        //         $contentVO->vatPrice = (float)$contentVO->price;
        //     }
        //     $contentVO->files = FilesManager::getContentFiles($uid, $lang);
        //     if ($contentVO->automaticSelectChild > 0) {
        //         BdConn::query("SELECT childUID FROM contentsToContents WHERE parentUID = " . $contentVO->UID . " ORDER BY sortOrder ASC LIMIT 1");
        //         $row = BdConn::single();
        //         $childUID = $row['childUID'];
				//
        //         BdConn::runSQL("SELECT deeplink FROM contentsInfo WHERE contentsUID = " . $childUID . " AND langID = " . $lang);
        //         $row2 = BdConn::single();
        //         $contentVO->deeplink = $row2['deeplink'];
        //     }
        //     $contentVO->defaultLangID = LanguageManager::getDefaultLanguage()->ID;
        //     $contentVO->userParent = UserManager::getUser($contentVO->userParentID);
        //     $contentVO->childrenUsers = RelationManager::getUsersFromContent($contentVO->UID);
        //     $userID = 0;
	      //   if (isset($_SESSION["userID"])) {
	      //       if (UtilsManager::is_serialized($_SESSION["userID"])) {
				//     $userVO = unserialize($_SESSION["userID"]);
				// } else {
				//     $userVO = UtilsManager::fixObject($_SESSION["userID"]);
				// }
	      //       $userID = $userVO->ID;
				//
	      //   }
        //     LogManager::logAction($userID, 11, 0, $contentVO->UID, $contentVO->deeplink);
        // }
				$contentVO->cols = 2;
        return $contentVO;
    }

	public static function getHomeContent($lang)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$filesManager = new FilesManager;

		BdConn::query("SELECT" . self::$contentSelectFields . "WHERE contents.isHome = '1' AND contentsInfo.langID = '$lang'");
        $row = BdConn::single();

		$contentVO = new ContentVO($row);
		if(!$contentVO->UID) {
			$contentVO->UID = 0;
		}
		$contentVO->vatPrice = (float) StoreManager::calculateVatRate($contentVO->price, $contentVO->storeVatRateID);
		if(!isset($contentVO->vatPrice))
		{
			$contentVO->vatPrice = (float) $contentVO->price;
		}
		$contentVO->metaDescription = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaDescription));
		$contentVO->metaKeywords = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaKeywords));
		$contentVO->vatPrice = StoreManager::calculateVatRate($contentVO->price, $contentVO->storeVatRateID);
		$contentVO->files = FilesManager::getContentFiles($contentVO->UID, $lang);
		$userID = 0;
		if (isset($_SESSION["userID"])) {
	        $userVO = new UserVO((array) unserialize($_SESSION["userID"]));
	        $userID = $userVO->ID;
	    }
        LogManager::logAction($userID, 11, 0, $contentVO->UID, $contentVO->deeplink);
		return $contentVO;
	}
	public static function getContentByDate($date, $lang)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}


		$filesManager = new FilesManager;
		$res = mysql_query("SELECT".self::$contentSelectFields ."WHERE contentsInfo.date like '%$date%' AND contentsInfo.enabled = '1' AND contentsInfo.langID='$lang'");

		$response = array();
		while ($row=mysql_fetch_array($res))
		{

			$contentVO = new ContentVO($row);
			$contentVO->vatPrice = (float) StoreManager::calculateVatRate($contentVO->price, $contentVO->storeVatRateID);
			if(!isset($contentVO->vatPrice))
			{
				$contentVO->vatPrice = (float) $contentVO->price;
			}
			$contentVO->metaDescription = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaDescription));
			$contentVO->metaKeywords = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaKeywords));
			$contentVO->files = FilesManager::getContentFiles($row['UID'], $lang);
			$contentVO->children = ContentsManager::getChildren($row['UID'], $lang);
			$response[] = $contentVO;
		}
		
		return $response;
	}
	public static function getChildrenByDeeplink($deeplink, $lang= null, $limit = NULL, $rand = NULL)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}
		$content = self::getContentByDeeplink($deeplink);
		$uid = $content->UID;
		return self::getChildren($uid, $lang, $limit, $rand);
	}

	public static function getChildrenFromParents($parentsUID, $lang = null){
		if(!$lang)
		{
			$lang = LanguageManager::getActiveLanguage()->ID;
		}

		$parentsUIDQuery = "contentsToContents.parentUID IN(".implode(",", $parentsUID).")";

		$sql = "SELECT count(UID) as count,".self::$contentSelectFields." WHERE $parentsUIDQuery AND contentsInfo.langID = '$lang' AND contents.enabled = '1' AND contentsInfo.enabled = '1' group by UID HAVING count >= ".count($parentsUID);

		// }
		$res = BdConn::runSQL($sql);
		$response = array();

		foreach ($res as $row) {
			$contentVO = new ContentVO($row);


			if($row['automaticSelectChild'])
			{

				$contentVO->deeplink = MenuManager::getRootDeeplink($row['UID'], $lang);
			}
			$contentVO->metaDescription = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaDescription));
			$contentVO->metaKeywords = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaKeywords));
			$contentVO->vatPrice = (float) StoreManager::calculateVatRate($contentVO->price, $contentVO->storeVatRateID);
			$contentVO->files = FilesManager::getContentFiles($row['UID'], $lang);
			$contentVO->children = ContentsManager::getChildren($row['UID'], $lang);
			$response[] = $contentVO;
		}

		return $response;
	}

	public static function getChildren($uid, $lang= null, $limit = NULL, $rand = NULL, $orderBy = NULL, $options = NULL, $currentPage = null, $itensPerPage = 10, $customWhere = NULL, $templatesToConsider = NULL)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}



		$filesManager = new FilesManager;

		$limitQuery = "";
		if($limit)
		{
			$limitQuery = " LIMIT ".$limit;
		}
		$orderQuery = "BY contentsToContents.sortOrder ASC";
		if($orderBy)
		{
			$orderQuery = mysql_real_escape_string("BY ".$orderBy);
		}

		if($rand)
		{
			$orderQuery = "BY RAND()";
		}

		$paginationLimit = "";
		if($currentPage)
		{
			$currentPage--;
			$totalPages = self::getTotalPages($uid, $lang, $itensPerPage);

			if($currentPage >= $totalPages)
			{
				$currentPage = $totalPages / $itensPerPage;
			}

			$paginationLimit = " limit ".($currentPage * $itensPerPage).", ".$itensPerPage;
		}
		$parentsUIDQuery = "";
		if(!is_array($uid))
		{
			$parentsUIDQuery = "contentsToContents.parentUID = '$uid'";
			$parentsUIDQueryGroup = "";
			$count = "";
		}
		else
		{
			$parentsUIDQuery = "";
			for($i = 0; $i < count($uid); $i++)
			{
				$id = $uid[$i];
				$parentsUIDQuery .= " contentsToContents.parentUID = '$id' ";
				if($i < count($uid)-1)
				{
					$parentsUIDQuery .= " OR ";
				}
				$parentsUIDQueryGroup = " GROUP BY contentsToContents.childUID HAVING c = ".count($uid);
				$count = "COUNT(*) c,";
			}
		}
		$templatesToConsiderQuery = "";

		if(is_array($templatesToConsider))
		{
			$templatesToConsiderQuery = " AND (contents.template = ";
			for($i = 0; $i < count($templatesToConsider); $i++)
			{
				if($i >0)
				{
					$templatesToConsiderQuery .= " OR contents.template = ";
				}
				$templatesToConsiderQuery .= " '".$templatesToConsider[$i]."' ";
			}
			$templatesToConsiderQuery .= ") ";
		}
		$sql = "SELECT $count".self::$contentSelectFields."
		WHERE $parentsUIDQuery AND contentsInfo.langID = '$lang' AND contents.enabled = '1' AND contentsInfo.enabled = '1' $customWhere $parentsUIDQueryGroup $templatesToConsiderQuery ORDER ".$orderQuery. $limitQuery.$paginationLimit;

		//return $sql;

		$res=BdConn::runSql($sql);


		$response = array();
		foreach ($res as $row) {
			$contentVO = new ContentVO($row);
			$optionsIn = $options;
			if(isset($options)) {


				if(count($options))
				{
					while (list($key, $value) = each($options)) {


					   	$contentVO->$key = $contentVO->$value;

					}
				}
			}
			if($row['automaticSelectChild'])
			{

				$contentVO->deeplink = MenuManager::getRootDeeplink($row['UID'], $lang);
			}
			$contentVO->metaDescription = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaDescription));
			$contentVO->metaKeywords = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaKeywords));
			$contentVO->vatPrice = (float) StoreManager::calculateVatRate($contentVO->price, $contentVO->storeVatRateID);
			$contentVO->files = FilesManager::getContentFiles($row['UID'], $lang);
			$contentVO->children = ContentsManager::getChildren($row['UID'], $lang, $limit, $rand, $orderBy, $optionsIn);
			$response[] = $contentVO;
		}

		return $response;

	}

	public static function getChildrenOrderByStartDate($uid, $lang, $countNow = NULL,$limit = NULL, $orderBy=NULL)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$filesManager = new FilesManager;

		$limitQuery = "";
		if($limit)
		{
			$limitQuery = " LIMIT ".$limit;
		}

		$startDateQuery = "";
		if($countNow)
		{
			$currentDate = date('Y').'-'.date('m').'-'.date('j')." ".date('H').":".date('i').":".date('s');
			$startDateQuery = " AND startDate > '".$currentDate."' ";
		}

		if($orderBy == "ASC")
		{
			$order = "ASC";
		}
		else if($orderBy == "DESC")
		{
			$order = "DESC";
		}

		$sql = ("SELECT".self::$contentSelectFields ." INNER JOIN contentsToContents ON contents.UID = contentsToContents.childUID
		WHERE contentsToContents.parentUID = '$uid' AND contentsInfo.langID = '$lang' AND contents.enabled = '1' AND contentsInfo.enabled = '1'".$startDateQuery."ORDER BY contentsInfo.startDate $order" . $limitQuery);

		$res=BdConn::runSql($sql);


		$response = array();
		foreach ($res as $row) {

			$contentVO = new ContentVO($row);
			$contentVO->vatPrice = (float) StoreManager::calculateVatRate($contentVO->price, $contentVO->storeVatRateID);
			if(!isset($contentVO->vatPrice))
			{
				$contentVO->vatPrice = (float) $contentVO->price;
			}
			$contentVO->files = FilesManager::getContentFiles($row['UID'], $lang);
			$contentVO->children = ContentsManager::getChildren($row['UID'], $lang);
			$contentVO->metaDescription = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaDescription));
			$contentVO->metaKeywords = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaKeywords));
			$response[] = $contentVO;
		}

		return $response;
	}

	public static function getContentByStartDate($date, $lang)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$filesManager = new FilesManager;
		$res = mysql_query("SELECT ".self::$contentSelectFields ."WHERE contentsInfo.startDate like '%$date%' AND contentsInfo.enabled = '1' AND contentsInfo.langID='$lang'");

		$response = array();
		while ($row=mysql_fetch_array($res))
		{
			$contentVO = new ContentVO($row);
			$contentVO->vatPrice = (float) StoreManager::calculateVatRate($contentVO->price, $contentVO->storeVatRateID);
			if(!isset($contentVO->vatPrice))
			{
				$contentVO->vatPrice = (float) $contentVO->price;
			}
			$contentVO->files = FilesManager::getContentFiles($row['UID'], $lang);
			$contentVO->children = ContentsManager::getChildren($row['UID'], $lang);
			$contentVO->metaDescription = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaDescription));
			$contentVO->metaKeywords = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaKeywords));
			$response[] = $contentVO;
		}

		return $response;
	}
	public static function getChildrenOrderByDate($uid, $lang, $countNow = NULL, $limit = NULL)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$filesManager = new FilesManager;

		$limitQuery = "";
		if($limit)
		{
			$limitQuery = " LIMIT ".$limit;
		}

		$startDateQuery = "";
		if($countNow)
		{
			$currentDate = date('Y').'-'.date('m').'-'.(date('j')+1)." ".date('H').":".date('i').":".date('s');
			$startDateQuery = " AND date < '".$currentDate."' ";
		}

		$sql= ("SELECT".self::$contentSelectFieldsAlias ." WHERE cc.parentUID = '$uid' AND ci.langID = '$lang' AND c.enabled = '1' AND ci.enabled = '1'".$startDateQuery." ORDER BY ci.date DESC" . $limitQuery);

		// return $sql;


		$res=BdConn::runSql($sql);


		$response = array();
		foreach ($res as $row) {
			$contentVO = new ContentVO($row);
			$contentVO->vatPrice = (float) StoreManager::calculateVatRate($contentVO->price, $contentVO->storeVatRateID);
			if(!isset($contentVO->vatPrice))
			{
				$contentVO->vatPrice = (float) $contentVO->price;
			}
			$contentVO->files = FilesManager::getContentFiles($row['UID'], $lang);
			$contentVO->children = ContentsManager::getChildren($row['UID'], $lang);
			$contentVO->metaDescription = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaDescription));
			$contentVO->metaKeywords = preg_replace('/^\s+|\n|\r|\s+$/m', '', strip_tags($contentVO->metaKeywords));
			$response[] = $contentVO;
		}

		return $response;
	}

	/*
		$templatesToConsider array
	*/

	public static function search($string, $lang, $limit = NULL, $templatesToConsider = NULL, $groupByParent = NULL, $searchChildsOf = null, $searchInFiles = true)
	{
		$string = mb_strtolower($string, 'UTF-8');
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}

		$filesManager = new FilesManager;

		$limitQuery = "";
		if($limit)
		{
			$limitQuery = " LIMIT ".$limit;
		}

		$templatesToConsiderQuery = "";
		if(is_array($templatesToConsider))
		{
			$templatesToConsiderQuery = " (contents.template = ";
			for($i = 0; $i < count($templatesToConsider); $i++)
			{
				if($i >0)
				{
					$templatesToConsiderQuery .= " OR contents.template = ";
				}
				$templatesToConsiderQuery .= " '".$templatesToConsider[$i]."' ";
			}
			$templatesToConsiderQuery .= ") AND";
		}

		$query=("SELECT ".self::$contentSelectFields ." WHERE $templatesToConsiderQuery ((lower(contentsInfo.deeplink) like '%$string%' collate utf8_general_ci OR lower(contentsInfo.menuLabel) like '%$string%' collate utf8_general_ci OR lower(contentsInfo.title) like '%$string%' collate utf8_general_ci OR lower(contentsInfo.body) like '%$string%' collate utf8_general_ci) AND contentsInfo.langID = '$lang' AND contentsInfo.enabled = '1' AND contents.enabled = '1') AND contents.searchable = 1  GROUP BY contents.UID ORDER BY contentsInfo.ID ASC". $limitQuery);
		// return $query;
		// $response = array();
		// if($searchInFiles) {
		// 	// TODO FILES
		// 	$query= ("SELECT contentsInfo.contentsUID,
		// 	contentsInfo.deeplink,
		// 	contentsInfo.title,
		// 	contentsInfo.menuLabel,
		// 	contentsInfo.automaticSelectChild
		// 	FROM contentsInfo INNER JOIN contents ON contentsInfo.contentsUID = contents.UID
		// 	INNER JOIN filesToContents ON filesToContents.contentsUID = contentsInfo.contentsUID
		// 	INNER JOIN files ON files.ID = filesToContents.fileID
		// 	WHERE lower(files.file) like '%$string%' OR lower(files.description) like '%$string%' OR lower(files.label) like '%$string%' AND contentsInfo.langID = '$lang' GROUP BY contentsUID ");

		// 	$res = BdConn::runSQL($query);
		// 	$response = array();

		// 	foreach ($res as $row) 
		// 	{
		// 		$contentVO = new ContentVO($row);
		// 		$response[] = $contentVO;
				
		// 	}
		// }
		
		$res = BdConn::runSQL($query);
		$response = array();

		foreach ($res as $row) 
		{
			$contentVO = new ContentVO($row);
			$response[] = $contentVO;
		}
		
		return $response;
	}

	public function smartSearch($search, $lang, $searchInFiles = true, $templatesToConsider = null, $limit = null)
	{
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}
		$limitQuery = "";
		if($limit)
		{
			$limitQuery = " LIMIT ".($limit);
		}
		$templatesToConsiderQuery = "";
		if(is_array($templatesToConsider))
		{
			$templatesToConsiderQuery = " WHERE (contents.template = ";
			for($i = 0; $i < count($templatesToConsider); $i++)
			{
				if($i >0)
				{
					$templatesToConsiderQuery .= " OR contents.template = ";
				}
				$templatesToConsiderQuery .= " '".$templatesToConsider[$i]."' ";
			}
			$templatesToConsiderQuery .= ")";
		}

		$keywords = explode ( " " , $search );
		$search = "+\"".$search."\"";
		for($i = 0; $i < count($keywords); $i++) {
			$search .= "+".$keywords[$i];
		}
		$response = array();
		$res = mysql_query("SELECT contentsInfo.contentsUID as UID,contentsInfo.title,contentsInfo.subTitle,contentsInfo.deeplink,contentsInfo.author, MATCH(contentsInfo.title, contentsInfo.menuLabel,contentsInfo.deeplink, contentsInfo.subTitle,contentsInfo.author) AGAINST('$search') AS relevance FROM contents INNER JOIN contentsInfo ON contents.UID = contentsInfo.contentsUID
		INNER JOIN contentsTemplates ON contents.template = contentsTemplates.template $templatesToConsiderQuery ORDER BY relevance DESC $limitQuery");
		while($row = mysql_fetch_array($res)) {

			$contentVO = new ContentVO($row);
			$contentVO->files = FilesManager::getContentFiles($row['UID'], $lang);
			if($row['relevance'] > 0) {
				$response[] = $contentVO;
			}
		}
		if($searchInFiles) {
			// if(count($response) == 0) {
			// 	if($limit)
			// 	{
			// 		$limitQuery = " LIMIT ".$limit;
			// 	}
			// }
			$res = mysql_query("SELECT ID,file,label as title,deeplink, MATCH(file, label, description, deeplink) AGAINST('$search') as relevance FROM files ORDER BY relevance DESC $limitQuery");
			while($row = mysql_fetch_array($res)) {
				$fileVO = new FileVO($row);
				$fileVO->extension = UtilsManager::getFileExtension($fileVO->file);
				$contentVO = new ContentVO($row);
				$contentVO->files->currentFilesExtensions[] = UtilsManager::getFileExtension($fileVO->file);
				$contentVO->files->default = $fileVO;

				$response[] = $contentVO;
			}
			return $response;
		}


		//SELECT ID,file,label,deeplink, MATCH(file, label, description, deeplink) AGAINST("+video+Triple+manual") as relevance FROM files ORDER BY relevance DESC
		return $response;
	}

	// ??????
	// private function getContentComposedData($contentVO) {

	// }

	public static function groupByParent($contents, $lang = null) {
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}
		$response = array();
		for($i = 0; $i <  count($contents); $i++) {
			$content = $contents[$i];
			$parentUID = self::getParentUID($content->UID, true);

			if(!in_array($parentUID, $response)) {
				$response[$parentUID] = new stdClass();
				$response[$parentUID]->UID = $parentUID;
				$response[$parentUID]->deeplink = self::getDeeplink($parentUID, $lang);
				$response[$parentUID]->title = self::getTitle($parentUID, $lang);
				$response[$parentUID]->menuLabel = self::getMenuLabel($parentUID, $lang);
				$response[$parentUID]->children = array();
				$response[$parentUID]->children[] = $content;
			}
			else {
				$response[$parentUID]->children[] = $content;
			}
		}
		$response = array_values($response);
		return $response;
	}

	public static function getTemplates()
	{
		$res=mysql_query("SELECT * FROM contentsTemplates");

		$response = array();
		while ($row=mysql_fetch_array($res))
		{
			$contentTemplateVO = new ContentTemplateVO($row);
			$response[] = $contentTemplateVO;
		}
		return $response;
	}
	public static function getEnabledSate($UID)
	{
		
		$sql = "SELECT enabled FROM contents WHERE UID=$UID";
        BdConn::query($sql);
        $deeplink = BdConn::single();
        return (boolean) $deeplink['enabled'];
	}
	public static function getDeeplink($UID, $languageID)
	{
		if(!is_numeric($UID)) {
			$UID = self::getUIDByVerboseID($UID);
		}
		$sql = "SELECT deeplink FROM contentsInfo WHERE contentsUID='" . $UID . "' AND langID='" . $languageID . "'";
        BdConn::query($sql);
        $deeplink = BdConn::single();
        return $deeplink['deeplink'];
	}
	public static function getMenuLabel($UID, $languageID)
	{
		if(!is_numeric($UID)) {
			$UID = self::getUIDByVerboseID($UID);
		}
		$sql = "SELECT menuLabel FROM contentsInfo WHERE contentsUID='" . $UID . "' AND langID='" . $languageID . "'";
		BdConn::query($sql);
		$menuLabel = BdConn::single();
		return $menuLabel['menuLabel'];
	}
	public static function getTitle($UID, $languageID)
	{
		if(!is_numeric($UID)) {
			$UID = self::getUIDByVerboseID($UID);
		}
		$sql = "SELECT title FROM contentsInfo WHERE contentsUID='".$UID."' AND langID='".$languageID."'";
		$resultset=BdConn::query($sql);
		$menuLabel = BdConn::single();

		return $menuLabel['title'];
	}

	public static function getLead($UID, $languageID)
	{
		if(!is_numeric($UID)) {
			$UID = self::getUIDByVerboseID($UID);
		}
		$sql = "SELECT lead FROM contentsInfo WHERE contentsUID='".$UID."' AND langID='".$languageID."'";
		$resultset=BdConn::runSQL($sql);
		$menuLabel = mysql_fetch_row($resultset);
		return $menuLabel[0];
	}

	public static function rateContent($UID, $rate)
	{
		date_default_timezone_set("Europe/London");
		$rateDate = date('y-m-d H:i:s');
		$userID = null;
		if( !isset($_SESSION) ) session_start();
		if(isset($_SESSION['userID']))
		{
			$userVO = $_SESSION["userID"];
			$userID = $userVO->ID;
		}

		if(self::userAlreadyRatedProduct($UID))
		{
			$sql = "INSERT INTO contentsRates (contentUID, ip, rate, rateDate, userID) VALUES ('".$UID."','".$_SERVER['REMOTE_ADDR']."','".$rate."','".$rateDate."','".$userID."')";
			$resultset=BdConn::runSQL($sql);
			LogManager::logAction($userID, '8', null, $UID);
		}
		return self::getContentRate($UID);

	}
	public static function getContentRate($UID)
	{
		$sql = "SELECT ROUND((floor(AVG(rate) * 2) / 2), 1) as rate FROM `contentsRates` WHERE contentUID = $UID";
		$resultset=BdConn::runSQL($sql);
		$rate = mysql_fetch_row($resultset);
		return $rate[0];
	}
	private function userAlreadyRatedProduct($UID)
	{
		$term = "ip";
		$search = $_SERVER['REMOTE_ADDR'];
		if( !isset($_SESSION) ) session_start();
		if(isset($_SESSION['userID']))
		{
			$userVO = $_SESSION["userID"];
			$term = "userID";
			$search = $userVO->ID;
		}
		$sql = "SELECT count($term) as count from contentsRates WHERE $term = '$search' AND contentUID = $UID";
		$resultset = BdConn::runSQL($sql);
		$count =mysql_fetch_row($resultset);
		if($count[0] == 1)
		{
			return false;
		}
		return true;

	}
	public static function getSiblingsUID($UID, $includeSelf = true, $orderByDate = null)
	{
		$lang = LanguageManager::getDefaultLanguage()->ID;
		
		$parentUID = MenuManager::getParent($UID, true);
		$notSelfQuery = "";
		if(!$includeSelf) {
			$notSelfQuery = "AND childUID != $UID";
		}
		if(is_array($parentUID)) {
			$parentUID = $parentUID[0]['UID'];
		}
		
		$sql = ("SELECT
		contentsToContents.childUID
		FROM
		contentsToContents
		INNER JOIN
		contents
		ON 
			contentsToContents.childUID = contents.UID
		WHERE
		contentsToContents.parentUID = $parentUID AND contents.enabled	= 1 $notSelfQuery 
		ORDER BY sortOrder ASC");
		$query = BdConn::runSQL($sql);

		$siblings = array();

		foreach ($query as $sibling) {
			$siblings[] = $sibling['childUID'];
		}

		if($orderByDate) {
			$orderQuery = "";
			for($i = 0; $i < count($siblings); $i++) {
				$orderQuery .= " contentsUID =  ".$siblings[$i];
				if($i < count($siblings)-1) {
					$orderQuery .= " OR ";
				}
			}
			$query = BdConn::runSQL("SELECT contentsUID from contentsInfo WHERE $orderQuery ORDER BY contentsInfo.date DESC");
			
			$siblings = array();

			foreach ($query as $sibling) {
				$siblings[] = $sibling['contentsUID'];
			}
			$siblings = array_unique($siblings);
			$siblings = array_values($siblings);
		}

		

		return $siblings;
	}

	public static function getContentsByGivenInterval($startDate, $intervalDays, $lang = NULL, $parentUIDIn = NULL, $getUserParentObject = true)
	{
		
		if(!$lang)
		{
			$lang = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($lang))
			{
				$lang = LanguageManager::getLanguageIDByShort($lang)->ID;
			}
		}
		global $mysqli;

		$query = BdConn::runSQL("SELECT ".self::$contentSelectFields ." WHERE (startDate >= '$startDate' AND startDate <= date_add('$startDate', INTERVAL $intervalDays DAY) OR date >= '$startDate' AND date <= date_add('$startDate', INTERVAL $intervalDays DAY)) AND contentsInfo.langID = '$lang' AND contentsInfo.enabled = 1 ORDER BY startDate DESC");

		$contents = array();
		foreach ($query as $content) 
		{
			$content = new ContentVO($content);
			
			$parentUID = MenuManager::getParent($content->UID);
			$content->parentUID = $parentUID;
			
			$content->childrenUsers = RelationManager::getUsersFromContent($content->UID);
			
			
			if($getUserParentObject && $content->userParentID) {
				//if($content->userParentID != 58) {
					$content->userParent = UserManager::getUser($content->userParentID);
				//}
			}
			if(!$parentUIDIn)
			{
				$contents[] = $content;
			}
			else
			{
				if($parentUIDIn == $parentUID)
				{
					$contents[] = $content;
				}
			}


		}

		return $contents;
	}

	public static function addUserContent($userChildID, $contentVO, $parentContentUID = 0, $options = NULL, $roleProfileID = null)
	{
		$checkUser = UserManager::getUser($userChildID);
		$ema = EmaBrain::Instance();

		if(isset($checkUser->ID) || $ema::$ALLOW_ADD_CONTENT_NO_SESSION)
		{

			//$contentVO->userParentID = $checkUser->ID;
			return self::addContent($contentVO,$parentContentUID, $options, $roleProfileID);
		}
		else
		{
			return false;
		}
	}
	/*
	// TO DO
	private function checkIfContentBelongsToUser($userID, $contentUID)
	{

	}
	*/
	public static function editUserContent($userChildID, $contentVO)
	{
		$checkUser = UserManager::getUser($userChildID);
		$ema = EmaBrain::Instance();

		if(isset($checkUser->ID) || $ema::$ALLOW_ADD_CONTENT_NO_SESSION)
		{
			$sql = ("UPDATE contentsInfo SET
			contentsInfo.title = '".$contentVO->title."',
			contentsInfo.subtitle = '".$contentVO->subtitle."',
			contentsInfo.menuLabel = '".$contentVO->menuLabel."',
			contentsInfo.body ='".$contentVO->body."',
			contentsInfo.lead = '".$contentVO->lead."',
			contentsInfo.author = '".$contentVO->author."',
			contentsInfo.link = '".$contentVO->link."',
			contentsInfo.linkTarget = '".$contentVO->linkTarget."',
			contentsInfo.date = '".$contentVO->date."',
			contentsInfo.startDate = '".$contentVO->startDate."',
			contentsInfo.endDate = '".$contentVO->endDate."',
			contentsInfo.price = '".$contentVO->price."',
			contentsInfo.location = '".$contentVO->location."',
			contentsInfo.optional1 = '".$contentVO->optional1."',
			contentsInfo.optional2 = '".$contentVO->optional2."',
			contentsInfo.optional3 = '".$contentVO->optional3."',
			contentsInfo.optional4 = '".$contentVO->optional4."',
			contentsInfo.optional5 = '".$contentVO->optional5."',
			contentsInfo.optional6 = '".$contentVO->optional6."',
			contentsInfo.optional7 = '".$contentVO->optional7."',
			contentsInfo.optional8 = '".$contentVO->optional8."',
			contentsInfo.optional9 = '".$contentVO->optional9."',
			contentsInfo.optional10 = '".$contentVO->optional10."',
			contentsInfo.highlightType = '".$contentVO->highlightType."',
			contentsInfo.urlTitle = '".$contentVO->urlTitle."',
			contentsInfo.weight = '".$contentVO->weight."',
			contentsInfo.quantity = '".$contentVO->quantity."',
			contentsInfo.deeplink = '".$contentVO->deeplink."',
			contentsInfo.storeVatRateID = '".$contentVO->storeVatRateID."',
			contentsInfo.metaDescription = '".$contentVO->metaDescription."',
			contentsInfo.metaKeywords = '".$contentVO->metaKeywords."'
			WHERE contentsInfo.contentsUID = '".$contentVO->UID."' AND langID ='".$contentVO->langID."'");
			return $sql;
			BdConn::runSQL($sql);

			$deletedUsers = RelationManager::getUsersFromContent($contentVO->UID, true);

			// RelationManager::deleteAllUsersFromContent($contentVO->UID);
			// MANAGE CHILDREN USERS
			if(count($contentVO->childrenUsers)) {
				for($i = 0; $i < count($contentVO->childrenUsers); ++$i)
				{
					$userInVO = $contentVO->childrenUsers[$i];
					// ARRAY of USERS VO with our without ID

					// If has email in, checks for diferences in fields
					if(UserManager::isEmailIn($userInVO->email) && $userInVO->email != "") {
						UserManager::hasUserChanged($userInVO, true);
					}
					// no email in, adds User
					else {
						$userRegisterResponse = UserManager::registerUser($userInVO, null, false, $contentVO->userParentID);
						$userInVO->ID = $userRegisterResponse->data;
					}
					// Adss to relations
					RelationManager::setUserToContent($contentVO->UID, $userInVO->ID);

					for($c = 0; $c < count($deletedUsers); $c++) {
						if($userInVO->ID == $deletedUsers[$c]) {
							array_splice($deletedUsers, $c, 1);
						}
					}

				}
			}
			RelationManager::deleteUsersFromContent($contentVO->UID, $deletedUsers);
			$contentVO = self::getContent($contentVO->UID, $contentVO->langID, null, true, true);
			$contentVO->deletedUsers = $deletedUsers;

			return $contentVO;
		}

	}


	private static function addContent($contentVO, $parentUID, $options = null, $roleProfileID = null)
	{

		//$contentVO = new ContentVO($contentVO);
		if(!isset($contentVO->langID))
		{
			$contentVO->langID = LanguageManager::getDefaultLanguage()->ID;
		}
		if(!isset($contentVO->template))
		{
			$contentVO->template = 'default.php';
		}

		// MANAGE PARENT USER
		if(isset($contentVO->userParent->email)) {
			$userParent = $contentVO->userParent;
			if(UserManager::isEmailIn($userParent->email)) {
				UserManager::hasUserChanged($userParent, true);
			}
			else {
				$userRegisterResponse = UserManager::registerUser($userParent, null, false);
				$userParent->ID = $userRegisterResponse->data;
			}
		}
		if(isset($userParent)) {
			$contentVO->userParentID = $userParent->ID;
		}

		$queryContent = BdConn::runSQL("INSERT INTO contents
		(contents.siteMapLabel, contents.template,userParentID
		) VALUES ('".$contentVO->title."', '".$contentVO->template."', '".$contentVO->userParentID."')
		");
		$addedUID = BdConn::getLastInsertId();

		if($addedUID)
		{
			if($roleProfileID) {
				RolesManager::setContentRole($addedUID, $roleProfileID);
			}

			$queryContentsToContents = BdConn::runSQL ("INSERT INTO contentsToContents (parentUID, childUID) VALUES ('".$parentUID."','".$addedUID."')");
			if($queryContentsToContents)
			{
				if(isset($options)) {
					while (list($key, $value) = each($options)) {

						if(property_exists($contentVO,$key))
						{
					   		$contentVO->$value = $contentVO->$key;
					   	}
					}
				}
				if(!isset($contentVO->deeplink)) {
					$deeplink = "";
					if($parentUID) {
						$deeplink = self::getDeeplink($parentUID, $contentVO->langID)."/";
					}
					$deeplink .= UtilsManager::makeSlugs($contentVO->title);
					$contentVO->deeplink = $deeplink;
				}

				if(!isset($contentVO->deeplinkParentIgnore)) {
					$contentVO->deeplinkParentIgnore = 0;
				}
				$contentVO->UID = $addedUID;
				 BdConn::runSQL("INSERT INTO contentsInfo
				(contentsInfo.contentsUID,
				contentsInfo.title,
				contentsInfo.menuLabel,
				contentsInfo.body,
				contentsInfo.date,
				contentsInfo.lead,
				contentsInfo.author,
				contentsInfo.link,
				contentsInfo.optional1,
				contentsInfo.optional2,
				contentsInfo.optional3,
				contentsInfo.optional4,
				contentsInfo.optional5,
				contentsInfo.optional6,
				contentsInfo.optional7,
				contentsInfo.optional8,
				contentsInfo.optional9,
				contentsInfo.optional10,
				contentsInfo.deeplink,
				contentsInfo.highlightType,
				contentsInfo.langID,
				contentsInfo.linkTarget,
				contentsInfo.startDate,
				contentsInfo.endDate,
				contentsInfo.price,
				contentsInfo.urlTitle,
				contentsInfo.metaDescription,
				contentsInfo.metaKeywords,
				contentsInfo.subtitle,
				contentsInfo.storeVatRateID,
				contentsInfo.weight,
				contentsInfo.quantity,
				contentsInfo.automaticSelectChild,
				contentsInfo.location,
				contentsInfo.deeplinkParentIgnore
				) VALUES ('".$addedUID."', '".$contentVO->title."','".$contentVO->menuLabel."','".$contentVO->body."','".$contentVO->date."', '".stripcslashes(html_entity_decode($contentVO->lead))."','".$contentVO->author."', '".$contentVO->link."', '".$contentVO->optional1."', '".$contentVO->optional2."', '".$contentVO->optional3."', '".$contentVO->optional4."', '".$contentVO->optional5."', '".$contentVO->optional6."', '".$contentVO->optional7."', '".$contentVO->optional8."', '".$contentVO->optional9."', '".$contentVO->optional10."', '".$contentVO->deeplink."', '".$contentVO->highlightType."', '".$contentVO->langID."', '".$contentVO->linkTarget."', '".$contentVO->startDate."', '".$contentVO->endDate."', '".$contentVO->price."', '".$contentVO->urlTitle."', '".$contentVO->metaDescription."', '".$contentVO->metaKeywords."', '".$contentVO->subtitle."', '".$contentVO->storeVatRateID."', '".$contentVO->weight."', '".$contentVO->quantity."', '".$contentVO->automaticSelectChild."', '".$contentVO->location."', ".$contentVO->deeplinkParentIgnore.")");

				for($i = 0; $i < count($contentVO->children); ++$i)
				{
					$child = $contentVO->children[$i];
					self::addContent($child, $addedUID,$options);
				}

				// MANAGE CHILDREN USERS
				if(count($contentVO->childrenUsers)) {
					for($i = 0; $i < count($contentVO->childrenUsers); ++$i)
					{
						$userInVO = $contentVO->childrenUsers[$i];
						// ARRAY of USERS VO with our without ID

						// If has email in, checks for diferences in fields
						if(UserManager::isEmailIn($userInVO->email) && $userInVO->email != "") {
							UserManager::hasUserChanged($userInVO, true);
						}
						// no email in, adds User
						else {
							$userRegisterResponse = UserManager::registerUser($userInVO, null, false, $contentVO->userParentID);
							$userInVO->ID = $userRegisterResponse->data;
						}
						// Adss to relations
						RelationManager::setUserToContent($contentVO->UID, $userInVO->ID);
					}
				}

			}
		}
		$contentVO = self::getContent($contentVO->UID, $contentVO->langID, null, true, true);
		return $contentVO;
	}


	public static function getContentsFromUser($userID, $langID = null)
	{
		if(!$langID)
		{
			$langID = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($langID))
			{
				$langID = LanguageManager::getLanguageIDByShort($langID)->ID;
			}
		}
		$checkUser = UserManager::getUser($userID);
		if(isset($checkUser->ID))
		{
			// DO YOUR STUFF
			$response = array();
			$res =mysql_query("SELECT UID from contents WHERE userParentID = $userID");
			while ($row=mysql_fetch_array($res))
			{
				$content = self::getContent($row['UID'], $langID);
				$content->children = self::getChildren($row['UID'], $langID);
				$response[] = $content;
			}
			if(!count($response))
			{
				return null;
			}
			return $response;

		}
		else
		{
			return false;
		}
	}

	public static function getTotalPages($uid, $langid, $itensPerPage = 10)
	{
		$queryTotalRegistos = BdConn::runSQL("SELECT count(contentsInfo.contentsUID) as total FROM contentsInfo INNER JOIN contentsToContents ON contentsInfo.contentsUID = contentsToContents.childUID WHERE contentsToContents.parentUID = $uid AND contentsInfo.langID = $langid AND enabled = 1 ORDER by contentsToContents.sortOrder ASC");
		$total = BdConn::next($queryTotalRegistos);
		$total = (int) $total['total'];
		return (int) ceil($total/$itensPerPage);
	}


	public static function deleteContent($UID) {

		if( !isset($_SESSION) ) session_start();
		if(isset($_SESSION['userID'])) {
			$user = $_SESSION['userID'];
		}
		if(AuthorizationManager::hasUserAccessToContent($user->ID, $UID)) {
			$sql ="DELETE FROM contentsInfo WHERE contentsUID = '$UID'";
			BdConn::runSQL($sql);

			$sql ="DELETE FROM contents WHERE UID = '$UID'";
			BdConn::runSQL($sql);

			$sql ="DELETE FROM filesToContents WHERE contentsUID = '$UID'";
			BdConn::runSQL($sql);

			$sql ="DELETE FROM contentsRolesPermissions WHERE contentsUID = '$UID'";
			BdConn::runSQL($sql);

			self::delChildren($UID);

			$sql ="DELETE FROM contentsToContents WHERE childUID = '$UID'";
			BdConn::runSQL($sql);

			return true;


		}
		return false;
	}

	private function delChildren($parentUID) {
		$res=mysql_query("SELECT
		contentsInfo.contentsUID as UID
		FROM contentsInfo
		INNER JOIN contentsToContents ON contentsInfo.contentsUID = contentsToContents.childUID
		WHERE contentsToContents.parentUID = '$parentUID' AND contentsToContents.mainParent = 1
		ORDER BY contentsToContents.sortOrder ASC");


		while ($row=mysql_fetch_array($res))
		{
			$UID = $row['UID'];

			self::delChildren($UID);

			$sql ="DELETE FROM contentsInfo WHERE contentsUID = '$UID'";
			BdConn::runSQL($sql);

			$sql ="DELETE FROM contents WHERE UID = '$UID'";
			BdConn::runSQL($sql);


			$sql ="DELETE FROM filesToContents WHERE contentsUID = '$UID'";
			BdConn::runSQL($sql);

			$sql ="DELETE FROM contentsRolesPermissions WHERE contentsUID = '$UID'";
			BdConn::runSQL($sql);

			$sql ="DELETE FROM contentsToContents WHERE childUID = '$UID'";
			BdConn::runSQL($sql);
		}
		return true;
	}



		
	/**
	 * getMultipleChildren
	 *
	 * @param  array $uids
	 * @param  int $langid
	 * @return array
	 */
	public static function getMultipleChildren($uids, $langID, $templatesToConsider = NULL) {
		$contents = [];
		if(!$langID)
		{
			$langID = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($langID))
			{
				$langID = LanguageManager::getLanguageIDByShort($langID)->ID;
			}
		}
		if(is_array($uids)) {

			for($i = 0; $i < count($uids); $i++) {
				$children = self::getChildren($uids[$i], $langID, NULL, NULL, NULL,  NULL, null, null, NULL, $templatesToConsider);
				$contents = array_merge($contents, $children);
			}
			
			return $contents;
		}
		return false;
	}
	/**
	 * getContentByVerboseID
	 *
	 * @param  string $verboseID
	 * @param  int $langID
	 * @return array
	 */
	public static function getUIDByVerboseID($verboseID)
	{
		
		$sql = "SELECT UID FROM contents WHERE verboseID='$verboseID'";
        BdConn::query($sql);
		$uid = BdConn::single();
		
        return $uid['UID'];
	}
	/**
	 * getContentByVerboseID
	 *
	 * @param  string $verboseID
	 * @param  int $langID
	 * @return array
	 */
	public static function getContentByVerboseID($verboseID, $langID)
	{
		if(!$langID)
		{
			$langID = LanguageManager::getDefaultLanguage()->ID;
		}
		else
		{
			if(!is_numeric($langID))
			{
				$langID = LanguageManager::getLanguageIDByShort($langID)->ID;
			}
		}
		
		
        return self::getContent(self::getUIDByVerboseID($verboseID), $langID);
	}

	/**
	 * getRedirectMatch
	 *
	 * @param  string $url
	 * @return $addedUIDurlString
	 */
	public static function getRedirectMatch($url)
	{
		$sql = "SELECT * FROM redirects WHERE url = '$url'";
		$resultset=BdConn::query($sql);
		$urlMatch = BdConn::single($resultset);
		return $urlMatch;
	}
	

}
