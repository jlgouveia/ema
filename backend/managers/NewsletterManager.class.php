<?php


class NewsletterManager {

	public static $language = "";




	/**
	*
 	* Author: Jose Luis Gouveia
	* This method returns all subscriptions
	*
	*/
	public static function getAllSubscriptions()
	{
		$res=BdConn::runSQL("SELECT firstName, email, subscribeDate  FROM usersWebsite WHERE active = '1' AND enabled = '1'");
		$lista="";
		foreach($res as $row)
		{

			$row=NewsletterManager::limpaNumericos($row);
			$lista[]=$row;

		}
		return $lista;
	}


	/**
	*
 	* Author: Jose Luis Gouveia
	* This method insert a record in newsletter
	*
	*/

	public static function insertSubscription($lang, $email = "", $name = "", $notes = "", $duploOptin = true)
	{

		self::$language = $lang;

		date_default_timezone_set("Europe/London");
		$managerInstance = new NewsletterManager();
		
		if(UserManager::isEmailIn($email))
		{
			$userVO = $managerInstance->getUser($email);
			
			$userVO->newsletter = 1;
			$userVO->notes = $notes;
			

			if(!isset($userVO->firstName) && isset($name)) {
				$userVO->firstName = $name;
			}
			
			$_SESSION["userID"] = serialize($userVO);
			
			$result = UserManager::editUserBasicInfo($userVO, $lang);
			LogManager::logAction($userVO->ID, '13', null, null, $notes);
			if($duploOptin) {
				self::duploOptin($userVO->firstName, $email, $userVO->activationCode);
			}
			return $result;

		}
		else
		{
			$userVO = new UserVO();
			$userVO->email = $email;
			$userVO->newsletter = 1;
			$userVO->notes = $notes;
			return UserManager::registerUser($userVO, $lang, $duploOptin);
		}
	}
	private function getUser($email)
	{
		$sql = "SELECT * FROM usersWebsite WHERE email = '$email'";
		
		$query = BdConn::query($sql);
		$row = BdConn::single();
        $userVO = new UserVO($row);
        return $userVO;
		
	}

	/**
	*
 	* Author: Jose Luis Gouveia
	* This method verifies if the email is already in the database
	*
	*/
	public static function isEmailIn($email_in)
	{
		$sqlquery = "SELECT email FROM usersWebsite WHERE email='$email_in'";

		$result = BdConn::runSQL($sqlquery);
		$number = BdConn::single();

		if ($number)
		{
			return true;
		}
		else {
			return false;
		}

	}


	/**
	*
 	* Author: Jose Luis Gouveia
	* This method verifies if the email is already activated
	*
	*/
	public static function isEmailActive($email_in)
	{
		$sqlquery = "SELECT active FROM usersWebsite WHERE email='$email_in' AND active = '1'";

		$result = BdConn::runSQL($sqlquery);
		$number = BdConn::single();

		if ($number)
		{
			return true;
		}
		else {
			return false;
		}

	}



	public static function activateSubscription($email_in, $active_code_in, $lang, $sendPassword = true, $isNewsletter = true)
	{
		return UserManager::activateSubscription($email_in, $active_code_in, $lang, $sendPassword, $isNewsletter);
	}
	public static function duploOptin($name_in,$email_in, $active_code_in){

		$to = $email_in;

		// Provides: <body text='black'>

		$mySubect = LabelsManager::getLabelValue("NEWSLETTERCONFIRMATIONSUBJECT",self::$language);

		$subject = SettingsManager::getAdminName()." / ".$mySubect;


		$link = SettingsManager::getSiteURL() ."/ema/frontend/inc/registerActivation.php?activation_code=". $active_code_in   . "&amp;language=".self::$language."&email=" . $email_in;

		$myContent = LabelsManager::getLabelValue("NEWSLETTERCONFIRMATIONSBODY",self::$language);

		$link_replace = array("%LINK%");

		$content = str_replace($link_replace,$link, $myContent);
		//return $name_in." ".$email_in." ".$active_code_in." ".$mySubect." ".$subject. " ".$link." ".$myContent;
		return SendEmailManager::sendEmail($to, $subject, $content);

	}

	public static function isCodeValid($email_in, $active_code_in){
		
		//return "SELECT * FROM usersWebsite WHERE email='$email_in' AND activationCode='$active_code_in'";
		$result = BdConn::runSQL("SELECT * FROM usersWebsite WHERE email='$email_in' AND activationCode='$active_code_in'");
		

		return $result;

		if ($number)
		{
			return true;
		}
		else {
			return false;
		}

		

	}

	private function limpaNumericos($row)
	{
		foreach ($row as $key => $value)
			if (is_numeric($key))
				unset( $row[$key] );

		return $row;

	}
}
