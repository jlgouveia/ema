<?php

class CTTManager
{
	// REF: http://www.cttexpresso.pt/fewcm/wcmservlet/ctt/empresas/Oferta_Nacional/prepenvioenderecamento/informacoesuteis.html

	/**
	* verify if zipCode4 is "ilhas"
	*
	* @param int (zipcode4)
	* @return Bool
	*/
	private function isZipcodeIsland($zipcode4)
	{
		// All "ilhas" zpicodes start  with 9
		$zipcode4 = (string)$zipcode4;

		if($zipcode4[0] == "9")
		{
			return true;
		}
		return false;
	}

	/**
	* get shipping price by given zipcode and package weight
	*
	* @param int (zipcode4)
	* @param int (weight)
	* @param string (paymentMethod) adds 3.5 if "cobrança"
	* @return Number
	*/
	function getPackagePrice($zipcode4, $pWeight, $paymentMethod)
	{
		/*
		até 3kg
		€6.00
		€9.00

		entre 3kg - 6kg
		€10.00
		€13.00

		entre 6kg - 9kg
		€14.00
		€17.00

		entre 9kg - 15kg
		€18.00
		€21.00

		entre 15kg - 20kg
		€22.00
		€25.00

		entre 20kg - 40kg
		€26.00
		€29.00

		entre 40kg - 60kg
		€30.00
		€35.00

		COBRANÇA 3.5
		*/
		$price = 0;
		$cobranca = 3.5;

		$rules = array();
		$rules[0] = $pWeight <= 3000;
		$rules[1] = $pWeight > 3000 && $pWeight <= 6000;
		$rules[2] = $pWeight > 6000 && $pWeight <= 9000;
		$rules[3] = $pWeight > 9000 && $pWeight <= 15000;
		$rules[4] = $pWeight > 15000 && $pWeight <= 20000;
		$rules[5] = $pWeight > 20000 && $pWeight <= 40000;
		$rules[6] = $pWeight > 40000 && $pWeight <= 60000;
		$rules[7] = $pWeight > 60000;

		$weightPricesContinente = array(6, 10, 14, 18, 22, 26, 30, 30);
		$weightPricesIlhas = array(9, 13, 17, 21, 25, 29, 35, 35);

		for($i = 0; i < count($rules); ++$i)
		{
			if($rules[$i])
			{
				if(!self::isZipcodeIsland($zipcode4))
				{
					$price = $weightPricesContinente[$i];
				}
				else
				{
					$price = $weightPricesIlhas[$i];
				}
				break;
			}
		}
		if(strtolower($paymentMethod) == "cobrança")
		{
			$price += $cobranca;
		}

		return $price;
	}
}

?>