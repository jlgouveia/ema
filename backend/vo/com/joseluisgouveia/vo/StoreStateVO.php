<?php

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class StoreStateVO extends ValueObject
{
	public $ID;
	public $label;
	public $color;
	
	public $_explicitType= "com.joseluisgouveia.vo.StoreStateVO";
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

?>