<?php

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class WebsiteVO extends ValueObject
{
	public $ID;
	public $name;
	public $url;
	public $logoURL;
	public $mysqlHost;
	public $mysqlDB;
	public $mysqlUser;
	public $mysqlPassword;
	public $enabled;
	
	public $_explicitType= "com.joseluisgouveia.vo.WebsiteVO";
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

?>