<?php

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class StoreCartVO extends ValueObject
{
	public $ID;
	public $userID;
	public $creationDate;
	public $country;
	public $currencySymbol;
	public $vatRate;
	public $totalPrice;
	public $totalItens;
	public $shippingFees;
	public $shippingCustomValue;
	public $shippingFeesVat;
	public $shippingFeesWhitoutVat;
	public $products;
	public $productsTotalPrice;
	public $productsTotalPriceWhitoutVat;
	public $vatTotalValue;
	public $vatRateValue;
	public $totalPriceWhitoutVat;
	public $totalWeight;
	public $totalWeightInKilograms;
	public $vatTotalValueWithFeeVat;




	public $_explicitType= "com.joseluisgouveia.vo.StoreCartVO";
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

?>