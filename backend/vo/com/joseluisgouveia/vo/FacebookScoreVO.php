<?PHP

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class FacebookScoreVO extends ValueObject
{
	public $participantFacebookUserID;
	public $totalPoints;
	public $image;
	public $firstName;
	public $lastName;
	public $fullName;
	public $rank;
	public $voted;
	public $email;
	
	
	
	public $_explicitType= "com.joseluisgouveia.vo.FacebookScoreVO";
		
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}



?>