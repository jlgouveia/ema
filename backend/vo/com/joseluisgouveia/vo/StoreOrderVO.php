<?php

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class StoreOrderVO extends UserVO
{
	public $ID;
	public $userID;
	public $shippingContactFirstName;
	public $shippingContactLastName;
	public $shippingAddress;
	public $shippingZipcode4;
	public $shippingZipcode3;
	public $shippingZipcodeZone;
	public $shippingAlternativeZipcode;
	public $shippingCountry;
	public $shippingContactMobile;
	public $invoiceFirstName;
	public $invoiceLastName;
	public $invoiceNif;
	public $invoiceAddress;
	public $invoiceZipcode4;
	public $invoiceZipcode3;
	public $invoiceZipcodeZone;
	public $invoiceAlternativeZipcode;
	public $invoiceCountry;
	public $notes;
	public $paymentMethodID;
	public $paymentMethodLabel;
	public $orderDate;
	public $sentDate;
	public $completeDate;
	public $stateID;
	public $products;


	public $_explicitType= "com.joseluisgouveia.vo.StoreOrderVO";
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

?>