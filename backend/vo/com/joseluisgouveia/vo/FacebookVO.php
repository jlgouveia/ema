<?PHP

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class FacebookVO extends ValueObject
{
	public $appID;
	public $appURL;
	public $pageID;
	
	public $_explicitType= "com.joseluisgouveia.vo.FacebookVO";
		
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}



?>