<?PHP

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class FacebookVotingSettingsVO extends ValueObject
{
	public $participantsThatWin;
	public $firstVotesIndicator;	
	
	public $_explicitType= "com.joseluisgouveia.vo.FacebookVotingSettingsVO";
		
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}



?>