<?PHP

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class LabelVO extends ValueObject
{
	public $ID;
	public $langID;
	public $type;
	public $label;
	public $kind;
	public $data;
	public $_explicitType= "com.joseluisgouveia.vo.LabelVO";

	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

?>