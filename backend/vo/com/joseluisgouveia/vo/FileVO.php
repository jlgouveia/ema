<?php

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class FileVO extends ValueObject
{
	public $ID;
	public $file;
	public $title;
	public $subtitle;
	public $body;
	public $optional1;
	public $optional2;
	public $optional3;
	public $optional4;
	public $default;
	public $thumb;
	public $videoType;
	public $videoID;	
	public $label;
	public $deeplink;
	public $description;
	public $extension;
	public $date_added;
	
	public $_explicitType= "com.joseluisgouveia.vo.FileVO";
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

?>