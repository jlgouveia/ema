<?php

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class ContentVO extends ValueObject
{
	public $ID;
	public $UID;
	public $langID;
	public $deeplink;
	public $menuLabel;
	public $title;
	public $subtitle;
	public $body;
	public $date;
	public $lead;
	public $author;
	public $location;
	public $startDate;
	public $endDate;
	public $price;
	public $vatPrice;
	public $weight;
	public $quantity;
	public $storeVatRateID;
	public $link;
	public $linkTarget;
	public $optional1;
	public $optional2;
	public $optional3;
	public $optional4;
	public $optional5;
	public $optional6;
	public $optional7;
	public $optional8;
	public $optional9;
	public $optional10;
	public $highlightType;
	public $automaticSelectChild;
	public $files;
	public $children;
	public $childrenUsers;
	public $template;
	public $templateLabel;
	public $siteMapLabel;
	public $locked;
	public $showInMenu;
	public $enabled;
	public $urlTitle;
	public $metaDescription;
	public $metaKeywords;
	public $isHome;
	public $notes;
	public $userParent;
	// PCF PROPERTIES
	public $recorded_audio;
	public $result;
	public $formated_duration_sum;
	public $duration_sum;
	public $total_time_formated;
	public $test;
	public $userParentID;
	public $relevance;


	public $_explicitType= "com.joseluisgouveia.vo.ContentVO";

	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}



?>