<?PHP
require_once(CLASSESPATH.'/core/ValueObject.class.php');

class LanguageVO extends ValueObject
{
	public $ID;
	public $short;
	public $sortOrder;
	public $description;
	public $currencySymbol;
	public $currencyID;
	public $enabled;
	public $isDefault;
	public $translateCode;
	public $_explicitType= "com.joseluisgouveia.vo.LanguageVO";
		
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

?>