<?php

require_once(CLASSESPATH.'/core/ValueObject.class.php');

class SettingsVO extends ValueObject
{
	public $siteURL;
	public $title;
	public $description;
	public $keywords;
	public $globalEmail;
	public $operationalEmail;
	public $storeOrderEmailTemplate;
	public $ordersEmail;
	public $adminName;
	public $analytics;
	public $linkColor;
	public $registerUserEmailTemplate;
	
	public $_explicitType= "com.joseluisgouveia.vo.SettingsVO";
	public function __construct($row = null)
	{
		parent::__construct($row);
	}
}

?>