<?php


class ValueObject
{

	public $_explicitType;

	public function __construct($row) {

		$this->_explicitType= VALUEOBJECTSPATH . "/". get_class($this);

		if (is_array($row))
			foreach ($row as $key => $value)
			{
				if (array_key_exists($key, $this))
				{

					$this->$key = $value;
					if(is_numeric($this->$key)) { $this->$key = (float) $this->$key; }
					$obj = new stdClass();
					if ($value=="")
						$this->$key ="";
				}
			}

	}

	public function createProperty($name, $value){
        $this->{$name} = $value;
    }
	// Remove os campos que não tenham dados
	function optimize() {
		foreach($this as $key => $value)
			if ($this->$key==null)
	  			unset($this->$key);

	  	return $this;
	}
}