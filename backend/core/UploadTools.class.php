<?php

	class UploadTools {
	
		
		function getFileExtension($file)
		{
			return strtolower(substr($file, strripos($file, '.')));
		}
		
		public function addImage($image, $newPath, $allowedExtensions)
		{

			if ($image==null)
				throw new Exception (ExceptionManager::$PT['IMAGE_PATH_NOT_FOUND']);
			
			$extension=Tools::getFileExtension($image);
						
			$valid_extension=false;
			for ($i=0; $i<count($allowedExtensions); ++$i)
			{
				if (strtolower($extension)==$allowedExtensions[$i])
				{
					$valid_extension=true;
					break;
				}
				
			}
			if ($valid_extension)
				throw new Exception (ExceptionManager::$PT['INVALID_EXTENSION']);
			
			if (!file_exists($newPath))
				mkdir($newPath); 
			
			$newname=(time() . rand(999, 999999));

			$newFile=$newPath. '/'. $newname . $extension;

			if (!copy($image, $newFile))				
				throw new Exception (ExceptionManager::$PT['FILE_COPY_FAILED']);
		
			unlink($image);
			
			return $newname. $extension;
		}
		
		public function addPdf($pdf, $newPath,  $allowedExtensions)
		{
			
			if ($pdf==null)
				throw new Exception (ExceptionManager::$PT['PATH_NOT_FOUND']);
			
			$extension=Tools::getFileExtension($pdf);
			
			$valid_extension=false;
			for ($i=0; $i<count($allowedExtensions); ++$i)
			{
				if (strtolower($extension)==$allowedExtensions[$i])
				{
					$valid_extension=true;
					break;
				}
				
			}
			if (!$valid_extension)
				throw new Exception (ExceptionManager::$PT['INVALID_EXTENSION']);
			
			if (!file_exists($newPath))
				mkdir($newPath); 
			
			$newname=(time() . rand(999, 999999));

			$newFile=$newPath. '/'. $newname . $extension;
			
			if (!copy($pdf, $newFile))				
				throw new Exception (ExceptionManager::$PT['FILE_COPY_FAILED']);
		
			unlink($pdf);
			
			return $newname. $extension;
		}
		
		public function addVideo($video, $newPath,  $allowedExtensions)
		{
			
			if ($video==null)
				throw new Exception (ExceptionManager::$PT['PATH_NOT_FOUND']);
			
			$extension=Tools::getFileExtension($video);
			
			$valid_extension=false;
			for ($i=0; $i<count($allowedExtensions); ++$i)
			{
				if (strtolower($extension)==$allowedExtensions[$i])
				{
					$valid_extension=true;
					break;
				}
				
			}
			if (!$valid_extension)
				throw new Exception (ExceptionManager::$PT['INVALID_EXTENSION']);
			
			if (!file_exists($newPath))
				mkdir($newPath); 
			
			$newname=(time() . rand(999, 999999));

			$newFile=$newPath. '/'. $newname . $extension;
			
			if (!copy($video, $newFile))				
				throw new Exception (ExceptionManager::$PT['FILE_COPY_FAILED']);
		
			unlink($video);
			
			return $newname. $extension;
		}
		
		public function checkFileSizes($image, $max_width, $max_height, $max_size)
		{
	
			if ($image==null)
				throw new Exception (ExceptionManager::$PT['IMAGE_PATH_NOT_FOUND']);
			
			$imageInfo = getimagesize($image);
				
			if($max_width < $imageInfo[0])
				throw new Exception('A imagem excede a largura máxima definida ('.$max_width.' pixeis)');
				
			if($max_height < $imageInfo[1])
				throw new Exception('A imagem excede a altura máxima definida ('.$max_height.' pixeis)');
				
			if($max_size < filesize($image))
				throw new Exception('A imagem excede o tamanho máximo definido ('.Tools::byteToString($max_size).')');
		}
		
		public static $imageExtensions=array(0 => '.jpg', 1 => '.png',2 => '.gif');
		public static $pdfExtension=array(0 =>'.pdf');
		public static $videoExtension=array(0 =>'.flv');
		//public static $docsExtensions=array(0 => '.pdf', 1 => '.txt');

	}
