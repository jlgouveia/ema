<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<?php

require_once("../../backend/cfg/config.inc.php");
$userVO = new UserVO();
$userVO->email = trim($_GET['email']);
$userVO->activationCode = trim($_GET['activation_code']);

$myOperationEmailManager = new OperationEmailManager;

$msg = UserManager::sendNewPassword($userVO, $GLOBALS['mainLang']);
echo $myOperationEmailManager->getOperationalEmail($msg);



?>

<script type="text/javascript">
	function timerClose()
	{
		var t=setTimeout("closeWindow()",3000);
	}
	function closeWindow()
	{
		window.close();
	}
	timerClose();
</script>