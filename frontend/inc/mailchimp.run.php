<?php
require_once($_SERVER['DOCUMENT_ROOT']."/ema/backend/cfg/config.inc.php");
$MailChimp = new MailChimp('7a6679ccb5b448dc0aa51da59d4667b8-us9');

//print_r($MailChimp->call('lists/list'));
$email = $_POST['subscriberEmail'];
$mailChimpListID = $_POST['mailChimpListID'];
$result = $MailChimp->call('lists/subscribe', array(
	'id'                => $mailChimpListID,
	'email'             => array('email'=>$email),
	'double_optin'      => true,
	'update_existing'   => false,
	'replace_interests' => false,
	'send_welcome'      => true,
));
echo json_encode($result);

?>