<?php


require_once($_SERVER['DOCUMENT_ROOT']."/backend/cfg/config.inc.php");

$userVO = new UserVO();
$userVO->email = trim($_POST['username']);
$userVO->password = trim($_POST['password']);

$userVO = UserManager::login($userVO);



if($userVO->ID)
{

	
	echo json_encode(array($userVO));
}
else
{	
	echo LabelsManager::getLabelValue("GENERICLOGINERROR", $GLOBALS['mainLang']);
}
?>

