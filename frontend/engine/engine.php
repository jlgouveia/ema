<?php

require_once($_SERVER['DOCUMENT_ROOT']."/ema/backend/cfg/config.inc.php");

extract($_POST);

if(!isset($languageID))
{
	if(isset($_COOKIE['languageID']))
	{
		$languageID = $_COOKIE['languageID'];
	}
	else
	{
		$languageID = LanguageManager::getDefaultLanguage();
		$languageID = $languageID->ID;
		$lang = $languageID;
	}
}
header('Content-Type: application/json');
switch($action)
{
	case "login":
		$userVO = new UserVO();
		$userVO->email = $email;
		$userVO->password = $password;

		echo json_encode(UserManager::login($userVO));
	break;

	case "logout":
		echo json_encode(UserManager::logout());
	break;

	case "getLabelValue":
		echo json_encode(LabelsManager::getLabelValue($type, $languageID, $toUpper));
	break;

	case "getChildren":

		if(!isset($limit))
		{
			$limit = null;
		}
		if(!isset($rand))
		{
			$rand = null;
		}
		if(!isset($orderBy))
		{
			$orderBy = null;
		}

		if(!isset($options))
		{
			$options = null;
		}

		if(!isset($currentPage))
		{
			$currentPage = null;
		}

		if(!isset($itensPerPage))
		{
			$itensPerPage = 10;
		}
		if(!isset($customWhere))
		{
			$customWhere = NULL;
		}
		if(!isset($customWhere))
		{
			$customWhere = NULL;
		}

		if(!isset($templatesToConsider))
		{
			$templatesToConsider = null;

		}
		
		// getChildren($uid, $lang= null, $limit = NULL, $rand = NULL, $orderBy = NULL, $options = NULL, $currentPage = null, $itensPerPage = 10, $customWhere = NULL, $templatesToConsider = NULL)
		
		echo json_encode(ContentsManager::getChildren($UID, $languageID, $limit, $rand, $orderBy, $options, $currentPage, $itensPerPage, $customWhere, $templatesToConsider));
	break;

	case "getMultipleChildren":
		if(!isset($templatesToConsider))
		{
			$templatesToConsider = null;

		}
		echo json_encode(ContentsManager::getMultipleChildren($UID, $languageID, $templatesToConsider));
	break;

	case "getChildrenOrderByDate":
		echo json_encode(ContentsManager::getChildrenOrderByDate($UID, $languageID));
	break;


	case "recoverPassword":
		$user = new UserVO($user);
		echo json_encode(UserManager::recoverPassword($user, $languageID));
	break;

	case "getAllContactsFromUser":
		echo json_encode(UserManager::getAllContactsFromUser());
	break;

	case "registerUser":
		// pass registerUser as input hidden
		$user = new UserVO($_POST);
		if(isset($body))
		{
			$body = implode(" ", $body);
			$user->notes = $body;
			//die(var_dump($user));
		}

		// ARRANJE FULLNAME
		if(!$user->firstName)
		{
			if(isset($_POST['name']))
			{
				$name = $_POST['name'];

				$parts = explode(" ", $name);

				if(count($parts) > 1)
				{
					$user->lastName = array_pop($parts);
					$user->firstName = implode(" ", $parts);
				}
				else
				{
					$user->firstName = $name;
				}
			}
		}

		// CONVERT BIRTHDATE

		//if(isset($birthDay)) { $user->birthday = $birthYear."-".$birthMonth."-".$birthDay; }

		if(isset($password))
		{
			$user->password = md5($password);
		}


		if(count($_FILES))
		{
			$user->files = $files = reArrayFiles($_FILES['file']);
		}

		if(!isset($duploOptin)) { $duploOptin = true; } else { $duploOptin = $duploOptin === 'true'? true: false; }
		if(!isset($sendRegisterToAdmin)) { $sendRegisterToAdmin = true; } else { $sendRegisterToAdmin = $sendRegisterToAdmin === 'true'? true: false; }
		if(!isset($sendRegisterInfoToUser)) { $sendRegisterInfoToUser = true; } else { $sendRegisterInfoToUser = $sendRegisterInfoToUser === 'true'? true: false; }
		if(!isset($allowDuplicateEmail)) { $allowDuplicateEmail = false; } else { $allowDuplicateEmail = $allowDuplicateEmail === 'true'? true: false; }
		if(!isset($user->parentID)) { $user->parentID = 0; }
		if(!isset($returnLabelTypeSucess)) { $returnLabelTypeSucess = "REGISTERSUCCESS"; }
		if(!isset($registerConfirmationSubject)) { $registerConfirmationSubject = "REGISTERCONFIRMATIONSUBJECT"; }
		if(!isset($registerConfirmationBody)) { $registerConfirmationBody = "REGISTERCONFIRMATIONSBODY"; }
		if(!isset($updateUser)) { $updateUser = true; } else { $updateUser = $updateUser === 'true'? true: false; }

		echo json_encode(UserManager::registerUser($user, $languageID, $duploOptin, $sendRegisterToAdmin, $user->parentID, $allowDuplicateEmail, $returnLabelTypeSucess, $registerConfirmationSubject, $registerConfirmationBody, $sendRegisterInfoToUser, $updateUser));


	break;

	case 'editUserBasicInfo':

		$user = new UserVO($_POST);
		if(!$user->firstName)
		{
			$name = $_POST['name'];

			$parts = explode(" ", $name);

			if(count($parts) > 1)
			{
				$user->lastName = array_pop($parts);
				$user->firstName = implode(" ", $parts);
			}
			else
			{
				$user->firstName = $name;
			}

		}

		// CONVERT BIRTHDATE
		//$birthdayConvert = strtotime($birthday);
		//$newBirthday = date('Y-m-d',$birthdayConvert);
		$user->birthday = $birthYear."-".$birthMonth."-".$birthDay;

		if(isset($password))
		{
			$user->password = md5($password);
		}
		echo json_encode(UserManager::editUserBasicInfo($user, $languageID, $parentID));
	break;

	case 'getUser':
		echo json_encode(UserManager::getUser($userID));
	break;
	
	case 'getAllUsers':
		echo json_encode(UserManager::getAllUsers());
	break;
	
	case 'searchUser':
		if(isset($limit)) {
			$limit = $limit;	
		}
		else {
			$limit = 5;
		}
		if(isset($searchField)) {
			$searchField = $searchField;	
		}
		else {
			$searchField = null;
		}
		
		echo json_encode(UserManager::searchUser($search, $limit, $searchField));
	break;

	case 'getListFromField':
		echo json_encode(UserManager::getListFromField($listField));
	break;

	case 'getChildrenByDeeplink':
		echo json_encode(ContentsManager::getChildrenByDeeplink($contentDeeplink, $languageID));
	break;

	case 'getContentByDeeplink':
		echo json_encode(ContentsManager::getContentByDeeplink($contentDeeplink));
	break;

	case 'getContent':
		echo json_encode(ContentsManager::getContent($contentUID, $languageID));
	break;

	case 'getChildren':
		echo json_encode(ContentsManager::getChildren($uid, $lang, $limit, $rand));
	break;

	case "rateContent":
		echo json_encode(ContentsManager::rateContent($UID, $rate));
	break;

	case "sendEmail":

		if(!isset($emailTo)) { $emailTo = SettingsManager::getGlobalEmail(); } else { $emailTo = $emailTo; }
		if(!isset($emailFrom)) { $emailFrom = SettingsManager::getGlobalEmail(); } else { $emailFrom = $emailFrom; }
		if(!isset($nameFrom)) { $nameFrom = SettingsManager::getAdminName(); } else { $nameFrom = $nameFrom; }
		if(!isset($useOperationEmail)) { $useOperationEmail = true; } else { $useOperationEmail = $useOperationEmail; }
		if(count($body))
		{
			$body = implode(" ", $body);
		}
		echo json_encode(SendEmailManager::sendEmail($emailTo, $subject, $body, $nameFrom, $emailFrom, $useOperationEmail));
	break;

	case "rateContent":
		echo json_encode(ContentsManager::rateContent($UID, $rate));
	break;

	case "search":
		if(!isset($limit))
		{
			$limit = null;
		}
		if(!isset($templatesToConsider))
		{
			$templatesToConsider = null;
		}
		if(!isset($groupByParent))
		{
			$groupByParent = null;
		}
		if(!isset($searchChildsOf))
		{
			$searchChildsOf = null;
		}
		if(!isset($searchInFiles))
		{
			$searchInFiles = true;
		}

		// ($string, $lang, $limit = NULL, $templatesToConsider = NULL, $groupByParent = NULL, $searchChildsOf = null, $searchInFiles = null)
		echo json_encode(ContentsManager::search($keyword, $languageID, $limit, $templatesToConsider, $groupByParent, $searchChildsOf, $searchInFiles));
	break;

	case "insertSubscription":
		if(!isset($name))
		{
			$name = "";
		}
		if(!isset($notes))
		{
			$notes = "";
		}
		echo json_encode(NewsletterManager::insertSubscription($languageID, $email, $name, $notes));
	break;

	case "mailchimpSubscription":
		$MailChimp = new MailChimp(MAILCHIMP_API_KEY);

		//print_r($MailChimp->call('lists/list'));
		$list_id = $_POST['mailChimpListID'];
		$result = $MailChimp->post("lists/$list_id/members", [
			'email_address' => $_POST['email'],
			'status'        => 'subscribed'
		]);
		// $email = $_POST['subscriberEmail'];
		// $mailChimpListID = ;
		// $result = $MailChimp->call('lists/subscribe', array(
		// 	'id'                => $mailChimpListID,
		// 	'email'             => array('email'=>$email),
		// 	'double_optin'      => true,
		// 	'update_existing'   => false,
		// 	'replace_interests' => false,
		// 	'send_welcome'      => true,
		// ));
		echo json_encode($result);

	break;

	case "addUserContent":
		if(isset($options)) {
			$options = $options;	
		}
		else {
			$options = null;
		}
		
		if(isset($userChildID)) {
			$userChildID = $userChildID;	
		}
		else {
			$userChildID = 0;
		}
		
		
		echo json_encode(ContentsManager::addUserContent($userChildID, $contentVO, $parentContentUID, $options));
	break;
	
	case "getContentsFromUser":
		echo json_encode(ContentsManager::getContentsFromUser($userID));
	break;

	case "saveImageBase64":
		//$imageData;
		if(!isset($prefix))
		{
			$prefix = "base-";
		}
		$imageDataName = $prefix.md5(date('YmdHis'));

		list($type, $imageData) = explode(';', $imageData);
		list(, $imageData)  = explode(',', $imageData);
		$imageData = base64_decode($imageData);

		file_put_contents($_SERVER['DOCUMENT_ROOT'].'/frontend/contents/users/'.$imageDataName.'.png', $imageData);
		$imageDataName = '/frontend/contents/users/'.$imageDataName.'.png';
		echo json_encode($imageDataName);
	break;
	default:
		//echo "no action";
	break;
	
	case "getContentsByGivenInterval":
		if(!isset($parentUIDIn)) {
			$parentUIDIn = null;
		}
		if(!isset($getUserParentObject)) {
			$getUserParentObject = true;
		}
		
		echo json_encode(ContentsManager::getContentsByGivenInterval($startDate, $intervalDays, $languageID, $parentUIDIn, $getUserParentObject));
	break;

	case 'zipContentFiles':
		if(isset($ZipFileName)) { $ZipFileName = UtilsManager::makeSlugs($ZipFileName).".zip"; } else { $ZipFileName = $UID.".zip"; }
		
		$destination = $_SERVER['DOCUMENT_ROOT']."/frontend/contents/".$ZipFileName;
		if(isset($overwrite)) { $overwrite = $overwrite; } else { $overwrite = false; }

		$files = FilesManager::getContentFiles($UID, $languageID);
		$filesArray = array();
		for($i = 0; $i < count($files->allFiles); $i++) {
			$filesArray[] = $_SERVER['DOCUMENT_ROOT']."/frontend/contents/".$files->allFiles[$i]->file;
		}
		$zipFile = UtilsManager::createZip($filesArray,$destination,$overwrite);
		echo json_encode($zipFile);
	break;

	case 'smartSearch':
		if(!isset($searchInFiles)) {
			$searchInFiles = true;
		}
		echo json_encode(ContentsManager::smartSearch($search, $searchInFiles));
	break;

	case 'logAction':
		if(!isset($contenUID)) { $contenUID = 0; } else { $contenUID = $contenUID; }
		if(!isset($notes)) { $notes = ""; } else { $notes = $notes; }
		if(!isset($userID)) { $userID = 0; } else { $userID = $userID; }
		
		echo json_encode(LogManager::logAction($userID, $actionID, null, $contenUID, $notes));
	break;
}

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}