<?php 
	//gallaecia-arq-arch.pdf
	require_once($_SERVER['DOCUMENT_ROOT']."/ema/backend/cfg/config.inc.php");
	extract($_GET);
	$fileName = $file;
	if(isset($file)) {
		$file = $_SERVER['DOCUMENT_ROOT']."/frontend/contents/".$_GET['file'];

		if (file_exists($file)) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);

		   	LogManager::logAction($userID, 10, 0, $contentUID, $fileName);
		}
	}
	
	header("HTTP/1.0 404 Not Found");