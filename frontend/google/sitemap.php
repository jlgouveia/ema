<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<?php echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'; ?>

<?php

	include_once("../../backend/cfg/config.inc.php");
	$lang = NULL;
	if(isset($_GET["lang"]))
	{
		$lang = $_GET["lang"];
	}
	$sitemap = MenuManager::getGoogleSitemap($lang);
	$depth = 1;
	function displaySiteMap($node)
	{
		global $depth, $siteURL;

		//$depth -= 0.2;
		for($i = 0; $i < count($node); ++$i)
		{

			//echo $node[$i]->deeplink;
			if($node[$i]->deeplink)
			{
			?>
<url>
					<loc><?php echo $node[$i]->deeplink; ?></loc>
					<changefreq>monthly</changefreq>
					<priority><?php echo $depth; ?></priority>
				</url>
				<?php
			}
			if(is_array($node[$i]->children)) {
				if(count($node[$i]->children))
				{
					displaySiteMap($node[$i]->children);
				}
			}
		}
	}

	displaySiteMap($sitemap);
?>
</urlset>