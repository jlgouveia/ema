<?php
ini_set('display_errors',1);
error_reporting(E_ALL);

date_default_timezone_set('UTC');
session_start();



// requires config file
require("backend/cfg/config.inc.php");

$ema = EmaBrain::Instance();
if(defined("ALLOW_GET_USERS")) {
	$ema::$ALLOW_GET_USERS = ALLOW_GET_USERS;
}
if(defined("ALLOW_EDIT_USERS")) {
	$ema::$ALLOW_EDIT_USERS = ALLOW_EDIT_USERS;	
}
if(defined("ALLOW_ADD_CONTENT_NO_SESSION")) {
	$ema::$ALLOW_ADD_CONTENT_NO_SESSION = ALLOW_ADD_CONTENT_NO_SESSION;
}
if(defined("ALLOW_ADD_USERS_RELATIONS")) {
	$ema::$ALLOW_ADD_USERS_RELATIONS = ALLOW_ADD_USERS_RELATIONS;
}

global $ema;
global $settings;
global $languageID;
global $menu;
global $content;
global $labels;
global $activationCode;
global $selectedID;
global $language;
global $userVO;
global $cart;
global $country;
global $allConvertedData;

$databaseVersion = SettingsManager::getDatabaseVersion();



if ($databaseVersion < 1 || $databaseVersion == NULL) {

	BdConn::runSql('ALTER TABLE contents MODIFY locked MEDIUMINT(9) default 0;');
	BdConn::runSql('ALTER TABLE contentsToContents MODIFY sortOrder TINYINT(4) default 0;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY firstName varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY lastName varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY email varchar(100) null;');
	if (!count($bdconn->query("SHOW COLUMNS FROM `usersWebsite` LIKE 'country'")->fetchAll())) {
	BdConn::runSql('ALTER TABLE usersWebsite ADD country varchar(50) null;');
	}
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY country varchar(50) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY birthday DATE default null;');
	BdConn::runSql('UPDATE usersWebsite SET birthday = NULL WHERE birthday = "0000-00-00"');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY address varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY zipcode4 varchar(4) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY zipcode3 varchar(3) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY zipcodeZone varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY alternativeZipcode varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY job varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingContactFirstName varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingContactLastName varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingAddress varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingZipcode4 varchar(4) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingZipcode3 varchar(3) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingZipcodeZone varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingAlternativeZipcode varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY shippingCountry varchar(50) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceFirstName varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceLastName varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceNif varchar(20) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceAddress varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceZipcode4 varchar(4) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceZipcode3 varchar(3) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceZipcodeZone varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceAlternativeZipcode varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY invoiceCountry varchar(50) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY phone varchar(20) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY mobile varchar(20) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional1 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional2 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional3 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional4 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional5 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional6 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional7 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional8 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional9 varchar(255) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY optional10 varchar(255) null;');
	if (!count($bdconn->query("SHOW COLUMNS FROM `usersWebsite` LIKE 'gender'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE usersWebsite ADD gender enum("f","m") default null;');
	}
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY gender enum("f","m") default null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY nif varchar(20) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY newsletter int(11) default 0;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY partnersPub int(11) default 0;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY notes text default null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY password varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY facebookID varchar(255) null;');

	BdConn::runSql('UPDATE usersWebsite SET subscribeDate = NULL WHERE subscribeDate = "0000-00-00"');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY subscribeDate DATETIME default null;');

	BdConn::runSql('UPDATE usersWebsite SET lastLoginDate = NULL WHERE lastLoginDate = "0000-00-00"');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY lastLoginDate DATETIME default null;');

	BdConn::runSql('UPDATE usersWebsite SET editDate = NULL WHERE editDate = "0000-00-00"');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY editDate DATETIME default null;');

	BdConn::runSql('UPDATE usersWebsite SET expireDate = NULL WHERE expireDate = "0000-00-00"');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY expireDate DATETIME default null;');

	BdConn::runSql('ALTER TABLE usersWebsite MODIFY activationCode varchar(100) null;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY active int(11) default 0;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY approved int(11) default 0;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY enabled int(11) default 1;');
	BdConn::runSql('ALTER TABLE usersWebsite MODIFY parentID int(11) default 0;');

	BdConn::runSql('ALTER TABLE log MODIFY notes text default null;');
	if (!count($bdconn->query("SHOW COLUMNS FROM `log` LIKE 'sessionID'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE log add sessionID varchar(250) null;');
	}
	if (!count($bdconn->query("SHOW COLUMNS FROM `log` LIKE 'referer'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE log add referer varchar(250) null;');
	}
	if (!count($bdconn->query("SHOW COLUMNS FROM `labels` LIKE 'editable'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE labels add editable varchar(250) null;');
	}
	BdConn::runSql('UPDATE settings SET databaseVersion  = 1');
}

if ($databaseVersion < 1.1) {
	BdConn::runSql('ALTER TABLE storeCartsProducts MODIFY customTitle varchar(255) null;');
	BdConn::runSql('ALTER TABLE storeCartsProducts MODIFY customImage varchar(255) null;');
	BdConn::runSql('ALTER TABLE storeCartsProducts MODIFY customImage varchar(255) null;');
	BdConn::runSql('ALTER TABLE storeCartsProducts MODIFY notes text default null;');
	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.1');
}

if ($databaseVersion < 1.2) {
	$val = BdConn::tableExists('storeSettings');

	if($val === FALSE)
	{
	   BdConn::runSQL('CREATE TABLE `storeSettings` (
	  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	  `access_token` text,
	  `refresh_token` text,
	  `expires_in` int(11) DEFAULT NULL,
	  `edit_date` datetime DEFAULT NULL,
	  `action` varchar(255) DEFAULT NULL,
	  	PRIMARY KEY (`id`)
		) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;');
	}


	if (!count($bdconn->query("SHOW COLUMNS FROM `storeCarts` LIKE 'editDate'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE storeCarts ADD editDate DATETIME default null;');
	}
	if (!count($bdconn->query("SHOW COLUMNS FROM `storeCarts` LIKE 'shippingCustomValue'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE storeCarts ADD shippingCustomValue float default null;');
	}

	if (!count($bdconn->query("SHOW COLUMNS FROM `storeOrders` LIKE 'editDate'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE storeOrders ADD editDate DATETIME default null;');
	}

	if (!count($bdconn->query("SHOW COLUMNS FROM `storeOrders` LIKE 'shippingCustomValue'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE storeOrders ADD shippingCustomValue float default null;');
	}
	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.2');
}
if ($databaseVersion < 1.3) {

	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional1 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional2 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional3 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional4 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional5 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional6 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional7 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional8 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional9 text default null;');
	BdConn::runSql('ALTER TABLE contentsInfo MODIFY optional10 text default null;');

	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.3');
}
if ($databaseVersion < 1.4) {

	if (!count($bdconn->query("SHOW COLUMNS FROM `contents` LIKE 'verboseID'")->fetchAll())) {
		BdConn::runSql('ALTER TABLE contents ADD verboseID varchar(255) null;');
	}

	BdConn::runSql('UPDATE settings SET databaseVersion  = 1.4');
}
if ($databaseVersion < 1.5) {
	$val = BdConn::tableExists('redirects');

	if($val === FALSE)
	{
	   BdConn::runSQL('CREATE TABLE `redirects` (
		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`url` varchar(250) DEFAULT NULL,
		`new_url` varchar(250) DEFAULT NULL,
		`redirect_code` int(11) DEFAULT NULL,
		PRIMARY KEY (`id`)
	  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
	}


  BdConn::runSql('UPDATE settings SET databaseVersion  = 1.5');
}


if(isset($_COOKIE['cart']))
{
	$cart = StoreManager::getFullCart();
}

$settings = SettingsManager::getSettings();

if(isset($_SESSION["userID"]))
{

	if (UtilsManager::is_serialized($_SESSION["userID"])) {
	    $userVO = unserialize($_SESSION["userID"]);
	} else {
	    $userVO = UtilsManager::fixObject($_SESSION["userID"]);
	}

	$activationCode = $userVO->activationCode;
	$userVO = UserManager::currentUser($activationCode);
}
$country = IP2NationManager::getCountry();


$countryCode = IP2NationManager::getCode($country);
$languageByIP =  LanguageManager::getLanguageByCountry($countryCode);
$language = $languageByIP;
if(!$languageByIP->ID) {
	$language = LanguageManager::getDefaultLanguage();
}



function getURLLevel($level)
{
	$levels = array();
	if(isset($_GET['id']))
	{
		$levels  = preg_split('[/]', strtolower($_GET['id']));

		$selectedLevel = "";
		if(is_numeric($level))
		{
			for($i = 0; $i <= $level; ++$i)
			{
				if($i < $level)
				{
					$selectedLevel .= $levels[$i]."/";
				}
				else
				{
					if(isset($levels[$i]))
					{
						$selectedLevel .= $levels[$i];
					}
				}

			}
		}
	}
	if($level >= count($levels))
	{
		$selectedLevel = "";
	}
	return $selectedLevel;
}
function getAddress()
{
	/*** check for https ***/
	$protocol = $_SERVER['HTTPS'] == 'on' ? 'https' : 'http';
	/*** return the full address ***/
	return $protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}
function returnURLLevels()
{
	$levels = "";
	if( isset($_GET['id']))
	{
		$levels  = preg_split('[/]', strtolower($_GET['id']));
	}
	return count($levels);
}


// DEFINE CONTENT ID

if(isset($_GET['id']))
{
	$selectedID = $_GET['id'];
}


// CHECK IF DEEPLINK EXISTS
if(isset($selectedID))
{
	if(!$language->ID) {
		$language = LanguageManager::getDefaultLanguage();
	}
	$content = ContentsManager::getContentByDeeplink($selectedID);

	if(!$content->UID)
	{
		$tryNewDeeplink = substr($selectedID, 0, strlen($selectedID)-1);

		$content = ContentsManager::getContentByDeeplink($tryNewDeeplink);

		if(!$content->UID)
		{
			tryFileDeeplink($selectedID);
			returnn404($selectedID);
		}
		if(!$content->enabled)
		{
			returnn404($selectedID);
		}
	}

	if(!$content->enabled)
	{
		if(!isset($_GET['preview'])) {
			returnn404($selectedID);
		}
	}
}
else
{


	if(isset($_COOKIE['languageID']))
	{
		$languageID = $_COOKIE['languageID'];
		$language = LanguageManager::getLanguage($languageID);

	}
	else
	{
		if(!$language->ID) {
			$language = LanguageManager::getDefaultLanguage();
		}
		$languageID = $language->ID;

	}
	$content = ContentsManager::getHomeContent ($languageID);
	$menu = MenuManager::getMenu($languageID);
	$labels = LabelsManager::getLabels($languageID);

	if($content->automaticSelectChild)
	{
		$content  = ContentsManager::getAutomaticSelectedChild($content->UID, $content->automaticSelectChild, $languageID);
		header("location:".$content->deeplink);
	}

	/*
$languageID = $languageByIP->ID;

	$content = ContentsManager::getHomeContent ($languageID);
	$menu = MenuManager::getMenu($languageID);
	$labels = LabelsManager::getLabels($languageID);
	if($content->automaticSelectChild)
	{
		$content  = ContentsManager::getAutomaticSelectedChild($content->UID, $content->automaticSelectChild, $languageID);
		header("location:".$content->deeplink);
	}
*/
}

if($content->template)
{

	// DEFINES LANGUAGE ID
	if(!isset($languageID))
	{
		 $languageID = $content->langID;
		 $language = LanguageManager::getLanguage($languageID);
		 setcookie('languageID', $languageID, time()+7*24*60*60, "/");
	}

	 $menu = MenuManager::getMenu($languageID);
	 $labels = LabelsManager::getLabels($languageID);



	// CHECK IF CONTENT IS LOCKED
	if(!$content->locked)
	{
		// CHECK IF TEMPLATE FILE EXISTS, IF NOT USES DEFAULT.template
		if(file_exists(TEMPLATESPATH.$content->template))
		{
			loadFrameworkTemplate($content->template);
		}

		else
		{

			loadFrameworkTemplate("default.php");
		}

	}
	else
	{

		if(isset($_SESSION["userID"]))
		{
			$userVO = $_SESSION["userID"];
			if(AuthorizationManager::hasUserAccessToContent($userVO->ID, $content->UID))
			{

				if(file_exists(TEMPLATESPATH.$content->template))
				{
					loadFrameworkTemplate($content->template);
				}

				else
				{

					loadFrameworkTemplate("default.php");
				}
			}
			else {
				loadFrameworkTemplate(TEMPLATE_NOLOGIN_FALLBACK);
			}

		}
		else
		{

			if($content->template == "default.php") {
				header("location:".$settings->siteURL);
			}
			else {
				loadFrameworkTemplate(TEMPLATE_NOLOGIN_FALLBACK);
			}

		}
	}


}
else
{

	loadFrameworkTemplate("default.php");
}

function defineGlobalSettings() {
	global $languageID;
	global $language;
	global $menu;
	global $labels;

	if(isset($_COOKIE['languageID']))
	{
		$languageID = $_COOKIE['languageID'];
		$language = LanguageManager::getLanguage($languageID);

	}
	else
	{
		$language = LanguageManager::getDefaultLanguage();
		$languageID = $language->ID;

	}
	//$content = ContentsManager::getHomeContent ($languageID);
	$menu = MenuManager::getMenu($languageID);
	$labels = LabelsManager::getLabels($languageID);
}

function tryFileDeeplink($selectedID){
	defineGlobalSettings();
	$file = FilesManager::getFileByDeeplink($selectedID);
	if(!$file->ID) {
		returnn404($selectedID);
	}
	global $content;
	$content->title = $file->label;
	$content->lead = $file->description;
	$content->deeplink = $file->deeplink;
	$content->files->default = $file;
	loadFrameworkTemplate(TEMPLATE_DEFAULT_FILE);
	exit();
}

function returnn404($selectedID = NULL) {
	$oldUrl = tryRedirect($selectedID);
	if(isset($oldUrl->url)) {
		if($oldUrl->url) {
			// die(var_dump($oldUrl->new_url));
			$location = "Location: /";
			
			$parsed = parse_url($oldUrl->new_url);
			
			if(!empty($parsed['scheme'])) {
				$location = "Location: ";
			}
			
			header($location.$oldUrl->new_url, true, $oldUrl->redirect_code);
			exit();
		} 
	}
	else { 
		header('HTTP/1.0 404 Not Found');
		loadFrameworkTemplate("404.php");
		exit();
	}
	
}

function tryRedirect($url) {
	return ContentsManager::getRedirectMatch($url);
}

function loadFrameworkTemplate($template) {
	global $language;
	setlocale(LC_ALL,$language->translateCode);

	global $settings;
	$mustacheSettings = array( 'settings' => LabelsManager::mustacheConvertObjects($settings));

	global $languageID;

	global $menu;
	$mustacheMenu = array();
 	if(is_array($menu)) { $mustacheMenu = array( 'menu' => LabelsManager::mustacheConvertObjects($menu, "UID")); }

	global $content;

	if(defined("FOUNDATION_INTERCHANGE_IMAGES")) {
		if(FOUNDATION_INTERCHANGE_IMAGES) {
			if($content->lead != "") {
				$dom = new DOMDocument();
				// CLEAN WARNINGS !?!?
				libxml_use_internal_errors(true);
				$dom->loadHTML(mb_convert_encoding($content->lead, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
				// CLEAN WARNINGS !?!?
				libxml_clear_errors();
				foreach ($dom->getElementsByTagName('img') as $image) {
					$siteUrl = SettingsManager::getSiteURL();
					$src = str_replace($siteUrl, "", $image->getAttribute('src'));
					$image->setAttribute('src', $src);
					
					$old_src = $image->getAttribute('src');
					// $image->setAttribute('src', $new_src);
					$image->setAttribute('data-src', $old_src);
					$responsive = FilesManager::getResponsiveImageFile($old_src);
					if(count($responsive)) {
						$interchange = "";
						for($i = 0; $i < count($responsive); $i++) {
							$interchange .= "[".$responsive[$i]->file.", ".$responsive[$i]->size."]";
							if($i < count($responsive)-1) {
								
								$interchange .= ",";
							}
						}
						$image->setAttribute('data-interchange', $interchange);
						
					}
					
				}

				$content->lead = $dom->saveHTML();
			}
			if($content->body != "") {
				$dom = new DOMDocument();
				// CLEAN WARNINGS !?!?
				libxml_use_internal_errors(true);
				$dom->loadHTML(mb_convert_encoding($content->body, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
				// CLEAN WARNINGS !?!?
				libxml_clear_errors();
				foreach ($dom->getElementsByTagName('img') as $image) {
					
					$old_src = $image->getAttribute('src');
					// $image->setAttribute('src', $new_src);
					$image->setAttribute('data-src', $old_src);
					$responsive = FilesManager::getResponsiveImageFile($old_src);
					if(count($responsive)) {
						$interchange = "";
						for($i = 0; $i < count($responsive); $i++) {
							$interchange .= "[".$responsive[$i]->file.", ".$responsive[$i]->size."]";
							if($i < count($responsive)-1) {
								
								$interchange .= ",";
							}
						}
						$image->setAttribute('data-interchange', $interchange);
						
					}
					
				}

				$content->body = $dom->saveHTML();
			}
		}
	}
	$mustacheContent = array( 'content' =>LabelsManager::mustacheConvertObjects($content));

	global $labels;
	$mustacheLabels = array();
	if(is_array($labels)) { $mustacheLabels = array( 'labels' =>LabelsManager::mustacheConvertObjects($labels, "type")); }

	global $activationCode;
	global $selectedID;

	global $language;
	$mustacheLanguage = array( 'language' =>LabelsManager::mustacheConvertObjects($language));

	global $userVO;
	$mustacheUserVO = array();
	if(is_array($userVO)) { $mustacheUserVO = array( 'userVO' =>LabelsManager::mustacheConvertObjects($userVO)); }


	global $cart;
	$mustacheCart = array();
	if(is_array($cart)) { $mustacheCart = array( 'cart' =>LabelsManager::mustacheConvertObjects($cart)); }


	Mustache_Autoloader::register();
	$mustache = new Mustache_Engine();
	global $allConvertedData;
	$allConvertedData = array_merge($mustacheSettings,$mustacheMenu,$mustacheContent,$mustacheLabels,$mustacheUserVO,$mustacheCart,$mustacheLanguage);

	$templateProcessed = getRenderedHTML(TEMPLATESPATH.$template);
	//var_dump($allConvertedData);
	echo $mustache->render($templateProcessed,$allConvertedData);
}

function getRenderedHTML($path)
{
	global $language;
	setlocale(LC_ALL,$language->translateCode);
	global $settings;
	global $languageID;
	global $menu;
	global $content;
	global $labels;
	global $activationCode;
	global $selectedID;
	global $language;
	global $userVO;
	global $cart;
	global $country;
	global $ema;
	global $allConvertedData;
	ob_start();
	include($path);
	$var=ob_get_contents();
	ob_end_clean();
	return $var;
}
